package com.dailysamachar.app.adapters;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.dailysamachar.app.R;

public class CustomViewHolder extends RecyclerView.ViewHolder {
    ImageView im_play;
    ImageView im_pause;
    RelativeLayout linearLayout;
    ImageView logoIV;
    ImageView imgdownIV;
    DemoplayerAdapter.OnItemClickListener onItemClickListener;


    public CustomViewHolder(@NonNull View itemView) {
        super(itemView);
        im_play=itemView.findViewById(R.id.play_video);
        im_pause=itemView.findViewById(R.id.pause_video);
        linearLayout=itemView.findViewById(R.id.video_control);
        logoIV=itemView.findViewById(R.id.logoIV);
        imgdownIV=itemView.findViewById(R.id.imgdownIV);
    }

    public void bind(Context ctx,int position, String videoModel) {
        Glide.with(ctx)
                .load(videoModel)
                .into(logoIV);
        im_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(position,videoModel,v);
            }
        });
        im_pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(position,videoModel,v);
            }
        });

        if (position == videoModel.length() - 1) {
            imgdownIV.setVisibility(View.GONE);
        }
        else {
           imgdownIV.setVisibility(View.VISIBLE);
        }

    }



}
