package com.dailysamachar.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.dailysamachar.app.interfaces.LangInterface;
import com.dailysamachar.app.model.LanguageModel;
import com.dailysamachar.app.R;

import java.util.List;

public class LanguageAdapter extends RecyclerView.Adapter<LanguageAdapter.MyViewHolder> {
    private final Context context;
    List<LanguageModel.State> mArrayList;
    private int row_index;
    private int count = 0;
    private LangInterface langInterface;
    String sel_langID;
    boolean selected = false;

    public LanguageAdapter(Context context, List<LanguageModel.State> mArrayList, LangInterface langInterface, String sel_langID) {
        this.context = context;
        this.mArrayList = mArrayList;
        this.langInterface = langInterface;
        this.sel_langID = sel_langID;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_language_list, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        LanguageModel.State mModel = mArrayList.get(listPosition);

        holder.rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                row_index = listPosition;
                count = 1;
                selected = true;
                notifyDataSetChanged();
            }
        });

        holder.itemLangTV.setText(mModel.getTitle());

        if (row_index == listPosition && count == 1) {
            holder.imgCheckIV.setImageResource(R.drawable.ic_check);
            langInterface.getLang(mModel.getId());
        } else {
            holder.imgCheckIV.setImageResource(R.drawable.ic_uncheck);
        }

        if (selected == false){
            if (mModel.getId().equals(sel_langID)){
                holder.imgCheckIV.setImageResource(R.drawable.ic_check);
                langInterface.getLang(mModel.getId());
            }else {
                holder.imgCheckIV.setImageResource(R.drawable.ic_uncheck);
            }
        }

    }

    @Override
    public int getItemCount() {
        return (mArrayList == null) ? 0 : mArrayList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView itemLangTV;
        ImageView imgCheckIV;
        RelativeLayout rl;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.itemLangTV = itemView.findViewById(R.id.itemLangTV);
            this.imgCheckIV = itemView.findViewById(R.id.imgCheckIV);
            this.rl = itemView.findViewById(R.id.rl);
        }
    }
}
