package com.dailysamachar.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.dailysamachar.app.R;
import com.dailysamachar.app.interfaces.StateInterface;
import com.dailysamachar.app.model.CountryModel;
import com.dailysamachar.app.model.StatesModel;
import com.dailysamachar.app.utils.NewsAppPreferences;
import com.dailysamachar.app.utils.SelectLocationAdapter;

import java.util.List;

public class SelectCountryAdapter extends RecyclerView.Adapter<SelectCountryAdapter.MyViewHolder> {
    private final Context context;
    List<CountryModel.CountryItem> mCountryArrayList;
    private final StateInterface mStateInterface;

    private int row_index;
    private int count = 0;
    //    String sel_stateID;
    String country_name;
    boolean selected = false;

    public SelectCountryAdapter(Context context, List<CountryModel.CountryItem> mCountryArrayList, StateInterface mStateInterface, String country_name) {
        this.context = context;
        this.mCountryArrayList = mCountryArrayList;
        this.mStateInterface = mStateInterface;
        this.country_name = country_name;
    }

    @Override
    public SelectCountryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_states_list, parent, false);
       MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final SelectCountryAdapter.MyViewHolder holder, int listPosition) {
        CountryModel.CountryItem mModel = mCountryArrayList.get(listPosition);
        holder.itemStateTV.setText(mModel.getTitle());
        holder.rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                row_index = listPosition;
                count = 1;
                selected = true;
                notifyDataSetChanged();

            }
        });

        if (row_index == 0 && count == 1) {
            mStateInterface.state(mModel.getTitle());
            holder.imgCheckIV.setImageResource(R.drawable.ic_check);
        } else {
            holder.imgCheckIV.setImageResource(R.drawable.ic_uncheck);
        }


        if (row_index == listPosition && count == 1) {
            mStateInterface.state(mModel.getTitle());
            holder.imgCheckIV.setImageResource(R.drawable.ic_check);
        } else {
            holder.imgCheckIV.setImageResource(R.drawable.ic_uncheck);
        }


        if (!selected) {
            if (mModel.getTitle().equals(country_name)) {
                holder.imgCheckIV.setImageResource(R.drawable.ic_check);
                //mStateInterface.state(mModel.getTitle());
            } else {
                holder.imgCheckIV.setImageResource(R.drawable.ic_uncheck);
            }
        }
        if (country_name != null && !country_name.equals("")) {
            if (NewsAppPreferences.readString(context, NewsAppPreferences.SELECTED_LOCATION, null) != null) {
                if (NewsAppPreferences.readString(context, NewsAppPreferences.SELECTED_LOCATION, null).equals(country_name)) {
                    if (listPosition == 0) {
                        holder.imgCheckIV.setImageResource(R.drawable.ic_check);
                    }
                } else {
                    if (listPosition == 0) {
                        holder.imgCheckIV.setImageResource(R.drawable.ic_uncheck);
                    }
                }
            }

        }

    }

    @Override
    public int getItemCount() {
        return (mCountryArrayList == null) ? 0 : mCountryArrayList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView itemStateTV;
        ImageView imgCheckIV;
        RelativeLayout rl;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.itemStateTV = itemView.findViewById(R.id.itemStateTV);
            this.rl = itemView.findViewById(R.id.rl);
            this.imgCheckIV = itemView.findViewById(R.id.imgCheckIV);
        }
    }

}

