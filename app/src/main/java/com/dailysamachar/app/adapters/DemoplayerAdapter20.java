package com.dailysamachar.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.dailysamachar.app.VideoModel;
import com.dailysamachar.app.model.HomeModel;
import com.dailysamachar.app.R;

import java.util.ArrayList;

public class DemoplayerAdapter20 extends RecyclerView.Adapter<DemoplayerAdapter20.CustomViewHolder >{
    Context ctx;
    String logo;
//    ArrayList<HomeModel.Datum> videoArrayList=new ArrayList<>();
    ArrayList<VideoModel> videoArrayList=new ArrayList<>();
    ArrayList<HomeModel.Datum> videoArrayList2=new ArrayList<>();
    OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int positon, String item, View view);
    }

    public DemoplayerAdapter20(Context context, ArrayList<VideoModel> videoArrayList, ArrayList<HomeModel.Datum> videoArrayList2, String logo, OnItemClickListener onItemClickListener) {
        this.ctx = context;
        this.videoArrayList = videoArrayList;
        this.videoArrayList2=videoArrayList2;
        this.logo=logo;
        this.onItemClickListener=onItemClickListener;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_content3, parent, false);
        return new CustomViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        String videoModel = videoArrayList2.get(position).getLink();
        Glide.with(ctx)
                .load(videoArrayList2.get(position).getLogo())
                .into(holder.logoIV);

        try {
            holder.bind(position, videoModel, onItemClickListener);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return videoArrayList2.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        ImageView im_play;
        ImageView im_pause;
        ImageView logoIV;
        RelativeLayout linearLayout;


        public CustomViewHolder(@NonNull View itemView) {
            super(itemView);
            im_play=itemView.findViewById(R.id.play_video);
            im_pause=itemView.findViewById(R.id.pause_video);
            logoIV=itemView.findViewById(R.id.logoIV);
            linearLayout=itemView.findViewById(R.id.video_control);
        }

        public void bind(int position, String videoModel, OnItemClickListener onItemClickListener) {


            im_play.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(position,videoModel,v);
                }
            });
            im_pause.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(position,videoModel,v);
                }
            });

        }
    }
}
