package com.dailysamachar.app.adapters;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.dailysamachar.app.R;
import com.dailysamachar.app.model.HomeModel;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdOptionsView;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;
import com.facebook.ads.MediaView;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdLayout;
import com.facebook.ads.NativeAdListener;
import com.google.android.ads.nativetemplates.TemplateView;
import com.google.android.gms.ads.AdLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by android-da on 6/7/18.
 */

public class AdsViewHolder extends RecyclerView.ViewHolder {

    //    public LinearLayout ll;
    private InterstitialAd interstitialAd;
    InterstitialAdListener interstitialAdListener;
    private NativeAd nativeAd;

    RelativeLayout crossRl;
    public RelativeLayout native_ad_LL;
    public NativeAdLayout native_ad_container;
    public TemplateView my_template;
    private ColorDrawable background;
    AdLoader adLoader;

    public AdsViewHolder(View itemView) {
        super(itemView);
        //ll = itemView.findViewById(R.id.ll);
        native_ad_LL = itemView.findViewById(R.id.native_ad_LL);
        native_ad_container = itemView.findViewById(R.id.native_ad_container);
        my_template = itemView.findViewById(R.id.my_template);
        crossRl = itemView.findViewById(R.id.crossRl);
    }

//    @SuppressLint("MissingPermission")
//    public void bindData(final Context context, ArrayList<HomeModel.Datum> newsList, int position, HomeModel.Datum videoModel) {
//        MobileAds.initialize(context);
//        // ca-app-pub-3940256099942544/2247696110
//       // ca-app-pub-3923747521837403/8876949173
//        adLoader = new AdLoader.Builder(context, "ca-app-pub-3923747521837403/8494062952")
//
//                .forNativeAd(new NativeAd.OnNativeAdLoadedListener() {
//
//                    @Override
//                    public void onNativeAdLoaded(NativeAd nativeAd) {
//                        if (adLoader.isLoading()) {
//                            Log.e("TAG", "onAdFailedToLoadmm: "+"working");
////                            Toast.makeText(context, "work", Toast.LENGTH_SHORT).show();
//
//                            NativeTemplateStyle styles = new
//                                    NativeTemplateStyle.Builder().withMainBackgroundColor(background).build();
//                            TemplateView template = itemView.findViewById(R.id.my_template);
//
//                            template.setNativeAd(nativeAd);
//
//                            template.removeAllViews();
//                            template.addView(template);
//
//                            template.setStyles(styles);
//
//                        }
//                        else
//                        {
////                            Toast.makeText(context, "woghrk", Toast.LENGTH_SHORT).show();
//
//                        }
//
//                    }
//
//                }).withAdListener(new AdListener() {
//                    @Override
//                    public void onAdFailedToLoad(LoadAdError adError) {
//                        Log.e("TAG", "onAdFailedToLoadmm: "+adError.toString());
////                        Toast.makeText(context, adError.toString(), Toast.LENGTH_SHORT).show();
//
//                        // Handle the failure by logging, altering the UI, and so on.
//                       // Toast.makeText(context, adError.getMessage(), Toast.LENGTH_SHORT).show();
//                    }
//                })
//                .withNativeAdOptions(new NativeAdOptions.Builder()
//                        // Methods in the NativeAdOptions.Builder class can be
//                        // used here to specify individual options settings.
//                        .build())
//
//                .build();
//
//        adLoader.loadAd(new AdRequest.Builder().build());
//
//
//    }


    public void bindData(final Context context, ArrayList<HomeModel.Datum> newsList, int position, HomeModel.Datum videoModel) {
        nativeAd = new NativeAd(context, "445117679825548_975482623455715");
//        445117679825548_975482623455715
//        IMG_16_9_LINK#YOUR_PLACEMENT_ID

        NativeAdListener nativeAdListener = new NativeAdListener() {
            @Override
            public void onMediaDownloaded(Ad ad) {
                Log.e("Eror", "onError: "+ad );
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                Log.e("Eror", "onError: "+adError.getErrorMessage() );
            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Race condition, load() called again before last ad was displayed
                if (nativeAd == null || nativeAd != ad) {
                    return;
                }
                // Inflate Native Ad into Container
                inflateAd(nativeAd,context);
            }

            @Override
            public void onAdClicked(Ad ad) {

            }

            @Override
            public void onLoggingImpression(Ad ad) {

            }
        };

        // Request an ad
        nativeAd.loadAd(
                nativeAd.buildLoadAdConfig()
                        .withAdListener(nativeAdListener)
                        .build());}
    private LinearLayout adView;
    private void inflateAd(NativeAd nativeAd, Context context) {

        nativeAd.unregisterView();

        // Add the Ad view into the ad container.

        LayoutInflater inflater = LayoutInflater.from(itemView.getContext());
        // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
        adView = (LinearLayout) inflater.inflate(R.layout.native_ad_layout, native_ad_LL, false);
        native_ad_container.addView(adView);

        // Add the AdOptionsView
        LinearLayout adChoicesContainer = itemView.findViewById(R.id.ad_choices_container);
        AdOptionsView adOptionsView = new AdOptionsView(adChoicesContainer.getContext(), nativeAd, native_ad_container);
        adChoicesContainer.removeAllViews();
        adChoicesContainer.addView(adOptionsView, 0);

        // Create native UI using the ad metadata.
        MediaView nativeAdIcon = adView.findViewById(R.id.native_ad_icon);
        TextView nativeAdTitle = adView.findViewById(R.id.native_ad_title);
        MediaView nativeAdMedia = adView.findViewById(R.id.native_ad_media);
        TextView nativeAdSocialContext = adView.findViewById(R.id.native_ad_social_context);
        TextView nativeAdBody = adView.findViewById(R.id.native_ad_body);
        TextView sponsoredLabel = adView.findViewById(R.id.native_ad_sponsored_label);
        Button nativeAdCallToAction = adView.findViewById(R.id.native_ad_call_to_action);

        // Set the Text.
        nativeAdTitle.setText(nativeAd.getAdvertiserName());
        nativeAdBody.setText(nativeAd.getAdBodyText());
        nativeAdSocialContext.setText(nativeAd.getAdSocialContext());
        nativeAdCallToAction.setVisibility(nativeAd.hasCallToAction() ? View.VISIBLE : View.INVISIBLE);
        nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
        sponsoredLabel.setText(nativeAd.getSponsoredTranslation());

        // Create a list of clickable views
        List<View> clickableViews = new ArrayList<>();
        clickableViews.add(nativeAdTitle);
        clickableViews.add(nativeAdCallToAction);

        // Register the Title and CTA button to listen for clicks.
        nativeAd.registerViewForInteraction(
                adView, nativeAdMedia, nativeAdIcon, clickableViews);
    }}




//        interstitialAd = new InterstitialAd(context, "445117679825548_988855588785085");
//        interstitialAdListener = new InterstitialAdListener() {
//            @Override
//            public void onInterstitialDisplayed(Ad ad) {
//                Log.e(TAG, "Interstitial ad displayed.");
//            }
//
//            @Override
//            public void onInterstitialDismissed(Ad ad) {
//                Log.e(TAG, "Interstitial ad dismissed.");
//            }
//
//            @Override
//            public void onError(Ad ad, com.facebook.ads.AdError adError) {
//
//                Log.d(TAG, "Interstitial ad is loaded and ready to be displayed!"+adError.getErrorMessage());
//            }
//
//            @Override
//            public void onAdLoaded(Ad ad) {
//
//                Log.d(TAG, "Interstitial ad is loaded and ready to be displayed!");
//
//                interstitialAd.show();
//            }
//
//            @Override
//            public void onAdClicked(Ad ad) {
//
//                Log.d(TAG, "Interstitial ad clicked!");
//            }
//
//            @Override
//            public void onLoggingImpression(Ad ad) {
//
//                Log.d(TAG, "Interstitial ad impression logged!");
//            }
//        };
//
//        interstitialAd.loadAd(
//                interstitialAd.buildLoadAdConfig()
//                        .withAdListener(interstitialAdListener)
//                        .build());
//
//    }

// }
