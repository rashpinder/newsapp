package com.dailysamachar.app.adapters;

import static androidx.constraintlayout.motion.utils.Oscillator.TAG;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.dailysamachar.app.R;
import com.dailysamachar.app.interfaces.AdsInterface;
import com.dailysamachar.app.interfaces.MuteInterface;
import com.dailysamachar.app.model.HomeModel;

import java.util.ArrayList;


public class DemoplayerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context ctx;
    String logo;
    String last_page;
    public static boolean img_fav;
    ArrayList<HomeModel.Datum> newsList;
    OnItemClickListener onItemClickListener;
    PaginationInterface paginationInterface;
    AdsInterface adsInterface;
    MuteInterface muteInterface;
    private static final int POST_TYPE = 1;
    private static final int AD_TYPE = 0;

    public interface OnItemClickListener {
        void onItemClick(int positon, String item, View view);
    }

    public interface PaginationInterface {
        void mPaginationInterface(boolean isLastScrolled);
    }

    public DemoplayerAdapter(Context context, ArrayList<HomeModel.Datum> newsList, String logo, OnItemClickListener onItemClickListener, PaginationInterface paginationInterface, AdsInterface adsInterface, String last_page, MuteInterface muteInterface) {
        this.ctx = context;
        this.newsList = newsList;
//        this.newsList2=newsList2;
        this.logo = logo;
        this.last_page = last_page;
        this.onItemClickListener = onItemClickListener;
        this.paginationInterface = paginationInterface;
        this.adsInterface = adsInterface;
        this.muteInterface = muteInterface;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == POST_TYPE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_content3, parent, false);
            return new CustomViewHolder(view);

        } else if (viewType == AD_TYPE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_native_ads, parent, false);
            return new AdsViewHolder(view);
        }
//        else{
//           return null;
//        }
        return null;
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") int position) {

        try {
            HomeModel.Datum videoModel = newsList.get(position);

            if (holder.getItemViewType() == AD_TYPE) {
                ((AdsViewHolder) holder).bindData(ctx, newsList, position, videoModel);


            } else if (holder.getItemViewType() == POST_TYPE) {
                ((CustomViewHolder) holder).txtChannelNameTV.setText(videoModel.getTitle());
                ((CustomViewHolder) holder).bind(position, videoModel.getLink(), onItemClickListener);
            }

            if (position >= newsList.size() - 1) {
                paginationInterface.mPaginationInterface(true);
            }

            if (videoModel.getLast().equalsIgnoreCase("true")) {
                newsList.addAll(newsList);
            }
            if (holder.getItemViewType() == AD_TYPE) {
                ((AdsViewHolder) holder).crossRl.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        for (int j = 0; j <= newsList.size(); j++) {

                            if (newsList.get(position).isIS_AD()) {
                                newsList.add(j, videoModel);
                                newsList.remove(j);

                                notifyItemRemoved(position);
                                notifyItemRangeChanged(position, getItemCount());
                                notifyItemInserted(position + 1);

                            }
                        }
                    }

                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
//        if (newsList.size() > 0) {
//            return newsList.size() + Math.round(newsList.size() / 5);
//        }
        return newsList.size();
    }

    @Override
    public int getItemViewType(int position) {
//        if (position == AD_TYPE) {
//            return POST_TYPE;
//        } else if ((position + 1) % 5 == 0)
//            return AD_TYPE;
//        else {
//            return POST_TYPE;
//        }
        if (newsList.get(position).isIS_AD()) {
            return AD_TYPE;
        } else {
            return POST_TYPE;
        }
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        ImageView im_play;
        ImageView im_pause;
        RelativeLayout linearLayout;
        ImageView logoIV;
        ImageView imgdownIV;
        ImageView imgMuteIV;
        TextView txtChannelNameTV;

        public CustomViewHolder(@NonNull View itemView) {
            super(itemView);
            im_play = itemView.findViewById(R.id.play_video);
            im_pause = itemView.findViewById(R.id.pause_video);
            linearLayout = itemView.findViewById(R.id.video_control);
            logoIV = itemView.findViewById(R.id.logoIV);
            imgdownIV = itemView.findViewById(R.id.imgdownIV);
            imgMuteIV = itemView.findViewById(R.id.imgMuteIV);
            txtChannelNameTV = itemView.findViewById(R.id.txtChannelNameTV);
        }

        public void bind(int position, String videoModel, OnItemClickListener onItemClickListener) {
            Glide.with(ctx)
                    .load(newsList.get(position).getLogo())
                    .into(logoIV);
            im_play.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(position, videoModel, v);
                }

            });


            Log.e(TAG, "video Model" +videoModel);

            im_pause.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(position, videoModel, v);
                }
            });
            imgMuteIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//
                }
            });
        }
    }
}

//package com.dailysamachar.app.adapters;

//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//
//import androidx.annotation.NonNull;
//import androidx.recyclerview.widget.RecyclerView;
//
//import com.dailysamachar.app.R;
//import com.dailysamachar.app.interfaces.AdsInterface;
//import com.dailysamachar.app.model.HomeModel;
//
//import java.util.ArrayList;
//
//
//public class DemoplayerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
//    Context ctx;
//    String logo;
//    public static boolean img_fav;
//    ArrayList<HomeModel.Datum> newsList = new ArrayList<>();
//    OnItemClickListener onItemClickListener;
//    PaginationInterface paginationInterface;
//    AdsInterface adsInterface;
//
//    public interface OnItemClickListener {
//        void onItemClick(int positon, String item, View view);
//    }
//
//    public interface PaginationInterface {
//        void mPaginationInterface(boolean isLastScrolled);
//    }
//
//    public DemoplayerAdapter(Context context, ArrayList<HomeModel.Datum> newsList, String logo, OnItemClickListener onItemClickListener, PaginationInterface paginationInterface, AdsInterface adsInterface) {
//        this.ctx = context;
//        this.newsList = newsList;
//        this.logo = logo;
//        this.onItemClickListener = onItemClickListener;
//        this.paginationInterface = paginationInterface;
//        this.adsInterface = adsInterface;
//    }
//
//    @NonNull
//    @Override
//    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        if (viewType == 1) {
//            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_content3, parent, false);
//            return new CustomViewHolder(view);
//        } else if (viewType == 0) {
//            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_content3, parent, false);
//            return new AdsViewHolder(view);
//        }
//        return null;
//    }
//
//    @Override
//    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
//        HomeModel.Datum videoModel = newsList.get(position);
//        try {
//            if (holder.getItemViewType() == 0) {
//                ((AdsViewHolder) holder).bindData(ctx);
//            } else if (holder.getItemViewType() == 1) {
//                ((CustomViewHolder) holder).bind(ctx, position, videoModel.getLink());
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//
//        if (position >= newsList.size() - 1) {
//            paginationInterface.mPaginationInterface(true);
//        }
//    }
//
//
//    @Override
//    public int getItemViewType(int position) {
//        if (position % 4 == 0)
//            return 0;
//        else {
//            return 1;
//        }
//    }
//
//
//    @Override
//    public int getItemCount() {
//        return (newsList == null) ? 0 : newsList.size();
//    }
//}
