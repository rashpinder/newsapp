package com.dailysamachar.app.interfaces;

import com.dailysamachar.app.model.AddDeviceTokenModel;
import com.dailysamachar.app.model.CountryModel;
import com.dailysamachar.app.model.FavUnfavModel;
import com.dailysamachar.app.model.ForgotPasswordModel;
import com.dailysamachar.app.model.HomeModel;
import com.dailysamachar.app.model.LanguageModel;
import com.dailysamachar.app.model.ProfileModel;
import com.dailysamachar.app.model.SignUpModel;
import com.dailysamachar.app.model.StatesModel;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface ApiInterface {

    String BASE_URL = "https://www.dharmani.com/NewsApp/webservices/";
    //String BASE_URL = "http://161.97.132.85/NewsApp/webservices/";


    @POST("signUp.php")
    Call<SignUpModel> signUpRequest(@Body Map<String, String> mParams);

//    @POST("logIn.php")
//    Call<SignUpModel> loginRequest(@Body Map<String, String> mParams);

    @POST("loginv2.php")
    Call<SignUpModel> loginRequest(@Body Map<String, String> mParams);


    @POST("forgetPassword.php")
    Call<ForgotPasswordModel> forgotPasswordRequest(@Body Map<String, String> mParams);

    @POST("home_v2.php")
   // Call<HomeModel> newsListRequest(@Body Map<String, String> mParams);
    Call<HomeModel> newsListRequest(@Header("token") String header,@Body Map<String, String> mParams);

    @POST("googleLogin.php")
    Call<SignUpModel> loginWithGoogleRequest(@Body Map<String, String> mParams);

    @POST("facebookLogin.php")
    Call<SignUpModel> loginWithFbRequest(@Body Map<String, String> mParams);

    @POST("getProfileDetails_v2.php")
    Call<ProfileModel> getProfileDetailsRequest(@Header("token") String header,@Body Map<String, String> mParams);

    @POST("favouriteUnfavouriteNews_v2.php")
    Call<FavUnfavModel> favUnfavRequest(@Header("token") String header,@Body Map<String, String> mParams);

    @POST("phoneNoLogin.php")
    Call<SignUpModel> phoneNoRequest(@Body Map<String, String> mParams);

    @POST("editProfile.php")
    Call<ProfileModel> editProfileRequest(@Body Map<String, String> mParams);

    @POST("getAllStates.php")
    Call<StatesModel> getStatesRequest();

    @POST("getAllLanguage.php")
    Call<LanguageModel> getLanguageRequest(@Body Map<String, String> mParams);

    @POST("updateLanguage.php")
    Call<ForgotPasswordModel> updateLanguageRequest(@Body Map<String, String> mParams);

    @POST("logOut.php")
    Call<ForgotPasswordModel> logoutRequest(@Body Map<String, String> mParams);

    @POST("DeleteAccount.php")
    Call<ForgotPasswordModel> deleteAccount(@Body Map<String, String> mParams);

    @POST("getAllStates_v2.php")
    Call<CountryModel> getCountryRequest();

    @POST("addDeviceToken.php")
    Call<AddDeviceTokenModel> addDeviceToken(
            @Body Map<String, String> mParams);



//    @Multipart
//    @POST("editProfile.php")
//    Call<ForgotPasswordModel> editProfileRequest(@Part("userID") RequestBody userID,
//                                                 @Part("name") RequestBody name,
//                                                 @Part("bio") RequestBody bio,
//                                                 @Part MultipartBody.Part profileImage);


//    Call<ForgotPasswordModel> editProfileRequest(Map<String, String> mEditProfileParam);
}