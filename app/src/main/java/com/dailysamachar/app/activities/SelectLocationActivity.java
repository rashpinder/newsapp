package com.dailysamachar.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dailysamachar.app.RetrofitApi.ApiClient;
import com.dailysamachar.app.interfaces.ApiInterface;
import com.dailysamachar.app.interfaces.StateInterface;
import com.dailysamachar.app.model.StatesModel;
import com.dailysamachar.app.utils.NewsAppPreferences;
import com.dailysamachar.app.utils.SelectLocationAdapter;
import com.dailysamachar.app.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SelectLocationActivity extends BaseActivity implements StateInterface {
    /**
     * Getting the Current Class Name
     */
    String TAG = SelectLocationActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = SelectLocationActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.selectedLocationRV)
    RecyclerView selectedLocationRV;
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    List<StatesModel.State> mStateArrayList;
    SelectLocationAdapter mAdapter;
    StateInterface stateInterface;
    String state_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_location);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        stateInterface = this;
        getStatesList();
    }

    private void getStatesList() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            if (mStateArrayList != null) {
                mStateArrayList.clear();
            }
            executeDropDownApi();
        }
    }

    private void executeDropDownApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getStatesRequest().enqueue(new Callback<StatesModel>() {
            @Override
            public void onResponse(Call<StatesModel> call, Response<StatesModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                StatesModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    mStateArrayList = new ArrayList<>();
                    mStateArrayList = mModel.getStates();

                    setStatesAdapter();

                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
            }
            }


            @Override
            public void onFailure(Call<StatesModel> call, Throwable t) {
                dismissProgressDialog();
                t.printStackTrace();
            }
        });
    }

    private void setStatesAdapter() {
        mAdapter = new SelectLocationAdapter(mActivity, mStateArrayList,stateInterface, NewsAppPreferences.readString(mActivity, NewsAppPreferences.STATE,state_name));
        selectedLocationRV.setLayoutManager(new LinearLayoutManager(mActivity));
        selectedLocationRV.setAdapter(mAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @OnClick({R.id.imgBackIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackIV:
                performBackClick();
                break;
        }
    }

    private void performBackClick() {
        startActivity(new Intent(mActivity, LocationActivity.class));
        finish();
//        onBackPressed();
    }

    @Override
    public void state(String title) {
        state_name=title;
        NewsAppPreferences.writeString(mActivity, NewsAppPreferences.STATE,title);
        NewsAppPreferences.writeString(mActivity, NewsAppPreferences.SELECT_STATE_FROM_STATE_DROPDOWN,title);
        Intent intent=new Intent(mActivity, LocationActivity.class);
        intent.putExtra("state",state_name);
        startActivity(intent);
        finish();
    }
}