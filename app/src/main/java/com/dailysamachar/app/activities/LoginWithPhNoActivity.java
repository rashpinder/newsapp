package com.dailysamachar.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.dailysamachar.app.utils.Constants;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthSettings;
import com.google.firebase.auth.FirebaseUser;
import com.dailysamachar.app.R;
import com.ybs.countrypicker.CountryPicker;
import com.ybs.countrypicker.CountryPickerListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginWithPhNoActivity extends BaseActivity {
    Activity mActivity = LoginWithPhNoActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = LoginWithPhNoActivity.this.getClass().getSimpleName();

    CountryPicker ccp;
    /*
     * Widgets
     * */
    @BindView(R.id.editPhoneNumberET)
    EditText editPhoneNumberET;
    @BindView(R.id.txtCountryCodeTV)
    TextView txtCountryCodeTV;
    @BindView(R.id.txtSendTV)
    TextView txtSendTV;
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    String mDialCode = "+91";

    String phoneNumber = "", countryCode = "";

    FirebaseAuth auth = FirebaseAuth.getInstance();
    FirebaseAuthSettings firebaseAuthSettings = auth.getFirebaseAuthSettings();
    FirebaseUser user = auth.getCurrentUser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_with_ph_no);
        ButterKnife.bind(this);
        setStatusBar(mActivity);

        /*Set Country Code Picker*/

    }

    private void performCountryCodeClick() {
        CountryPicker picker = CountryPicker.newInstance("Select Country");  // dialog title
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID) {
                // Implement your code here
                //   imgFlagIV.setImageResource(flagDrawableResID);
                txtCountryCodeTV.setText(dialCode);
                mDialCode = dialCode;
                picker.dismiss();
            }
        });
        picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
    }


    private void performContinueClick() {
        phoneNumber = mDialCode + editPhoneNumberET.getText().toString().trim();

        if (phoneNumber.equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_phone));
        } else if (editPhoneNumberET.length() < 10) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_phone));
        } else {
            Intent intent = new Intent(mActivity, VerifyOtpActivity.class);
            intent.putExtra( Constants.PHONE_NUMBER,phoneNumber);
            startActivity(intent);
            finish();
        }
    }

    @OnClick({R.id.txtSendTV,R.id.txtCountryCodeTV,R.id.imgBackIV})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtSendTV:
                performContinueClick();
                break;
                case R.id.txtCountryCodeTV:
                    performCountryCodeClick();
                break;
                case R.id.imgBackIV:
                   onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(mActivity, LoginActivity.class));
        finish();
    }
}
