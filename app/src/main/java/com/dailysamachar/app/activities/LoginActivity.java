package com.dailysamachar.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dailysamachar.app.RetrofitApi.ApiClient;
import com.dailysamachar.app.interfaces.ApiInterface;
import com.dailysamachar.app.model.SignUpModel;
import com.dailysamachar.app.utils.NewsAppPreferences;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.dailysamachar.app.R;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;

public class LoginActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    /**
     * Getting the Current Class Name
     */
    String TAG = LoginActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = LoginActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.editEmailET)
    EditText editEmailET;
    @BindView(R.id.editPasswordET)
    EditText editPasswordET;
    @BindView(R.id.txtLoginTV)
    TextView txtLoginTV;
    @BindView(R.id.txtForgotPassTV)
    TextView txtForgotPassTV;
    @BindView(R.id.imgGoogleIV)
    ImageView imgGoogleIV;
    @BindView(R.id.imgFbIV)
    ImageView imgFbIV;
    @BindView(R.id.imgCallIV)
    ImageView imgCallIV;
    @BindView(R.id.txtDontHaveAccountTV)
    TextView txtDontHaveAccountTV;

    private URL fbProfilePicture;
    String FacebookProfilePicture = "";


    GoogleSignInClient mGoogleSignInClient;
    protected static final int GOOGLE_SIGN_IN = 12324;
    protected GoogleApiClient mGoogleApiClient;

    CallbackManager mCallbackManager;
    //Firebase
    private FirebaseAuth mFirebaseAuth;
    String GoogleProfilePicture = "";
    private String fbEmail, fbLastName, fbFirstName, fbId, userName, fbSocialUserserName;
    String Username = "";

    String facebook_id = "";
    String google_id = "";
    String email = "";
    String google_username = "";

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        buildGoogleApiClient();
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        // Initialize Facebook Login button
        FirebaseApp.initializeApp(this);

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mFirebaseAuth = FirebaseAuth.getInstance();
        mCallbackManager = CallbackManager.Factory.create();

        //Logout First
        FirebaseAuth.getInstance().signOut();
        LoginManager.getInstance().logOut();
        getDeviceToken();
    }


    @OnClick({R.id.txtLoginTV, R.id.txtForgotPassTV, R.id.imgGoogleIV, R.id.imgFbIV, R.id.imgCallIV,R.id.txtDontHaveAccountTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtLoginTV:
                performLoginClick();
                break;
            case R.id.txtForgotPassTV:
                performForgotPassClick();
                break;
            case R.id.imgGoogleIV:
                signIn();
                break;
            case R.id.imgFbIV:
                performFbClick();
                break;
            case R.id.imgCallIV:
                performCallClick();
                break;
                case R.id.txtDontHaveAccountTV:
                performSignUpClick();
                break;
        }
    }

    private void performSignUpClick() {
        Intent intent = new Intent(mActivity, SignUpActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private void performCallClick() {
        Intent intent = new Intent(mActivity, LoginWithPhNoActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }


    private void performLoginClick() {
        if (isValidate()) {
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.internet_connection_error));
            } else {
                executeLoginApi();
            }
        }
    }


    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("email", editEmailET.getText().toString().trim());
        mMap.put("password", editPasswordET.getText().toString().trim());
        mMap.put("deviceType", "2");
        mMap.put("deviceToken",strDeviceToken);
        Log.e(TAG, "**PARAM**" + mMap);
        return mMap;
    }


    private void executeLoginApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.loginRequest(mParam()).enqueue(new retrofit2.Callback<SignUpModel>() {
            @Override
            public void onResponse(Call<SignUpModel> call, Response<SignUpModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                SignUpModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1"))
                {
                    showToast(mActivity, mModel.getMessage());
                    NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.ISLOGIN, true);
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.ID, mModel.getData().getUserID());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.EMAIL, mModel.getData().getEmail());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.VERIFIED, mModel.getData().getVerified());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.ADMIN, mModel.getData().getAdmin());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.CREATION_AT, mModel.getData().getCreated());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.PASSWORD, response.body().getData().getPassword());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.IMAGE, response.body().getData().getProfileImage());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.FB_TOKEN, response.body().getData().getFacebookToken());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.GOOGLE_TOKEN, response.body().getData().getGoogleToken());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.PH_NO, response.body().getData().getPhoneNo());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.BIO, response.body().getData().getBio());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.USERNAME, response.body().getData().getName());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.VER_CODE, response.body().getData().getVerificationCode());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.AUTH_TOKEN, response.body().getData().getAuthToken());
                     Log.e(TAG,"AUTH_TOKEN::"+ response.body().getData().getAuthToken());
                    Intent intent = new Intent(mActivity, HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                    Log.e(TAG, "Profile..." + response.body().getData().getProfileImage());

                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SignUpModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    /*
     * Set up validations for Sign In fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editEmailET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_email_address));
            flag = false;
        } else if (!isValidEmaillId(editEmailET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address));
            flag = false;
        } else if (editPasswordET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password));
            flag = false;
        }
        return flag;
    }

    private void performForgotPassClick() {
        startActivity(new Intent(mActivity, ForgotPasswordActivity.class));
    }
    /**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the
     * LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        Log.i("", "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private void signIn() {
//        signOut();
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, GOOGLE_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case GOOGLE_SIGN_IN:
                Task<GoogleSignInAccount> completedTask = GoogleSignIn.getSignedInAccountFromIntent(data);
                handleSignInResult(completedTask);
                break;
            default:
                mCallbackManager.onActivityResult(requestCode, resultCode, data);
                break;

        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            assert account != null;
            if (account.getEmail()!=null) {
            email = account.getEmail();}
            if (account.getId()!=null){
            google_id = account.getId();}
            if (account.getDisplayName()!=null) {
                google_username = account.getDisplayName();
            }

            if (account.getPhotoUrl() != null) {
                GoogleProfilePicture = account.getPhotoUrl().toString();
//                    mBase64GoogleImage=convertUrlToBase64(GoogleProfilePicture);
            } else {
                GoogleProfilePicture = "";
//                    mBase64GoogleImage="";
            }

            executeGoogleApii();

        } catch (ApiException e) {
            Log.e("MyTAG", "signInResult:failed code=" + e.getStatusCode());
            Toast.makeText(mActivity, "Failed", Toast.LENGTH_LONG);

        }
    }

    private void executeGoogleApii() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            loginWithGoogle();
        }
    }

    /*
     * Execute google api
     * */
    private Map<String, String> mGoogleParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("email", email);
        mMap.put("userName", google_username);
        mMap.put("googleToken", google_id);
        mMap.put("deviceType", "2");
        mMap.put("deviceToken",strDeviceToken);
        mMap.put("image", GoogleProfilePicture);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void loginWithGoogle() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.loginWithGoogleRequest(mGoogleParams()).enqueue(new retrofit2.Callback<SignUpModel>() {
            @Override
            public void onResponse(Call<SignUpModel> call, Response<SignUpModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                SignUpModel mModel = response.body();
                if (mModel.getStatus().equalsIgnoreCase("1")) {
                    showToast(mActivity, mModel.getMessage());
                    NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.ISLOGIN, true);
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.ID, mModel.getData().getUserID());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.EMAIL, mModel.getData().getEmail());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.VERIFIED, mModel.getData().getVerified());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.ADMIN, mModel.getData().getAdmin());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.CREATION_AT, mModel.getData().getCreated());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.PASSWORD, response.body().getData().getPassword());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.IMAGE, response.body().getData().getProfileImage());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.FB_TOKEN, response.body().getData().getFacebookToken());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.GOOGLE_TOKEN, response.body().getData().getGoogleToken());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.PH_NO, response.body().getData().getPhoneNo());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.BIO, response.body().getData().getBio());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.USERNAME, response.body().getData().getName());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.VER_CODE, response.body().getData().getVerificationCode());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.AUTH_TOKEN, response.body().getData().getAuthToken());

                    Intent intent = new Intent(mActivity, HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
                else{
                    showAlertDialog(mActivity,mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SignUpModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void loginWithFacebook() {
        if (isNetworkAvailable(mActivity)) {
            LoginManager.getInstance().logInWithReadPermissions(mActivity, Arrays.asList("public_profile", "email"));
            LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    Log.d("onSuccess: ", loginResult.getAccessToken().getToken());
                    getFacebookData(loginResult);
                }

                @Override
                public void onCancel() {
                    Toast.makeText(mActivity, "Cancel", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(FacebookException error) {
                    Toast.makeText(mActivity, error.toString(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error));
        }
    }

    private void getFacebookData(LoginResult loginResult) {
        showProgressDialog(mActivity);
        GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                dismissProgressDialog();
                try {

                    if (object.has("id")) {
                        fbId = object.getString("id");
                        Log.e("LoginActivity", "id" + fbId);
                    }

                    //check permission first userName
                    if (object.has("first_name")) {
                        fbFirstName = object.getString("first_name");
                        Log.e("LoginActivity", "first_name" + fbFirstName);

                    }
                    //check permisson last userName
                    if (object.has("last_name")) {
                        fbLastName = object.getString("last_name");
                        Log.e("LoginActivity", "last_name" + fbLastName);
                    }
                    //check permisson email
                    if (object.has("email")) {
                        fbEmail = object.getString("email");
                        Log.e("LoginActivity", "email" + fbEmail);
                    }

                    JSONObject jsonObject = new JSONObject(object.getString("picture"));
                    if (jsonObject != null) {
                        JSONObject dataObject = jsonObject.getJSONObject("data");
                        Log.e("Loginactivity", "json oject get picture" + dataObject);
                        fbProfilePicture = new URL("https://graph.facebook.com/" + fbId + "/picture?width=500&height=500");
                        Log.e("LoginActivity", "json object=>" + object.toString());
                    }
                    if (fbProfilePicture != null) {
                        FacebookProfilePicture = String.valueOf(fbProfilePicture);

                    } else {
                        FacebookProfilePicture = "";
                    }

                    fbSocialUserserName = fbFirstName + " " + fbLastName;

                    Username = fbSocialUserserName;
                    email = fbEmail;
                    facebook_id = fbId;

                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.FACEBOOK_IDD, facebook_id);

                    executeLoginWithFbApi();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        Bundle bundle = new Bundle();
        Log.e("LoginActivity", "bundle set");
        bundle.putString("fields", "id, first_name, last_name,email,picture");
        graphRequest.setParameters(bundle);
        graphRequest.executeAsync();
    }

    /*
     * Execute fb api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("email", fbEmail);
        mMap.put("userName", Username);
        mMap.put("facebookToken", facebook_id);
        mMap.put("deviceType", "2");
        mMap.put("deviceToken",strDeviceToken);
        mMap.put("image", FacebookProfilePicture);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeLoginWithFbApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.loginWithFbRequest(mParams()).enqueue(new retrofit2.Callback<SignUpModel>() {
            @Override
            public void onResponse(Call<SignUpModel> call, Response<SignUpModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                SignUpModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    showToast(mActivity, mModel.getMessage());
                    NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.ISLOGIN, true);
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.ID, mModel.getData().getUserID());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.EMAIL, mModel.getData().getEmail());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.VERIFIED, mModel.getData().getVerified());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.ADMIN, mModel.getData().getAdmin());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.CREATION_AT, mModel.getData().getCreated());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.PASSWORD, response.body().getData().getPassword());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.IMAGE, response.body().getData().getProfileImage());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.FB_TOKEN, response.body().getData().getFacebookToken());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.GOOGLE_TOKEN, response.body().getData().getGoogleToken());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.PH_NO, response.body().getData().getPhoneNo());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.BIO, response.body().getData().getBio());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.USERNAME, response.body().getData().getName());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.VER_CODE, response.body().getData().getVerificationCode());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.AUTH_TOKEN, response.body().getData().getAuthToken());

                    Intent intent = new Intent(mActivity, HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else{
                    showAlertDialog(mActivity,mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SignUpModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    // [START signOut]
    private void signOut() {
        if (mGoogleSignInClient != null) {
            mGoogleSignInClient.signOut()
                    .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            // [START_EXCLUDE]
                            Log.e(TAG, "==Logout Successfully==");
                            // [END_EXCLUDE]
                        }
                    });
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void performFbClick() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            FacebookSdk.sdkInitialize(getApplicationContext());
            LoginManager.getInstance().logOut();
            AppEventsLogger.activateApp(getApplication());
            mCallbackManager = CallbackManager.Factory.create();
            loginWithFacebook();
        }
    }

//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        if (progressDialog != null && progressDialog.isShowing()) {
//            progressDialog.cancel();
//        }
//    }
}
