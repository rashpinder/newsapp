package com.dailysamachar.app.activities;


import static androidx.constraintlayout.motion.utils.Oscillator.TAG;
import static com.dailysamachar.app.activities.LocationActivity.KEY_LOCATION;
import static com.dailysamachar.app.activities.LocationActivity.KEY_REQUESTING_LOCATION_UPDATES;
import static com.dailysamachar.app.utils.Constants.REQUEST_PERMISSION_CODE;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.dailysamachar.app.Config;
import com.dailysamachar.app.R;
import com.dailysamachar.app.RetrofitApi.ApiClient;
import com.dailysamachar.app.adapters.DemoplayerAdapter;
import com.dailysamachar.app.interfaces.AdsInterface;
import com.dailysamachar.app.interfaces.ApiInterface;
import com.dailysamachar.app.interfaces.MuteInterface;
import com.dailysamachar.app.model.FavUnfavModel;
import com.dailysamachar.app.model.ForgotPasswordModel;
import com.dailysamachar.app.model.HomeModel;
import com.dailysamachar.app.utils.Constants;
import com.dailysamachar.app.utils.NewsAppPreferences;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.facebook.ads.AudienceNetworkAds;
import com.google.android.ads.nativetemplates.TemplateView;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.material.navigation.NavigationView;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends YouTubeBaseActivity implements DemoplayerAdapter.PaginationInterface,
        com.dailysamachar.app.interfaces.posInterface, AdsInterface, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, ResultCallback<LocationSettingsResult> {
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    private static final int REQUEST_CHECK_SETTINGS = 0x1;
    public static boolean img_fav;
    public static int a = 0;
    public static int var;
    public Dialog progressDialog;
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer_layout;
    @BindView(R.id.imgFavoriteIV)
    ImageView imgFavoriteIV;
    @BindView(R.id.mProgressBar)
    ProgressBar mProgressBar;
    LinearLayoutManager layoutManager;
    int currentPage = -1;
    String address;
    Activity mActivity = HomeActivity.this;
    YouTubePlayer youTubePlayer2;
    YouTubePlayerView youTubeView;
    File imagePath;
    String item;
    Intent intent;
    String videoName;
    ImageView img;
    String newsId;
    List<HomeModel.Datum> mNewsList = new ArrayList<>();
    com.dailysamachar.app.interfaces.posInterface posInterface;
    DemoplayerAdapter.PaginationInterface paginationInterface;
    AdsInterface adsInterface;
    MuteInterface muteInterface;
    boolean terms = true;
    String logo;
    String fav;
    int pos;
    @BindView(R.id.recylerview_frag_home)
    RecyclerView recyclerView;
    @BindView(R.id.topRL)
    RelativeLayout topRL;
    @BindView(R.id.imgMenuIV)
    ImageView imgMenuIV;
    @BindView(R.id.locRL)
    RelativeLayout locRL;
    @BindView(R.id.txtCountryTV)
    TextView txtCountryTV;

    LinearLayout homeLL;
    ImageView imgHomeIV;
    TextView txtHomeTV;
    LinearLayout locationLL;
    ImageView imgLocIV;
    TextView txtLocationTV;
    LinearLayout favLL;
    ImageView imgFavIV;
    TextView txtFavTV;
    LinearLayout settingsLL;
    ImageView imgSettingsIV;
    TextView txtSettingsTV;
    LinearLayout logoutLL;
    RelativeLayout fbRL;
    RelativeLayout pinterestRL;
    RelativeLayout instagramRL;
    RelativeLayout twitterRL;
    ImageView imgLogoutIV;
    TextView txtLogoutTV;
    LinearLayout profileLL;
    TextView txtUsernameTV;
    TextView txtShareTV;
    ImageView imgShareIV;
    LinearLayout shareLL;
    ImageView imgProfileIV;
    TextView txtSignInTV;
    LinearLayout languageLL;
    ImageView imgLanguageIV;
    TextView txtLanguageTV;
    LinearLayout contactLL;
    ImageView imgContactIV;
    TextView txtContactTV;
    @BindView(R.id.navBar)
    NavigationView navBar;
    @BindView(R.id.content)
    RelativeLayout content;
    //    public SimpleSideDrawer mSimpleSideDrawer;
    @BindView(R.id.txtNoDataFountTV)
    TextView txtNoDataFountTV;
    AdView adView;
    Handler handler = new Handler();
    DemoplayerAdapter adapter;
    ArrayList<HomeModel.Datum> newsList;
    TemplateView template;
    int currentVolume;
    private ColorDrawable background;

    // private InterstitialAd interstitialAd;
//    InterstitialAdListener interstitialAdListener;
    int b;
    int orientation;
    int adsPos;
    private View mPlayButtonLayout;
    private TextView mPlayTimeTextView;
    private Handler mHandler = null;
    private SeekBar mSeekBar;
    private int page_no = 1;
    private int item_count = 10;
    private boolean isFirstTym = true;
    private String strLastPage = "FALSE";
    private List<HomeModel.Datum> tempArrayList = new ArrayList<>();
    //creating Object of AdLoader
//    private AdLoader adLoader;
    // simple boolean to check the status of ad
    private boolean adLoaded = false;
    boolean mCheck = false;

    protected LocationRequest mLocationRequest;
    protected LocationSettingsRequest mLocationSettingsRequest;
    protected Location mCurrentLocation;
    protected Boolean mRequestingLocationUpdates;
    private String mAccessFineLocation = Manifest.permission.ACCESS_FINE_LOCATION;
    private String mAccessCourseLocation = Manifest.permission.ACCESS_COARSE_LOCATION;
    protected GoogleApiClient mGoogleApiClient;
    protected final static String KEY_LAST_UPDATED_TIME_STRING = "last-updated-time-string";
    protected String mLastUpdateTime;
    public double mLatitude;
    public double mLongitude;

    String strClick = "false";


    int ITEMS_PER_AD = 4;
    /*    private List<NativeAd> nativeAdList;
        private ArrayList<Object> objects;*/
//    boolean rem = true;
//    AudioManager audioManager;
//    AdView mAdView;
    private AdView mAdView;

    public static String extractYoutubeVideoId(String ytUrl) {

        String vId = null;

        String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";

        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(ytUrl);

        if (matcher.find()) {
            vId = matcher.group();
        }
        return vId;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        Log.d(TAG, "------Native Ad Project runs------");

        mRequestingLocationUpdates = false;
        mLastUpdateTime = "";
        /*Update values using data stored in the Bundle.*/
        updateValuesFromBundle(savedInstanceState);
        buildGoogleApiClient();
        // FirebaseApp.initializeApp(this);
        createLocationRequest();
        buildLocationSettingsRequest();
        if (checkPermission()) {
            checkLocationSettings();
        } else {
            requestPermission();
        }
        updateValuesFromBundle(savedInstanceState);

        Display display = ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
        orientation = display.getOrientation();
        paginationInterface = this;
        adsInterface = (AdsInterface) this;
        this.posInterface = posInterface;
        this.muteInterface = muteInterface;


        setNavigationDrawer();

        setNavigationViewListener();


        getCountryData();

//        mAdView = findViewById(R.id.adView);
        //  FacebookSdk.setIsDebugEnabled(true);
        // AdSettings.addTestDevice("Ecjbz3dtSpzzoxkSN0ltJrVdx4s=");

        AudienceNetworkAds.initialize(this);
        showAds();

    }


    public void getCountryData() {
        if (NewsAppPreferences.readString(mActivity, NewsAppPreferences.COUNTRY, "").equals("")) {
            NewsAppPreferences.writeString(mActivity, NewsAppPreferences.COUNTRY, "India");
            NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.IS_FIRST_TIME, true);
            txtCountryTV.setText("India");
        } else {
            txtCountryTV.setText(NewsAppPreferences.readString(mActivity, NewsAppPreferences.COUNTRY, null));
        }

    }
    public void showAds() {
        try {
            Log.e(TAG, "showAds: "+"Ads" );
//        mAdView.setVisibility(View.VISIBLE);
            adView = new AdView(this, "445117679825548_495169711487011", AdSize.BANNER_HEIGHT_50);
//        445117679825548_495169711487011
//        IMG_16_9_APP_INSTALL#YOUR_PLACEMENT_ID

// Find the Ad Container
            LinearLayout adContainer = findViewById(R.id.adView);

// Add the ad view to your activity layout
            adContainer.addView(adView);

// Request an ad
            adView.loadAd();
        }
        catch(Exception e){
            Log.e(TAG, "showAds: "+e.getMessage() );
        }
    }

//    public void showAds() {
//        mAdView.setVisibility(View.VISIBLE);
//        AdRequest adRequest = new AdRequest.Builder()
//                .build();
//        mAdView.loadAd(adRequest);
//        mAdView.setAdListener(new AdListener() {
//            @Override
//            public void onAdLoaded() {
//
//                Log.d("mAdView", "onAdFailedToLoad. But why? " + "load");
//                // Code to be executed when an ad finishes loading.
//            }
//
//            @Override
//            public void onAdFailedToLoad(LoadAdError error) {
//                int errorCode = error.getCode();
//                String errorMessage = error.getMessage();
//                Log.e("mAdView", "onAdFailedToLoad. But why? " + errorCode + "and" + errorMessage);
//                // Code to be executed when an ad request fails.
//            }
//
//            @Override
//            public void onAdOpened() {
//
//                Log.d("mAdView", "onAdFailedToLoad. But why? " + "opened");
//                // Code to be executed when an ad opens an overlay that
//                // covers the screen.
//            }
//
//            @Override
//            public void onAdClicked() {
//                Log.d("mAdView", "onAdFailedToLoad. But why? " + "clicked");
//
//                // Code to be executed when the user clicks on an ad.
//            }
//
//
//            @Override
//            public void onAdClosed() {
//                Log.d("mAdView", "onAdFailedToLoad. But why? " + "closed");
//                // Code to be executed when the user is about to return
//                // to the app after tapping on an ad.
//            }
//        });
//    }


    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    // This snippet shows the system bars. It does this by removing all the flags
// except for the ones that make the content appear under the system bars.
    private void showSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (adView != null) {
            adView.destroy();
        }
       // Release the player
        dismissProgressDialog();
        Log.e(TAG, "onDestroy");
        finish();


    }

    private void setProfileData() {
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.ic_ph_pro)
                .error(R.drawable.ic_ph_pro)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .dontAnimate()
                .dontTransform();
        Glide.with(mActivity)
                .load(NewsAppPreferences.readString(mActivity, NewsAppPreferences.IMAGE, null))
                .apply(options)
                .into(imgProfileIV);
        txtUsernameTV.setText(NewsAppPreferences.readString(mActivity, NewsAppPreferences.USERNAME, null));
    }

    private void setNavigationDrawer() {
        View header = navBar.getHeaderView(0);
//        mSimpleSideDrawer = new SimpleSideDrawer(mActivity);
//        mSimpleSideDrawer.setLeftBehindContentView(R.layout.drawer_layout);
        profileLL = findViewById(R.id.profileLL);
        imgProfileIV = findViewById(R.id.imgProfileIV);
        txtUsernameTV = findViewById(R.id.txtUsernameTV);
        txtSignInTV = findViewById(R.id.txtSignInTV);

        homeLL = header.findViewById(R.id.homeLL);
        imgHomeIV = header.findViewById(R.id.imgHomeIV);
        txtHomeTV = header.findViewById(R.id.txtHomeTV);
        locationLL = header.findViewById(R.id.locationLL);
        txtLocationTV = header.findViewById(R.id.txtLocationTV);
        imgLocIV = header.findViewById(R.id.imgLocIV);
        favLL = header.findViewById(R.id.favLL);
        imgFavIV = header.findViewById(R.id.imgFavIV);
        txtFavTV = header.findViewById(R.id.txtFavTV);
        settingsLL = header.findViewById(R.id.settingsLL);
        imgSettingsIV = header.findViewById(R.id.imgSettingsIV);
        txtSettingsTV = header.findViewById(R.id.txtSettingsTV);
        logoutLL = header.findViewById(R.id.logoutLL);
        imgLogoutIV = header.findViewById(R.id.imgLogoutIV);
        txtLogoutTV = header.findViewById(R.id.txtLogoutTV);
//        profileLL =  header.findViewById(R.id.profileLL);
//        imgProfileIV =  header.findViewById(R.id.imgProfileIV);
//        txtUsernameTV =  header.findViewById(R.id.txtUsernameTV);
        imgShareIV = header.findViewById(R.id.imgShareIV);
        txtShareTV = header.findViewById(R.id.txtShareTV);
        shareLL = header.findViewById(R.id.shareLL);
        fbRL = header.findViewById(R.id.fbRL);
        pinterestRL = header.findViewById(R.id.pinterestRL);
        instagramRL = header.findViewById(R.id.instagramRL);
        twitterRL = header.findViewById(R.id.twitterRL);
//        txtSignInTV =  header.findViewById(R.id.txtSignInTV);
        languageLL = header.findViewById(R.id.languageLL);
        txtLanguageTV = header.findViewById(R.id.txtLanguageTV);
        imgLanguageIV = header.findViewById(R.id.imgLanguageIV);
        contactLL = header.findViewById(R.id.contactLL);
        txtContactTV = header.findViewById(R.id.txtContactTV);
        imgContactIV = header.findViewById(R.id.imgContactIV);

        //open links
        // setSocialIcons();
    }

    private void setNavigationViewListener() {
        locationLL.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
//                drawer_layout.closeDrawer(GravityCompat.START);
                drawer_layout.closeDrawer(GravityCompat.START);
                imgLocIV.setColorFilter(getResources().getColor(R.color.colorPink));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtLocationTV.setTextColor(getResources().getColor(R.color.colorPink));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LANG_SEL, false);

                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.SHARE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.PROFILE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOC_SEL, true);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.FAV_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOGOUT_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.HOME_SEL, false);
                txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.CONTACT_SEL, false);

         /*       if (txtCountryTV.equals("India")) {
                    Intent intent = new Intent(mActivity, LocationActivity.class);
                    intent.putExtra("loc_sel", "3");
                    startActivity(intent);
                    finish();
                }*/

                if (mGoogleApiClient != null) {
                    if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
                        startLocationUpdates();
                    }
                }


                if (NewsAppPreferences.readString(mActivity, NewsAppPreferences.COUNTRY, "India").equals("India")) {
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.readString(mActivity, NewsAppPreferences.COUNTRY, null), null);
                    txtCountryTV.setText(NewsAppPreferences.readString(mActivity, NewsAppPreferences.COUNTRY, null));
                    Intent intent = new Intent(mActivity, LocationActivity.class);
                    intent.putExtra("loc_sel", "3");
                    startActivity(intent);
                    finish();
                    mCheck = false;

                } else {
                    //  showAlertDialogNearByCountry(mActivity, getString(R.string.this_feature));
                    showToast(mActivity, getString(R.string.this_feature));
                    Intent intent = new Intent(mActivity, LocationActivity.class);
                    intent.putExtra("loc_sel", "3");
                    startActivity(intent);
                    finish();
                    mCheck = true;
                }

            }
            //open links
            //setSocialIcons();

        });

        favLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgFavIV.setColorFilter(getResources().getColor(R.color.colorPink));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                txtFavTV.setTextColor(getResources().getColor(R.color.colorPink));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.SHARE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.PROFILE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOC_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.FAV_SEL, true);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOGOUT_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.HOME_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LANG_SEL, false);
                txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.CONTACT_SEL, false);


                if ((NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null)) != null) {
                    drawer_layout.closeDrawer(GravityCompat.START);
                    Intent intent = new Intent(mActivity, FavvActivity.class);
                    intent.putExtra("loc_sel", "2");
                    startActivity(intent);
                    finish();

                } else {
                    showLoginDialog(mActivity, "Please Login First");
                }

                //open links
                //setSocialIcons();

            }
        });

        profileLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorPink));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorPink));
                txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.SHARE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.PROFILE_SEL, true);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOC_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.FAV_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOGOUT_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.HOME_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LANG_SEL, false);
                txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.CONTACT_SEL, false);

                //open links
                //   setSocialIcons();

                performProfileClick();


            }

        });


        homeLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer_layout.closeDrawer(GravityCompat.START);
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorPink));
                imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorPink));
                txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.SHARE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.PROFILE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOC_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.FAV_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOGOUT_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.HOME_SEL, true);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LANG_SEL, false);
                txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.CONTACT_SEL, false);

                //open links
                //   setSocialIcons();

            }
        });

        logoutLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorPink));

                txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorPink));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.SHARE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.PROFILE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOC_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.FAV_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOGOUT_SEL, true);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.HOME_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LANG_SEL, false);
                txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.CONTACT_SEL, false);


                if ((NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null)) != null) {
//                    drawer_layout.closeDrawer(GravityCompat.START);
                    performLogoutClick();
                } else {
                    showAlertDialog(mActivity, "You are not Logged In");
                }


            }
        });

        fbRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Uri uri = Uri.parse("https://www.facebook.com/samachartvapp");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);

            }
        });

        twitterRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Uri uri = Uri.parse("https://twitter.com/AppSamachar");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        instagramRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Uri uri = Uri.parse("https://www.instagram.com/samacharapp/");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);

            }
        });

        pinterestRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Uri uri = Uri.parse("https://in.pinterest.com/appsamachar");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        shareLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtShareTV.setTextColor(getResources().getColor(R.color.colorPink));
                imgShareIV.setColorFilter(getResources().getColor(R.color.colorPink));
                txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.SHARE_SEL, true);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.PROFILE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOC_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.FAV_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOGOUT_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.HOME_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LANG_SEL, false);
                txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.CONTACT_SEL, false);

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Hey checkout our samachar app : " + "https://play.google.com/store/apps/details?id=com.dailysamachar.app");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });

        txtSignInTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.SHARE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.PROFILE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOC_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.FAV_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOGOUT_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.HOME_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LANG_SEL, false);
                txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.CONTACT_SEL, false);

                Intent intent = new Intent(mActivity, LoginActivity.class);
                startActivity(intent);
                finish();

            }
        });
        languageLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtLanguageTV.setTextColor(getResources().getColor(R.color.colorPink));
                imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorPink));

                txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.SHARE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.PROFILE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOC_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.FAV_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOGOUT_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LANG_SEL, true);
                txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.CONTACT_SEL, false);

                Intent intent = new Intent(mActivity, LanguageActivity.class);
                startActivity(intent);
                finish();

            }
        });
        contactLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                txtContactTV.setTextColor(getResources().getColor(R.color.colorPink));
                imgContactIV.setColorFilter(getResources().getColor(R.color.colorPink));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.CONTACT_SEL, true);

                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.SHARE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.PROFILE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOC_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.FAV_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOGOUT_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LANG_SEL, false);
                Intent intent = new Intent(mActivity, WebViewActivity.class);
                intent.putExtra(Constants.WV_TYPE, Constants.CONTACT_US);
                startActivity(intent);
                finish();

            }
        });


    }

    public void setSocialIcons() {

        fbRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Uri uri = Uri.parse("https://www.facebook.com/samachartvapp");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);

            }
        });

        twitterRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Uri uri = Uri.parse("https://twitter.com/AppSamachar");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        instagramRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Uri uri = Uri.parse("https://www.instagram.com/samacharapp/");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);

            }
        });

        pinterestRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Uri uri = Uri.parse("https://in.pinterest.com/appsamachar");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

    }

    /*
   transparent status bar
    */
    public void setStatusBar(Activity mActivity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mActivity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            // edited here
            mActivity.getWindow().setStatusBarColor(getResources().getColor(R.color.colorWhite));
        }
    }

    private void showNewsList() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeCategoryListApi();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
         mMap.put("userID", NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null));
        mMap.put("location", NewsAppPreferences.readString(mActivity, NewsAppPreferences.STATE, null));
        mMap.put("country", NewsAppPreferences.readString(mActivity, NewsAppPreferences.COUNTRY, null));
        mMap.put("type", "4");
        Log.e(TAG, "**PARAM**" + mMap);
        return mMap;
    }

    private void executeCategoryListApi() {
        if (page_no == 1) {
            showProgressDialog(mActivity);
        } else if (page_no > 1) {
            dismissProgressDialog();
//                mProgressBar.setVisibility(View.GONE);
        }
//        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.newsListRequest(NewsAppPreferences.readString(mActivity, NewsAppPreferences.AUTH_TOKEN,""),mParam()).enqueue(new Callback<HomeModel>() {
            @Override
            public void onResponse(Call<HomeModel> call, Response<HomeModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                HomeModel mModel = response.body();

                assert mModel != null;

                if (mModel.getLastPage() != null && !mModel.getLastPage().equals("")) {
                    strLastPage = mModel.getLastPage();
                }
                if (page_no == 1) {
                    dismissProgressDialog();
                }
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {

                    if (page_no == 1) {
                        mNewsList = mModel.getData();


                    } else if (page_no > 1) {
                        tempArrayList = mModel.getData();
                    }
                    if (tempArrayList != null) {
                        if (tempArrayList.size() > 0) {
                            mNewsList.addAll(tempArrayList);
                        }
                    }

                    if (mNewsList.size() > 4) {
                        AddItemToList();
                    }/* else {
                        mNewsList.addAll(mNewsList);
                    }*/

                    if (page_no == 1) {
                        setCategoryAdapter();
                    } else {
                        synchronized (adapter) {
                  /*          adapter.notify();
                           adapter.notifyDataSetChanged();*/
                        }
                    }
                    txtNoDataFountTV.setVisibility(View.GONE);
                    imgFavoriteIV.setVisibility(View.VISIBLE);

                }
                else if (mModel.getStatus().equals("3")) {
                    showAuthorisationAlert(mActivity);
                }
                else if (mModel.getStatus().equals("0")) {
                    txtNoDataFountTV.setVisibility(View.VISIBLE);
                    txtNoDataFountTV.setText(mModel.getMessage());
                    imgFavoriteIV.setVisibility(View.GONE);
                }
            }

            private void setCategoryAdapter() {
                layoutManager = new LinearLayoutManager(getApplicationContext());
//                layoutManager.setOrientation(RecyclerView.VERTICAL);
                recyclerView.setLayoutManager(layoutManager);
//                recyclerView.setHasFixedSize(true);

                recyclerView.setOnFlingListener(null);

                SnapHelper snapHelper = new PagerSnapHelper();
                snapHelper.attachToRecyclerView(recyclerView);


                // this is the scroll listener of recycler view which will tell the current item number
                recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);

                    }

                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);

                        try {
                            //here we find the current item number
                            final int scrollOffset = recyclerView.computeVerticalScrollOffset();
                            final int height = recyclerView.getHeight();
                            int page_no = scrollOffset / height;
                            var = page_no;
                            if (var == 0) {
                                if (mNewsList.get(page_no).getFavourite().equals("1")) {
                                    imgFavoriteIV.setImageResource(R.drawable.ic_fav_sel);
                                    img_fav = true;
                                } else {
                                    imgFavoriteIV.setImageResource(R.drawable.ic_fav);
                                    img_fav = false;
                                }
                                if (page_no != currentPage) {
                                    currentPage = page_no;
                                    try {
                                        Log.e("currentPage", "" + currentPage);
                                        Set_Player(currentPage);
                                        Release_Privious_Player();


                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        e.getMessage();
                                        e.getStackTrace();
                                    }
                                }
                            } else if ((var + 1) % 5 == 0) {
                                imgFavoriteIV.setVisibility(View.GONE);
//                            showToast(mActivity, "Ad");
                            } else {
                                imgFavoriteIV.setVisibility(View.VISIBLE);
                                if (mNewsList.get(page_no).getFavourite().equals("1")) {
                                    imgFavoriteIV.setImageResource(R.drawable.ic_fav_sel);
                                    img_fav = true;
                                } else {
                                    imgFavoriteIV.setImageResource(R.drawable.ic_fav);
                                    img_fav = false;
                                }
                                if (page_no != currentPage) {
                                    currentPage = page_no;
                                    try {
                                        Set_Player(currentPage);
                                        Release_Privious_Player();


                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        e.getMessage();
                                        e.getStackTrace();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                adapter = new DemoplayerAdapter(getApplicationContext(), (ArrayList<HomeModel.Datum>) mNewsList, logo, new DemoplayerAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int positon, String item, View view) {
                        switch (view.getId()) {
                            case R.id.play_video:
//                                        performClick(item);
                                break;
                            case R.id.pause_video:
//                                if (getResources().getConfiguration().orientation == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
//                                    topRL.setVisibility(View.GONE);
//                                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//                                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//                                    drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
//                                    setTheme(R.style.HomeTheme);
//                                    hideSystemUI();
//                                } else if (getResources().getConfiguration().orientation == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
//                                    topRL.setVisibility(View.VISIBLE);
//                                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//                                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
//                                    drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
//                                    setStatusBar(mActivity);
//                                    showSystemUI();
//                                    setStatusBar(mActivity);
//                                } else if (getResources().getConfiguration().orientation == ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE) {
//                                    topRL.setVisibility(View.VISIBLE);
//                                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//                                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
//                                    drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
//                                    setStatusBar(mActivity);
//                                    showSystemUI();
//                                    setStatusBar(mActivity);
//                                } else if (getResources().getConfiguration().orientation == ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT) {
//                                    topRL.setVisibility(View.GONE);
//                                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//                                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//                                    drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
//                                    setTheme(R.style.HomeTheme);
//                                    hideSystemUI();
//                                } else {
//                                    topRL.setVisibility(View.VISIBLE);
//                                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//                                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
//                                    drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
//                                    setStatusBar(mActivity);
//                                    showSystemUI();
//                                    setStatusBar(mActivity);
//                                }
//                                new Handler().postDelayed(new Runnable() {
//                                    public void run() {
//                                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
//                                        // do something...
//                                    }
//                                }, 6000
                                break;
                        }
                    }
                }, paginationInterface, adsInterface, strLastPage, muteInterface);
                recyclerView.setItemViewCacheSize(mNewsList.size());
                recyclerView.setAdapter(adapter);
                recyclerView.setHasFixedSize(true);

                // nativeAdList = new ArrayList<>();

                adapter.notifyDataSetChanged();

                // recyclerView.smoothScrollToPosition(pos);

            }


            @Override
            public void onFailure(Call<HomeModel> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    dismissProgressDialog();
                }
                t.printStackTrace();
            }
        });
    }

    private void AddItemToList() {

        List<HomeModel.Datum> mTemNewsList = new ArrayList<>();

        /* to add ads in list in fifth position */
        int i = ITEMS_PER_AD;

        if (page_no > 1) {
            for (int j = 0; j < mNewsList.size(); j++) {
                if (mNewsList.get(j).isIS_AD()) {
                    mNewsList.remove(mNewsList.get(j));
                }
                Log.e(TAG, mNewsList.toString());
            }
            Log.e(TAG, mNewsList.toString());
        }

        while (i <= mNewsList.size()) {
            HomeModel.Datum datum = new HomeModel.Datum();
            datum.setId("");
             datum.setFavourite("");
            datum.setTitle("");
            datum.setDescription("");
            datum.setLogo("");
            datum.setLink("");
            datum.setLocation("");
            datum.setCreated("");
            datum.setLast("");
            datum.setIS_AD(true);
            mNewsList.add(i, datum);
            i += ITEMS_PER_AD;
            i++;
        }

        Log.e(TAG, "mNewsList" + mNewsList.toString());
    }

    @Override
    public void mPaginationInterface(boolean isLastScrolled) {
        if (isLastScrolled == true) {
            if (strLastPage.equals("FALSE")) {
                showProgressDialog(mActivity);
//            mProgressBar.setVisibility(View.VISIBLE);
                page_no++;
            } else {
//                dismissProgressDialog();
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (strLastPage.equals("FALSE")) {
                        executeCategoryListApi();
                    } else {
//                        if (progressDialog.isShowing()) {
                        dismissProgressDialog();
//                        }
//                        mProgressBar.setVisibility(View.GONE);
                    }
                }
            }, 500);
        }
    }

    private void performClick2(String item, int positon) {
        Intent intent = new Intent(this, MainActivity20.class);
        intent.putExtra("videoPosition", String.valueOf(positon));
        intent.putExtra("videoId", item);
        intent.putExtra("locc", "explore");
        intent.putExtra("logo", mNewsList.get(positon).getLogo());
        Bundle bundle = new Bundle();
        bundle.putSerializable("mylist", (Serializable) mNewsList);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void performToast() {
        //  Toast.makeText(mActivity, "djjd", Toast.LENGTH_SHORT).show();
    }

    private void performClick(String item) {
        Intent intent = new Intent(this, MainActivity3.class);
        intent.putExtra("videoId", item);
        startActivity(intent);
    }

    private void Release_Privious_Player() {
        try {
            if (youTubePlayer2 != null) {
                youTubePlayer2.pause();
                youTubePlayer2.release();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Set_Player(int currentPage) {
        item = mNewsList.get(currentPage).getLink();
        newsId = mNewsList.get(currentPage).getId();
        Log.e("wjdqbd", mNewsList.get(currentPage).getLink());
        String extractvid = item;
        Log.e("Check", String.valueOf(extractvid));
        View layout = layoutManager.findViewByPosition(currentPage);
        youTubeView = layout.findViewById(R.id.youtube_view);
        mPlayButtonLayout = layout.findViewById(R.id.video_control);
//        mPlayTimeTextView = (TextView) layout.findViewById(R.id.play_time);
//        mSeekBar = (SeekBar) layout.findViewById(R.id.video_seekbar);

        try {
            youTubeView.initialize(Config.DEVELOPER_KEY, new YouTubePlayer.OnInitializedListener() {
                @Override
                public void onInitializationSuccess(YouTubePlayer.Provider provider,
                                                    YouTubePlayer youTubePlayer, boolean wasRestored) {
                    Log.e("extractvid", "Success");
                    // do any work here to cue video, play video, etc.
                    if (!wasRestored) {
                        String value1 = extractYoutubeVideoId(extractvid);
                        Log.e(TAG,"VALUE::"+value1);
                        // youTubePlayer.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE);
                        if (value1 != null) {
//                                try {
                            try {
                                youTubePlayer.loadVideo(value1);
                                Log.e("value!", value1);
                            } catch (IllegalStateException e) {
                                youTubeView.initialize(Config.DEVELOPER_KEY, this);
                            }
                        }
//                                catch (Exception e) {
//                                    e.printStackTrace();
//                                }
//                            }
                        // loadVideo() will auto play video
                        // Use cueVideo() method, if you don't want to play it automatically
                        youTubePlayer2 = youTubePlayer;
                        youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.MINIMAL);
//                            youTubePlayer.setShowFullscreenButton(true);
                    }
                }

                @Override
                public void onInitializationFailure(YouTubePlayer.Provider provider,
                                                    YouTubeInitializationResult errorReason) {
                    if (errorReason.isUserRecoverableError()) {
                        youTubeView.initialize(Config.DEVELOPER_KEY, this);
//                            errorReason.getErrorDialog(HomeActivity.this, RECOVERY_DIALOG_REQUEST).show();
                    } else {
                        youTubeView.initialize(Config.DEVELOPER_KEY, this);
//                            String errorMessage = String.format(
//                                    getString(R.string.error_player), errorReason.toString());
//                            Toast.makeText(HomeActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
//        exoPlayer.setPlayWhenReady(false); // Pause playback
        //Toast.makeText(mActivity, "pause", Toast.LENGTH_SHORT).show();
        try {
            if (youTubePlayer2 != null) {
                youTubePlayer2.pause();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            topRL.setVisibility(View.VISIBLE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            showSystemUI();
            setStatusBar(mActivity);
        } else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            topRL.setVisibility(View.GONE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            setTheme(R.style.HomeTheme);
            drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            setStatusBar(mActivity);
            hideSystemUI();
        } else {
            topRL.setVisibility(View.VISIBLE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            showSystemUI();
            setStatusBar(mActivity);
        }

    }

    @Override
    protected void onResume() {

        super.onResume();
//        exoPlayer.setPlayWhenReady(true); // Start playback
        getCountryData();
        //   setSocialIcons();
        NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.HOME_SEL, true);
        if (NewsAppPreferences.readBoolean(mActivity, NewsAppPreferences.HOME_SEL, false)) {
            imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgHomeIV.setColorFilter(getResources().getColor(R.color.colorPink));
            imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorPink));
            txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
        } else if (NewsAppPreferences.readBoolean(mActivity, NewsAppPreferences.FAV_SEL, false)) {
            imgFavIV.setColorFilter(getResources().getColor(R.color.colorPink));
            imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            txtFavTV.setTextColor(getResources().getColor(R.color.colorPink));
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
        } else if (NewsAppPreferences.readBoolean(mActivity, NewsAppPreferences.LOC_SEL, false)) {
            imgLocIV.setColorFilter(getResources().getColor(R.color.colorPink));
            imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            txtLocationTV.setTextColor(getResources().getColor(R.color.colorPink));
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
        } else if (NewsAppPreferences.readBoolean(mActivity, NewsAppPreferences.LOGOUT_SEL, false)) {
            imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorPink));
            txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorPink));
            txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
        } else if (NewsAppPreferences.readBoolean(mActivity, NewsAppPreferences.PROFILE_SEL, false)) {

            imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorPink));
            imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtSettingsTV.setTextColor(getResources().getColor(R.color.colorPink));
            txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
        } else if (NewsAppPreferences.readBoolean(mActivity, NewsAppPreferences.SHARE_SEL, false)) {
            imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtShareTV.setTextColor(getResources().getColor(R.color.colorPink));
            imgShareIV.setColorFilter(getResources().getColor(R.color.colorPink));

            txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
        } else if (NewsAppPreferences.readBoolean(mActivity, NewsAppPreferences.LANG_SEL, false)) {
            imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            txtLanguageTV.setTextColor(getResources().getColor(R.color.colorPink));
            imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorPink));
            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
        } else if (NewsAppPreferences.readBoolean(mActivity, NewsAppPreferences.CONTACT_SEL, false)) {
            imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgContactIV.setColorFilter(getResources().getColor(R.color.colorPink));

            txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtContactTV.setTextColor(getResources().getColor(R.color.colorPink));
            imgContactIV.setColorFilter(getResources().getColor(R.color.colorPink));
            imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));
        }
        if ((NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null)) != null) {
            setProfileData();
            imgProfileIV.setVisibility(View.VISIBLE);
            txtUsernameTV.setVisibility(View.VISIBLE);
            txtSignInTV.setVisibility(View.GONE);
            logoutLL.setVisibility(View.VISIBLE);
        } else {
            imgProfileIV.setVisibility(View.GONE);
            txtUsernameTV.setVisibility(View.GONE);
            txtSignInTV.setVisibility(View.VISIBLE);
            logoutLL.setVisibility(View.GONE);
        }

        try {
            if (youTubePlayer2!=null){
            youTubePlayer2.release();}
            Set_Player(currentPage);
            if (youTubePlayer2 != null) {
                youTubePlayer2.play();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick({R.id.imgFavoriteIV, R.id.imgMenuIV, R.id.locRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgFavoriteIV:
                if ((NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null)) != null) {
                    favUnfav();
                } else {
                    showLoginDialog(mActivity, "Please Login First");
                }
                break;

            case R.id.imgMenuIV:
                drawerOpenClick();
//                mSimpleSideDrawer.openLeftSide();
                break;

            case R.id.locRL:
                startActivity(new Intent(mActivity, SelectCountryActivity.class));
                finish();
        }
    }

    private void drawerOpenClick() {
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer_layout, 1, 0) {

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
//                super.onDrawerSlide(drawerView, slideOffset);
//                float slideX = drawerView.getWidth() * slideOffset;
//                content.setTranslationX(slideX);
            }
        };

        drawer_layout.addDrawerListener(actionBarDrawerToggle);
        drawer_layout.openDrawer(Gravity.LEFT);
    }

    private boolean checkSharePermission() {
        int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
        int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED;
    }

    private void requestSharePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 360);
        } else {
        }
    }

    @Override
    public void onBackPressed() {
        if (getResources().getConfiguration().orientation == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
            topRL.setVisibility(View.VISIBLE);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            showSystemUI();
            setStatusBar(mActivity);
        } else if (getResources().getConfiguration().orientation == ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE) {
            topRL.setVisibility(View.VISIBLE);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            showSystemUI();
            setStatusBar(mActivity);
        } else if (getResources().getConfiguration().orientation == 2) {
            topRL.setVisibility(View.VISIBLE);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            showSystemUI();
            setStatusBar(mActivity);
        }
//        else if ((NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null)) == null) {
//            startActivity(new Intent(mActivity, LoginActivity.class));
//            finish();
//        }
        else {
            finish();
        }
    }

    private void performLogoutClick() {
        showSignoutAlert(mActivity, getString(R.string.logout_sure));
    }


    public void showSignoutAlert(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.signout_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnNo = alertDialog.findViewById(R.id.btnNo);
        TextView btnYes = alertDialog.findViewById(R.id.btnYes);
        txtMessageTV.setText(strMessage);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
                alertDialog.dismiss();
            }
        });
    }
    public void showAuthorisationAlert(Activity mActivity) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.authorisation_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView loginAgain = alertDialog.findViewById(R.id.loginAgain);

        alertDialog.show();
        loginAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
                alertDialog.dismiss();
            }
        });
    }


    private void logout() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeLogoutApi();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mLogoutParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeLogoutApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.logoutRequest(mLogoutParams()).enqueue(new Callback<ForgotPasswordModel>() {
            @Override
            public void onResponse(Call<ForgotPasswordModel> call, Response<ForgotPasswordModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                ForgotPasswordModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    SharedPreferences preferences = NewsAppPreferences.getPreferences(Objects.requireNonNull(mActivity));
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.clear();
                    editor.apply();
                    mActivity.onBackPressed();
                    Intent mIntent = new Intent(mActivity, LoginActivity.class);
                    mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(mIntent);
                    finish();

                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ForgotPasswordModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(120000);
        mLocationRequest.setFastestInterval(30000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest).setAlwaysShow(true);
        mLocationSettingsRequest = builder.build();
    }

    public boolean checkPermission() {
        int mlocationFineP = ContextCompat.checkSelfPermission(mActivity, mAccessFineLocation);
        int mlocationCourseP = ContextCompat.checkSelfPermission(mActivity, mAccessCourseLocation);
        // int mlocationbgP = ContextCompat.checkSelfPermission(mActivity, mAccessBgLocation);
        return mlocationFineP == PackageManager.PERMISSION_GRANTED && mlocationCourseP == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{mAccessFineLocation, mAccessCourseLocation}, REQUEST_PERMISSION_CODE);
    }

    public void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback((ResultCallback<? super LocationSettingsResult>) this);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION_CODE) {// If request is cancelled, the result arrays are empty.
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted, yay! Do the
                checkLocationSettings();

            };
        }
    }

    private void updateValuesFromBundle(Bundle savedInstanceState) {

        if (savedInstanceState != null) {
            if (savedInstanceState.keySet().contains(KEY_REQUESTING_LOCATION_UPDATES)) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(
                        KEY_REQUESTING_LOCATION_UPDATES);
            }
            if (savedInstanceState.keySet().contains(KEY_LOCATION)) {
                mCurrentLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            }
            // Update the value of mLastUpdateTime from the Bundle and update the UI.
            if (savedInstanceState.keySet().contains(KEY_LAST_UPDATED_TIME_STRING)) {
                mLastUpdateTime = savedInstanceState.getString(KEY_LAST_UPDATED_TIME_STRING);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i("", "User agreed to make required location settings changes.");
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i("", "User chose not to make required location settings changes.");
                        break;
                    default:
                        // finish();
                }
        }
    }


    protected void startLocationUpdates() {
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient,
                    mLocationRequest,
                    (LocationListener) this
            ).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(Status status) {

                    mRequestingLocationUpdates = true;
                }
            });
        } catch (Exception e) {
//            mGoogleApiClient.connect();
            e.printStackTrace();

        }
    }

    protected synchronized void buildGoogleApiClient() {
        Log.i("", "Building GoogleApiClient");

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks) this)
                .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener) this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();

    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.e(TAG, "onStart");
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e(TAG, "onRestart");
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i("", "Connected to GoogleApiClient");
        if (mCurrentLocation == null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        }
    }

    /**
     * Callback that fires when the location changes.
     */

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        mLatitude = mCurrentLocation.getLatitude();
        mLongitude = mCurrentLocation.getLongitude();
        Log.e(TAG, "*********LATITUDE********" + mLatitude);
        Log.e(TAG, "*********LONGITUDE********" + mLongitude);
        NewsAppPreferences.writeString(mActivity, NewsAppPreferences.CURR_LAT, "" + mLatitude);
        NewsAppPreferences.writeString(mActivity, NewsAppPreferences.CURR_LONG, "" + mLongitude);
        try {
            DecimalFormat df = new DecimalFormat();
            df.setMaximumFractionDigits(3);
            mLatitude = Double.parseDouble(df.format(mCurrentLocation.getLatitude()));
            mLongitude =
                    Double.parseDouble(df.format(mCurrentLocation.getLongitude()));

            Geocoder geocoder = null;
            List<Address> addresses = null;
            geocoder = new Geocoder(this, Locale.getDefault());
            try {
                addresses = geocoder.getFromLocation(mLatitude, mLongitude, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (addresses != null && addresses.size() > 0) {
                //      String locality = addresses.get(0).getLocality();
                //     String country = addresses.get(0).getAdminArea();

                //    Log.e("state", "st" + country);
                String country_ = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

                if (strClick.equals("false")) {
                    showNewsList();
                    strClick = "true";
                }

                if (!(NewsAppPreferences.readString(mActivity, "var", "")).equals("true")) {
                    NewsAppPreferences.writeString(mActivity, "var", "true");
                    txtCountryTV.setText(country_);
                    // Toast.makeText(mActivity, "Your Current location is" + " " + country, Toast.LENGTH_SHORT).show();
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.COUNTRY, country_);
                }



            }
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.i("", "Connection suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i("", "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    private void favUnfav() {
        performfavUnfav();
    }
    /*
     *
     * Error Alert Dialog
     * */
    public void showLoginDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.setCancelable(true);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                startActivity(new Intent(mActivity, LoginActivity.class));
                finish();
            }
        });
        alertDialog.show();
    }
    private void performProfileClick() {
        if ((NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null)) != null) {
            startActivity(new Intent(mActivity, ProfileActivity.class));
            drawer_layout.closeDrawer(GravityCompat.START);
        } else {
            showLoginDialog(mActivity, "Please Login First");
        }
    }

    /*
     * Check Internet Connections
     * */
    public boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /*
     * Toast Message
     * */
    public void showToast(Activity mActivity, String strMessage) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show();
    }

    /*
     *
     * Error Alert Dialog
     * */
    public void showAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    /*
     * Show Progress Dialog
     * */

    public void showProgressDialog(Activity mActivity) {
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        Objects.requireNonNull(progressDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        if (progressDialog != null)
            progressDialog.show();
    }


    /*
     * Hide Progress Dialog
     * */
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void fav(int pos) {

    }

    public void performfavUnfav() {
        if (!isNetworkAvailable(Objects.requireNonNull(mActivity))) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeFavUnFavApi();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null));
        mMap.put("newsID", newsId);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeFavUnFavApi() {
//        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.favUnfavRequest(NewsAppPreferences.readString(mActivity, NewsAppPreferences.AUTH_TOKEN,""),mParams()).enqueue(new Callback<FavUnfavModel>() {
            @Override
            public void onResponse(Call<FavUnfavModel> call, Response<FavUnfavModel> response) {
//                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                FavUnfavModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    if (mModel.getType().equals(1)) {
                        showToast(mActivity, mModel.getMessage());
                        mNewsList.get(var).setFavourite("1");
                        imgFavoriteIV.setImageResource(R.drawable.ic_fav_sel);
                        imgFavoriteIV.setColorFilter(ContextCompat.getColor(mActivity, R.color.colorPink), android.graphics.PorterDuff.Mode.MULTIPLY);
                        img_fav = true;
                    }
                    else {
                        mNewsList.get(var).setFavourite("0");
                        imgFavoriteIV.setImageResource(R.drawable.ic_fav);
                        imgFavoriteIV.setColorFilter(ContextCompat.getColor(mActivity, R.color.colorPink), android.graphics.PorterDuff.Mode.MULTIPLY);
                        img_fav = false;
                        showToast(mActivity, mModel.getMessage());
                    }
                }
                else  if (mModel.getStatus().equals("3")) {
                    showAuthorisationAlert(mActivity);
                }
                else {
                    showAlertDialog(mActivity, "No Data Available");
                }
            }

            @Override
            public void onFailure(Call<FavUnfavModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e(TAG, "onStop");
        dismissProgressDialog();
    }

    @Override
    public void adsPos(int pos) {
        adsPos = pos;

    }

    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.i("", "All location settings are satisfied.");
                startLocationUpdates();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                Log.i("", "Location settings are not satisfied. Show the user a dialog to" +
                        "upgrade location settings ");
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(mActivity, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Log.i("", "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                Log.i("", "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");
                break;
            default:
                // finish();
        }
    }
}




