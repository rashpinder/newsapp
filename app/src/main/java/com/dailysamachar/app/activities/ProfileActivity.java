package com.dailysamachar.app.activities;

import static android.view.View.GONE;
import static androidx.constraintlayout.motion.utils.Oscillator.TAG;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.dailysamachar.app.R;
import com.dailysamachar.app.RetrofitApi.ApiClient;
import com.dailysamachar.app.interfaces.ApiInterface;
import com.dailysamachar.app.model.ForgotPasswordModel;
import com.dailysamachar.app.model.ProfileModel;
import com.dailysamachar.app.utils.Constants;
import com.dailysamachar.app.utils.NewsAppPreferences;
import com.google.android.material.navigation.NavigationView;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = ProfileActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = ProfileActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.txtNameTV)
    TextView txtNameTV;
    @BindView(R.id.imgEditIV)
    ImageView imgEditIV;
    @BindView(R.id.imgUserProfileIV)
    ImageView imgUserProfileIV;
    @BindView(R.id.txtEmailTV)
    TextView txtEmailTV;
    @BindView(R.id.txtBioTV)
    TextView txtBioTV;
    @BindView(R.id.txtBioDescTV)
    TextView txtBioDescTV;
    @BindView(R.id.aboutRL)
    RelativeLayout aboutRL;
    @BindView(R.id.privacyRL)
    RelativeLayout privacyRL;
    @BindView(R.id.termsRL)
    RelativeLayout termsRL;
    @BindView(R.id.logoutRL)
    RelativeLayout logoutRL;
    @BindView(R.id.DeleteAccountRL)
    RelativeLayout DeleteAccountRL;
    @BindView(R.id.imgMenuIV)
    ImageView imgMenuIV;
    @BindView(R.id.imgDeleteAccountIV)
    ImageView imgDeleteAccountIV;
    @BindView(R.id.txtDeleteAccountTV)
    TextView txtDeleteAccountTV;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer_layout;
    @BindView(R.id.navBar)
    NavigationView navBar;
    @BindView(R.id.content)
    RelativeLayout content;
    @BindView(R.id.viewDeleteAccount)
    View viewDeleteAccount;
    @BindView(R.id.viewLogout)
    View viewLogout;
    LinearLayout homeLL;
    ImageView imgHomeIV;
    TextView txtHomeTV;
    LinearLayout locationLL;
    ImageView imgLocIV;
    TextView txtLocationTV;
    LinearLayout favLL;
    ImageView imgFavIV;
    TextView txtFavTV;
    LinearLayout settingsLL;
    ImageView imgSettingsIV;
    TextView txtSettingsTV;
    LinearLayout logoutLL;
    ImageView imgLogoutIV;
    TextView txtLogoutTV;
    LinearLayout profileLL;
    TextView txtUsernameTV;
    TextView txtShareTV;
    ImageView imgShareIV;
    LinearLayout shareLL;
    ImageView imgProfileIV;
    TextView txtSignInTV;
    LinearLayout languageLL;
    ImageView imgLanguageIV;
    TextView txtLanguageTV;
    LinearLayout contactLL;
    ImageView imgContactIV;
    TextView txtContactTV;

//    public SimpleSideDrawer mSimpleSideDrawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        setStatusBar(mActivity);
        ButterKnife.bind(this);
        setNavigationDrawer();
        setNavigationViewListener();

        if (NewsAppPreferences.readBoolean(mActivity, NewsAppPreferences.HOME_SEL, false)) {
            imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgHomeIV.setColorFilter(getResources().getColor(R.color.colorPink));
            imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorPink));
            txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));

        } else if (NewsAppPreferences.readBoolean(mActivity, NewsAppPreferences.FAV_SEL, false)) {
            imgFavIV.setColorFilter(getResources().getColor(R.color.colorPink));
            imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtFavTV.setTextColor(getResources().getColor(R.color.colorPink));
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));

        } else if (NewsAppPreferences.readBoolean(mActivity, NewsAppPreferences.LOC_SEL, false)) {
            imgLocIV.setColorFilter(getResources().getColor(R.color.colorPink));
            imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            txtLocationTV.setTextColor(getResources().getColor(R.color.colorPink));
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
        } else if (NewsAppPreferences.readBoolean(mActivity, NewsAppPreferences.LOGOUT_SEL, false)) {
            imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorPink));

            txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorPink));
            txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
        } else if (NewsAppPreferences.readBoolean(mActivity, NewsAppPreferences.PROFILE_SEL, false)) {

            imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorPink));
            imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtSettingsTV.setTextColor(getResources().getColor(R.color.colorPink));
            txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
        } else if (NewsAppPreferences.readBoolean(mActivity, NewsAppPreferences.SHARE_SEL, false)) {
            imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtShareTV.setTextColor(getResources().getColor(R.color.colorPink));
            imgShareIV.setColorFilter(getResources().getColor(R.color.colorPink));

            txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
        } else if (NewsAppPreferences.readBoolean(mActivity, NewsAppPreferences.LANG_SEL, false)) {
            imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtLanguageTV.setTextColor(getResources().getColor(R.color.colorPink));
            imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorPink));

            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
        } else if (NewsAppPreferences.readBoolean(mActivity, NewsAppPreferences.CONTACT_SEL, false)) {
            imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgContactIV.setColorFilter(getResources().getColor(R.color.colorPink));

            txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtContactTV.setTextColor(getResources().getColor(R.color.colorPink));
            imgContactIV.setColorFilter(getResources().getColor(R.color.colorPink));
            imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));
        }
    }

    private void setProfileData() {
        RequestOptions options = new RequestOptions()
                .error(R.drawable.ic_ph_pro)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .dontAnimate()
                .dontTransform();
        Glide.with(mActivity)
                .load(NewsAppPreferences.readString(mActivity, NewsAppPreferences.IMAGE, null))
                .apply(options)
                .into(imgProfileIV);
        txtUsernameTV.setText(NewsAppPreferences.readString(mActivity, NewsAppPreferences.USERNAME, null));
    }

    private void setNavigationDrawer() {
        View header = navBar.getHeaderView(0);
//        mSimpleSideDrawer = new SimpleSideDrawer(mActivity);
//        mSimpleSideDrawer.setLeftBehindContentView(R.layout.drawer_layout);
        profileLL = findViewById(R.id.profileLL);
        imgProfileIV = findViewById(R.id.imgProfileIV);
        txtUsernameTV = findViewById(R.id.txtUsernameTV);
        txtSignInTV = findViewById(R.id.txtSignInTV);

        homeLL = header.findViewById(R.id.homeLL);
        imgHomeIV = header.findViewById(R.id.imgHomeIV);
        txtHomeTV = header.findViewById(R.id.txtHomeTV);
        locationLL = header.findViewById(R.id.locationLL);
        txtLocationTV = header.findViewById(R.id.txtLocationTV);
        imgLocIV = header.findViewById(R.id.imgLocIV);
        favLL = header.findViewById(R.id.favLL);
        imgFavIV = header.findViewById(R.id.imgFavIV);
        txtFavTV = header.findViewById(R.id.txtFavTV);
        settingsLL = header.findViewById(R.id.settingsLL);
        imgSettingsIV = header.findViewById(R.id.imgSettingsIV);
        txtSettingsTV = header.findViewById(R.id.txtSettingsTV);
        logoutLL = header.findViewById(R.id.logoutLL);
        imgLogoutIV = header.findViewById(R.id.imgLogoutIV);
        txtLogoutTV = header.findViewById(R.id.txtLogoutTV);
//        profileLL =  header.findViewById(R.id.profileLL);
//        imgProfileIV =  header.findViewById(R.id.imgProfileIV);
//        txtUsernameTV =  header.findViewById(R.id.txtUsernameTV);
        imgShareIV = header.findViewById(R.id.imgShareIV);
        txtShareTV = header.findViewById(R.id.txtShareTV);
        shareLL = header.findViewById(R.id.shareLL);
//        txtSignInTV =  header.findViewById(R.id.txtSignInTV);
        languageLL = header.findViewById(R.id.languageLL);
        txtLanguageTV = header.findViewById(R.id.txtLanguageTV);
        imgLanguageIV = header.findViewById(R.id.imgLanguageIV);
        contactLL = header.findViewById(R.id.contactLL);
        txtContactTV = header.findViewById(R.id.txtContactTV);
        imgContactIV = header.findViewById(R.id.imgContactIV);
    }


    private void setNavigationViewListener() {

        locationLL.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                drawer_layout.closeDrawer(GravityCompat.START);
                imgLocIV.setColorFilter(getResources().getColor(R.color.colorPink));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtLocationTV.setTextColor(getResources().getColor(R.color.colorPink));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LANG_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.SHARE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.PROFILE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOC_SEL, true);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.FAV_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOGOUT_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.HOME_SEL, false);

                txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.CONTACT_SEL, false);
                Intent intent = new Intent(mActivity, LocationActivity.class);
                intent.putExtra("loc_sel", "3");
                startActivity(intent);
                finish();
            }
        });
        favLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgFavIV.setColorFilter(getResources().getColor(R.color.colorPink));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                txtFavTV.setTextColor(getResources().getColor(R.color.colorPink));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LANG_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.SHARE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.PROFILE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOC_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.FAV_SEL, true);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOGOUT_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.HOME_SEL, false);

                txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.CONTACT_SEL, false);
                if ((NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null)) != null) {
                    drawer_layout.closeDrawer(GravityCompat.START);
                    Intent intent = new Intent(mActivity, FavvActivity.class);
                    intent.putExtra("loc_sel", "2");
                    startActivity(intent);
                } else {
                    showLoginDialog(mActivity, "Please Login First");
                }

            }
        });

        profileLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer_layout.closeDrawer(GravityCompat.START);
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LANG_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.SHARE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.PROFILE_SEL, true);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOC_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.FAV_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOGOUT_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.HOME_SEL, false);

                txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.CONTACT_SEL, false);
//                performProfileClick();
            }
        });


        homeLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer_layout.closeDrawer(GravityCompat.START);

                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorPink));
                imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorPink));
                txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LANG_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.SHARE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.PROFILE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOC_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.FAV_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOGOUT_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.HOME_SEL, true);

                txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.CONTACT_SEL, false);
                Intent intent = new Intent(mActivity, HomeActivity.class);
                intent.putExtra("loc_sel", "1");
                startActivity(intent);
                finish();
            }
        });

        logoutLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorPink));

                txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorPink));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LANG_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.SHARE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.PROFILE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOC_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.FAV_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOGOUT_SEL, true);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.HOME_SEL, false);

                txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.CONTACT_SEL, false);

                if ((NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null)) != null) {
                    drawer_layout.closeDrawer(GravityCompat.START);
                    performLogoutClick();
                } else {
                    showAlertDialog(mActivity, "You are not Logged In");
                    DeleteAccountRL.setVisibility(GONE);
                    viewDeleteAccount.setVisibility(GONE);
                }

            }
        });

        shareLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtShareTV.setTextColor(getResources().getColor(R.color.colorPink));
                imgShareIV.setColorFilter(getResources().getColor(R.color.colorPink));
                txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LANG_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.SHARE_SEL, true);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.PROFILE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOC_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.FAV_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOGOUT_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.HOME_SEL, false);

                txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.CONTACT_SEL, false);
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Hey checkout our samachar app :  " + "https://play.google.com/store/apps/details?id=com.dailysamachar.app");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);


            }
        });
        txtSignInTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LANG_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.SHARE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.PROFILE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOC_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.FAV_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOGOUT_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.HOME_SEL, false);

                txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.CONTACT_SEL, false);
                Intent intent = new Intent(mActivity, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
        languageLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtLanguageTV.setTextColor(getResources().getColor(R.color.colorPink));
                imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorPink));

                txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.SHARE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.PROFILE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOC_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.FAV_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOGOUT_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LANG_SEL, true);

                txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.CONTACT_SEL, false);
                Intent intent = new Intent(mActivity, LanguageActivity.class);
                startActivity(intent);
                finish();

            }
        });
        contactLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                txtContactTV.setTextColor(getResources().getColor(R.color.colorPink));
                imgContactIV.setColorFilter(getResources().getColor(R.color.colorPink));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.CONTACT_SEL, true);

                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.SHARE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.PROFILE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOC_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.FAV_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOGOUT_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LANG_SEL, false);
                Intent intent = new Intent(mActivity, WebViewActivity.class);
                intent.putExtra(Constants.WV_TYPE, Constants.CONTACT_US);
                startActivity(intent);
                finish();

            }
        });
    }

    private void performProfileClick() {
        if ((NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null)) != null) {
            startActivity(new Intent(mActivity, ProfileActivity.class));
            drawer_layout.closeDrawer(GravityCompat.START);
        } else {
            showLoginDialog(mActivity, "Please Login First");
        }
    }

    /*
     *
     * Error Alert Dialog
     * */
    public void showLoginDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.setCancelable(true);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                startActivity(new Intent(mActivity, LoginActivity.class));
                finish();
            }
        });
        alertDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if ((NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null)) != null) {
            imgProfileIV.setVisibility(View.VISIBLE);
            txtUsernameTV.setVisibility(View.VISIBLE);
            txtSignInTV.setVisibility(GONE);
        } else {
            imgProfileIV.setVisibility(GONE);
            txtUsernameTV.setVisibility(GONE);
            txtSignInTV.setVisibility(View.VISIBLE);
        }
        getUserProfileDetails();
    }


    private void getUserProfileDetails() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeGetUseFrDetailsApi();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null));
        Log.e(TAG, "**PARAM**" + mMap);
        return mMap;
    }

    private void executeGetUseFrDetailsApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getProfileDetailsRequest(NewsAppPreferences.readString(mActivity, NewsAppPreferences.AUTH_TOKEN,"")
                ,mParam()).enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                ProfileModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1"))
                {
                    String imageurl = mModel.getUserDetails().getProfileImage();
                    RequestOptions options = new RequestOptions()
                            .placeholder(R.drawable.ic_ph)
                            .error(R.drawable.ic_ph)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                            .dontAnimate()
                            .dontTransform();
                    if (imageurl != null) {
                        Glide.with(mActivity)
                                .load(imageurl)
                                .apply(options)
                              //  .placeholder(R.drawable.ic_ph_pro)
                                .into(imgUserProfileIV);
                        NewsAppPreferences.writeString(mActivity, NewsAppPreferences.IMAGE, imageurl);
                    } else {
                        String img = NewsAppPreferences.readString(mActivity, NewsAppPreferences.IMAGE, null);
                        Glide.with(mActivity)
                                .load(img)
                                .apply(options)
                               // .placeholder(R.drawable.ic_ph_pro)
                                .into(imgUserProfileIV);
                    }

                    txtNameTV.setText(mModel.getUserDetails().getName());
                    txtEmailTV.setText(mModel.getUserDetails().getEmail());
                    txtBioDescTV.setText(mModel.getUserDetails().getBio());
                    txtUsernameTV.setText(mModel.getUserDetails().getName());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.USERNAME, mModel.getUserDetails().getName());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.IMAGE, mModel.getUserDetails().getProfileImage());

                    setProfileData();
                }

                else if(mModel.getStatus().equals("3"))
                {
                    showAuthorisationAlert(mActivity);
                }

                else {
                    showAlertDialog(mActivity, "No Data Available");
                }
            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    @OnClick({R.id.imgEditIV, R.id.aboutRL, R.id.termsRL, R.id.privacyRL, R.id.logoutRL, R.id.imgMenuIV,R.id.DeleteAccountRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgEditIV:
                performEditProfileClick();
                break;
            case R.id.aboutRL:
                performAboutClick();
                break;
            case R.id.termsRL:
                performTermsClick();
                break;
            case R.id.privacyRL:
                performPrivacyClick();
                break;
            case R.id.logoutRL:
                performLogoutCheck();
                break;
            case R.id.DeleteAccountRL:
                performDeleteAccountCheck();
                break;
            case R.id.imgMenuIV:
//                mSimpleSideDrawer.openLeftSide();
                drawerOpenClick();
                break;
        }
    }

    private void drawerOpenClick() {
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer_layout, 1, 0) {

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
//                super.onDrawerSlide(drawerView, slideOffset);
//                float slideX = drawerView.getWidth() * slideOffset;
//                content.setTranslationX(slideX);
            }
        };

        drawer_layout.addDrawerListener(actionBarDrawerToggle);
        drawer_layout.openDrawer(Gravity.LEFT);
    }

    private void performLogoutCheck(){

        if ((NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null)) != null) {
            performLogoutClick();
        } else {
            showAlertDialog(mActivity, "You are not Logged In");
            DeleteAccountRL.setVisibility(GONE);
            viewDeleteAccount.setVisibility(GONE);
        }
    }


    private void performLogoutClick() {
        showSignoutAlert(mActivity, getString(R.string.logout_sure));
    }


    public void showSignoutAlert(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.signout_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnNo = alertDialog.findViewById(R.id.btnNo);
        TextView btnYes = alertDialog.findViewById(R.id.btnYes);
        txtMessageTV.setText(strMessage);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
                alertDialog.dismiss();
            }
        });
    }
    public void showAuthorisationAlert(Activity mActivity) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.authorisation_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView loginAgain = alertDialog.findViewById(R.id.loginAgain);

        alertDialog.show();
        loginAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
                alertDialog.dismiss();
            }
        });
    }
//    private void logout() {
//        SharedPreferences preferences = NewsAppPreferences.getPreferences(mActivity);
//        SharedPreferences.Editor editor = preferences.edit();
//        editor.clear();
//        editor.apply();
//        onBackPressed();
//        Intent mIntent = new Intent(mActivity, LoginActivity.class);
//        mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(mIntent);
//    }

    private void logout() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeLogoutApi();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mLogoutParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeLogoutApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.logoutRequest(mLogoutParams()).enqueue(new Callback<ForgotPasswordModel>() {
            @Override
            public void onResponse(Call<ForgotPasswordModel> call, Response<ForgotPasswordModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                ForgotPasswordModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    SharedPreferences preferences = NewsAppPreferences.getPreferences(Objects.requireNonNull(mActivity));
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.clear();
                   editor.apply();
                    mActivity.onBackPressed();
                    Intent mIntent = new Intent(mActivity, LoginActivity.class);
                    mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(mIntent);


                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ForgotPasswordModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void performDeleteAccountCheck(){

        if ((NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null)) != null) {
            performDeleteAccountClick();

        }
    }

    private void performDeleteAccountClick() {
        showDeleteAccountAlert(mActivity, getString(R.string.delete_acc));
    }

    public void showDeleteAccountAlert(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.signout_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnNo = alertDialog.findViewById(R.id.btnNo);
        TextView btnYes = alertDialog.findViewById(R.id.btnYes);
        txtMessageTV.setText(strMessage);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAccount();
                alertDialog.dismiss();
            }
        });
    }

    private void deleteAccount() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeDeleteAccountApi();
            logoutRL.setVisibility(GONE);
            viewLogout.setVisibility(GONE);
        }
    }

    private void executeDeleteAccountApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.deleteAccount(mLogoutParams()).enqueue(new Callback<ForgotPasswordModel>() {
            @Override
            public void onResponse(Call<ForgotPasswordModel> call, Response<ForgotPasswordModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                ForgotPasswordModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                 SharedPreferences preferences = NewsAppPreferences.getPreferences(Objects.requireNonNull(mActivity));
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.clear();
                  //  editor.apply();
                    SharedPreferences preferencess = NewsAppPreferences.getPreferences(Objects.requireNonNull(mActivity));
                    SharedPreferences.Editor editors = preferencess.edit();
                    editors.clear();
                    editors.apply();
                    mActivity.onBackPressed();
                    Intent mIntent = new Intent(mActivity, LoginActivity.class);
                    mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(mIntent);
                    finish();

                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ForgotPasswordModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void performPrivacyClick() {
        Intent mIntent = new Intent(mActivity, WebViewActivity.class);
        mIntent.putExtra(Constants.WV_TYPE, Constants.PRIVACY_POLICY);
        startActivity(mIntent);
    }

    private void performTermsClick() {
        Intent mIntent = new Intent(mActivity, WebViewActivity.class);
        mIntent.putExtra(Constants.WV_TYPE, Constants.TERMS_SERVICES);
        startActivity(mIntent);
    }

    private void performAboutClick() {
        Intent mIntent = new Intent(mActivity, WebViewActivity.class);
        mIntent.putExtra(Constants.WV_TYPE, Constants.ABOUT);
        startActivity(mIntent);
    }

    private void performEditProfileClick() {
        startActivity(new Intent(mActivity, EditProfileActivity.class));
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(mActivity, HomeActivity.class));
        finish();
    }
}
