package com.dailysamachar.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dailysamachar.app.RetrofitApi.ApiClient;
import com.dailysamachar.app.adapters.LanguageAdapter;
import com.dailysamachar.app.interfaces.ApiInterface;
import com.dailysamachar.app.interfaces.LangInterface;
import com.dailysamachar.app.model.ForgotPasswordModel;
import com.dailysamachar.app.model.LanguageModel;
import com.dailysamachar.app.utils.NewsAppPreferences;
import com.dailysamachar.app.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LanguageActivity extends BaseActivity implements LangInterface {
    /**
     * Getting the Current Class Name
     */
    String TAG = LanguageActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = LanguageActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.selectedLangRV)
    RecyclerView selectedLangRV;
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.txtUpdateTV)
    TextView txtUpdateTV;
    List<LanguageModel.State> mLanguageArrayList = new ArrayList<>();;
    LanguageAdapter mAdapter;
    private LangInterface langInterface;
    String langID;
    String sel_langID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        langInterface = this;
        getLangList();
    }
    /*
     * Execute api
     * */
    private Map<String, String> mGetParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void getLangList() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            if (mLanguageArrayList != null) {
                mLanguageArrayList.clear();
            }
            executeGetLangApi();
        }
    }

    private void executeGetLangApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getLanguageRequest(mGetParam()).enqueue(new Callback<LanguageModel>() {
            @Override
            public void onResponse(Call<LanguageModel> call, Response<LanguageModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                LanguageModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    sel_langID=mModel.getLanguageCheckedID();

                    mLanguageArrayList = mModel.getStates();
                    setLangAdapter();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }


            @Override
            public void onFailure(Call<LanguageModel> call, Throwable t) {
                dismissProgressDialog();
                t.printStackTrace();
            }
        });
    }

    private void setLangAdapter() {
        mAdapter = new LanguageAdapter(mActivity, mLanguageArrayList, langInterface,sel_langID);
        selectedLangRV.setLayoutManager(new LinearLayoutManager(mActivity));
        selectedLangRV.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @OnClick({R.id.imgBackIV, R.id.txtUpdateTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackIV:
                performBackClick();
                break;
            case R.id.txtUpdateTV:
                performUpdateClick();
                break;
        }
    }

    private void performUpdateClick() {
        setLangList();
    }

    private void performBackClick() {
        startActivity(new Intent(mActivity, HomeActivity.class));
        finish();
//        onBackPressed();
//        finish();
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null));
        mMap.put("languageID", langID);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void setLangList() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            if (mLanguageArrayList != null) {
                mLanguageArrayList.clear();
            }
            executeSetLangApi();
        }
    }

    private void executeSetLangApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.updateLanguageRequest(mParam()).enqueue(new Callback<ForgotPasswordModel>() {
            @Override
            public void onResponse(Call<ForgotPasswordModel> call, Response<ForgotPasswordModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                ForgotPasswordModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    showToast(mActivity, mModel.getMessage());
                    startActivity(new Intent(mActivity, HomeActivity.class));
                    finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }


            @Override
            public void onFailure(Call<ForgotPasswordModel> call, Throwable t) {
                dismissProgressDialog();
                t.printStackTrace();
            }
        });
    }

    @Override
    public void getLang(String langId) {
        langID = langId;
    }
}