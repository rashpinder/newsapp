package com.dailysamachar.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;

import androidx.annotation.NonNull;

import com.dailysamachar.app.R;
import com.dailysamachar.app.RetrofitApi.ApiClient;
import com.dailysamachar.app.interfaces.ApiInterface;

import com.dailysamachar.app.model.AddDeviceTokenModel;
import com.dailysamachar.app.utils.NewsAppPreferences;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;

public class SplashActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = SplashActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = SplashActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setStatusBar(mActivity);
        printKeyHash(mActivity);
        setSplash();

        getDeviceToken();
    }


    //    device token

    public void getDeviceToken() {
        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
            @Override
            public void onComplete(@NonNull Task<String> task) {

                if (!task.isSuccessful()) {
                    Log.e(TAG, "**Get Instance Failed**", task.getException());
                    return;
                }

                // Get new Instance ID token
                strDeviceToken = task.getResult();
                NewsAppPreferences.writeString(mActivity, NewsAppPreferences.DEVICE_TOKEN, strDeviceToken);
                Log.e(TAG, "**Push Token**" + strDeviceToken);
                executeTokenApi();
            }
        });
    }
//    public void getDeviceToken() {
//        FirebaseInstanceId.getInstance().getInstanceId()
//                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
//                        if (!task.isSuccessful()) {
//                            Log.e(TAG, "**Get Instance Failed**", task.getException());
//                            return;
//                        }
//
//                        // Get new Instance ID token
//                        strDeviceToken = task.getResult().getToken();
//                        NewsAppPreferences.writeString(mActivity, NewsAppPreferences.DEVICE_TOKEN, strDeviceToken);
//                        Log.e(TAG, "**Push Token**" + strDeviceToken);
//                        executeTokenApi();
//                    }
//                });
//    }
    private void setSplash() {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(mActivity, HomeActivity.class));
                    finish();
                }
            }, 2000);
        }




//    private void getKeyHash() {
//        PackageInfo info;
//        try {
//            info = getPackageManager().getPackageInfo("com.dailysamachar.app", PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md;
//                md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                String something = new String(Base64.encode(md.digest(), 0));
//                //String something = new String(Base64.encodeBytes(md.digest()));
//                Log.e("hash key:::::::::", something);
//            }
//        } catch (PackageManager.NameNotFoundException e1) {
//            Log.e("name not found", e1.toString());
//        } catch (NoSuchAlgorithmException e) {
//            Log.e("no such an algorithm", e.toString());
//        } catch (Exception e) {
//            Log.e("exception", e.toString());
//        }
//    }

public String printKeyHash(Activity context) {
    PackageInfo packageInfo;
    String key = null;
    try {
//getting application package name, as defined in manifest
        String packageName = context.getApplicationContext().getPackageName();

//Retriving package info
        packageInfo = context.getPackageManager().getPackageInfo(packageName,
                PackageManager.GET_SIGNATURES);

        Log.e("Package Name=", context.getApplicationContext().getPackageName());

        for (Signature signature : packageInfo.signatures) {
            MessageDigest md = MessageDigest.getInstance("SHA");
            md.update(signature.toByteArray());
            key = new String(Base64.encode(md.digest(), 0));
            Log.e(TAG, "Key Hash=++++++=" + key);
        }
    } catch (PackageManager.NameNotFoundException e1) {
        Log.e("Name not found", e1.toString());
    } catch (NoSuchAlgorithmException e) {
        Log.e("No such an algorithm", e.toString());
    } catch (Exception e) {
        Log.e("Exception", e.toString());
    }
    return key;
}


    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("deviceType", "2");
        mMap.put("deviceToken",strDeviceToken);
        Log.e(TAG, "**PARAM**" + mMap);
        return mMap;
    }

    private void executeTokenApi() {
        //showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.addDeviceToken(mParam()).enqueue(new retrofit2.Callback<AddDeviceTokenModel>() {
            @Override
            public void onResponse(Call<AddDeviceTokenModel> call, Response<AddDeviceTokenModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                AddDeviceTokenModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<AddDeviceTokenModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

}
