package com.dailysamachar.app.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.dailysamachar.app.R;
import com.dailysamachar.app.RetrofitApi.ApiClient;
import com.dailysamachar.app.interfaces.ApiInterface;
import com.dailysamachar.app.model.ForgotPasswordModel;
import com.dailysamachar.app.model.ProfileModel;
import com.dailysamachar.app.utils.NewsAppPreferences;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.nekoloop.base64image.Base64Image;
import com.nekoloop.base64image.RequestEncode;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditProfileActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = EditProfileActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = EditProfileActivity.this;

    //public Bitmap bitmap;


    /*
     * Widgets
     * */

    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;

    @BindView(R.id.editNameET)
    EditText editNameET;

    @BindView(R.id.imgEditProfileIV)
    ImageView imgEditProfileIV;

    @BindView(R.id.editEmailET)
    EditText editEmailET;

    @BindView(R.id.profileFL)
    FrameLayout profileFL;

    @BindView(R.id.editBioET)
    EditText editBioET;

    @BindView(R.id.txtUpdateTV)
    TextView txtUpdateTV;
    Bitmap selectedImage;
    String mBase64Image = "";
    private long mLastClickTime = 0;

    /*
     * Initialize Menifest Permissions:
     * & Camera Gallery Request @params
     * */
//    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
//    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
//    public String writeCamera = Manifest.permission.CAMERA;

    private String readImagePermission = Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU ?
            Manifest.permission.READ_MEDIA_IMAGES : Manifest.permission.READ_EXTERNAL_STORAGE;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        setStatusBar(mActivity);
        ButterKnife.bind(this);
        getUserProfileDetails();

        if (ContextCompat.checkSelfPermission(
                EditProfileActivity.this,
                readImagePermission
        ) == PackageManager.PERMISSION_GRANTED) {
            // Permission granted
        } else {
            // Permission not granted
        }

        editBioET.setOnTouchListener((v, event) -> {
            if (editBioET.hasFocus()) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_SCROLL:
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        return true;
                }
            }
            return false;
        });


    }

    private void getUserProfileDetails() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeGetUserDetailsApi();
        }
    }


    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeGetUserDetailsApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getProfileDetailsRequest(NewsAppPreferences.readString(mActivity, NewsAppPreferences.AUTH_TOKEN,""),
                mParam()).enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                ProfileModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    String imageurl = mModel.getUserDetails().getProfileImage();
                    RequestOptions options = new RequestOptions()
                            .placeholder(R.drawable.ic_ph)
                            .error(R.drawable.ic_ph)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                            .dontAnimate()
                            .dontTransform();


                    if (imageurl != null) {
                        Glide.with(mActivity)
                                .load(imageurl)
                                .placeholder(R.drawable.ic_ph_pro)
                                .into(imgEditProfileIV);
                        NewsAppPreferences.writeString(mActivity, NewsAppPreferences.IMAGE, imageurl);


                    } else {
                        String img = NewsAppPreferences.readString(mActivity, NewsAppPreferences.IMAGE, null);
                        Glide.with(mActivity)
                                .load(img)
                                .placeholder(R.drawable.ic_ph_pro)
                                .into(imgEditProfileIV);
                    }
                    editNameET.setText(mModel.getUserDetails().getName());
//                    editEmailET.setText(mModel.getUserDetails().getEmail());
                    editBioET.setText(mModel.getUserDetails().getBio());
                }
                else if(mModel.getStatus().equals("3"))
                {
                    showAuthorisationAlert(mActivity);
                }

                else {
                    showAlertDialog(mActivity, "No Data Available");
                }
            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    @OnClick({R.id.imgBackIV, R.id.txtUpdateTV, R.id.profileFL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackIV:
                performBackClick();
                break;
            case R.id.txtUpdateTV:
                performUpdateClick();
                break;
            case R.id.profileFL:
                performProfileClick();
                break;
        }
    }

    private void performBackClick() {
        onBackPressed();
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public boolean hasPermissions(Context context, String... permissions) {
        if (context != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    String[] PERMISSIONS = new String[0];

    /* Camera Gallery functionality
     * */
    private void performProfileClick() {
        if (!hasPermissions(mActivity, PERMISSIONS)) {
            ActivityCompat.requestPermissions(
                    mActivity,
                    PERMISSIONS,
                    369
            );
        }
        else{
            onSelectImageClick();
        }

//        if (checkPermission()) {
//            onSelectImageClick();
//            //   Click();
//
//        } else {
//            requestPermission();
//        }
    }

//    private boolean checkPermission() {
//        int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
//        int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
//        int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
//        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
//    }
//
//
//    private void requestPermission() {
//        ActivityCompat.requestPermissions(mActivity, new String[]{writeExternalStorage, writeReadStorage, writeCamera}, 369);
//    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 369:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    onSelectImageClick();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }
                break;
        }
    }

    /**
     * Start pick image activity with chooser.
     */
    private void onSelectImageClick() {
        ImagePicker.with(this)
                .crop()
                .compress(120)
                .maxResultSize(200, 200)
                .cropSquare()
                .start();
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.OFF)
                .setAspectRatio(70, 70)
                .setMultiTouchEnabled(false)
                .setOutputCompressQuality(10)
                .start(this);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {

            Uri uri = data.getData();
            Log.e(TAG, "**Path**$uri");

            // Use Uri object instead of File to avoid storage permissions

            Glide
                    .with(this)
                    .load(uri)
                    .centerCrop()
                    .placeholder(R.drawable.ic_ph_pro)
                    .into(imgEditProfileIV);

            try {
                selectedImage = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                mBase64Image = encodeTobase64(selectedImage);


            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }




/*
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // handle result of pick image chooser


        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(mActivity, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(mActivity, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()


            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }

        }


        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                try {
                    final InputStream imageStream = getContentResolver().openInputStream(result.getUri());
                    selectedImage = BitmapFactory.decodeStream(imageStream);
                    String path = MediaStore.Images.Media.insertImage(mActivity.getContentResolver(), selectedImage, "IMG_", null);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();

                    selectedImage.compress(Bitmap.CompressFormat.PNG, 10, baos);
                    RequestOptions options = new RequestOptions()
                            .placeholder(R.drawable.ic_ph)
                            .error(R.drawable.ic_ph)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                            .skipMemoryCache(true)
                            .dontAnimate()
                            .dontTransform();

                    if (path != null) {
                        Glide.with(mActivity).load(path)
                                .apply(options)
                                .into(imgEditProfileIV);

                    }


                    mBase64Image = encodeTobase64(selectedImage);
                    Log.e(TAG, "**Image Base 64**" + mBase64Image);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                showToast(mActivity, "Cropping failed: " + result.getError());
            }
        }
    }

    private String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.PNG, 10, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }
*/


    private String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 60, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }

    private void performUpdateClick() {
        if (isValidate()) {
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.internet_connection_error));
            } else {
                showProgressDialog(mActivity);
                executeUpdateProfileApi();
            }
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mEditProfileParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null));
        mMap.put("name", editNameET.getText().toString().trim());
        mMap.put("bio", editBioET.getText().toString().trim());
        mMap.put("profileImage", mBase64Image);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeUpdateProfileApi() {
//            showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.editProfileRequest(mEditProfileParam()).enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                ProfileModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    Log.e(TAG, "onResponse: "+ mModel.getUserDetails().getProfileImage() );
                    showToast(mActivity, "Profile Updated Successfully");
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.IMAGE, mModel.getUserDetails().getProfileImage());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.USERNAME, mModel.getUserDetails().getName());

                    finish();
                } else {
                    showAlertDialog(mActivity, "Profile not Updated");
                }
            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });

//        showProgressDialog(mActivity);
//        MultipartBody.Part mMultipartBody1 = null;
//
//        if (selectedImage != null) {
//            RequestBody requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(selectedImage));
//            mMultipartBody1 = MultipartBody.Part.createFormData("profileImage", getAlphaNumericString() + ".jpg", requestFile1);
//            Log.e("img","img"+mMultipartBody1);
//        }
//        RequestBody userID = RequestBody.create(MediaType.parse("multipart/form-data"), NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null));
//        RequestBody name = RequestBody.create(MediaType.parse("multipart/form-data"), editNameET.getText().toString().trim());
//        RequestBody bio = RequestBody.create(MediaType.parse("multipart/form-data"), editBioET.getText().toString().trim());
//        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
//
//        mApiInterface.editProfileRequest(userID, name, bio, mMultipartBody1).enqueue(new Callback<ProfileModel>() {
//
//            @Override
//            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
//                dismissProgressDialog();
//                Log.e(TAG, "**RESPONSE**" + response.body().toString());
//                ProfileModel mModel = response.body();
//                if (mModel.getStatus().equals("1")) {
//                    showToast(mActivity, mModel.getMessage());
//                    finish();
//                } else {
//                    showAlertDialog(mActivity, mModel.getMessage());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ProfileModel> call, Throwable t) {
//                dismissProgressDialog();
//                Log.e(TAG, "**ERROR**" + t.getMessage());
//            }
//        });
    }

    public byte[] convertBitmapToByteArrayUncompressed(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, stream);
        byte[] byteArray = stream.toByteArray();
        Bitmap compressedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        Log.e(TAG, "value--" + byteArray);
        return byteArray;
    }

    // generate dynamically string
    public String getAlphaNumericString() {
        int n = 20;

// chose a Character random from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

// create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

// generate a random number between
// 0 to AlphaNumericString variable length
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

// add Character one by one in end of sb
            sb.append(AlphaNumericString
                    .charAt(index));
        }

        return sb.toString();
    }

    /*
     * Set up validations
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_name));
            flag = false;
        } else if (editBioET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_bio));
            flag = false;
        }

        return flag;
    }




    public void showAuthorisationAlert(Activity mActivity) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.authorisation_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView loginAgain = alertDialog.findViewById(R.id.loginAgain);

        alertDialog.show();
        loginAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
                alertDialog.dismiss();
            }
        });
    }

    private void logout() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeLogoutApi();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mLogoutParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeLogoutApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.logoutRequest(mLogoutParams()).enqueue(new Callback<ForgotPasswordModel>() {
            @Override
            public void onResponse(Call<ForgotPasswordModel> call, Response<ForgotPasswordModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                ForgotPasswordModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    SharedPreferences preferences = NewsAppPreferences.getPreferences(Objects.requireNonNull(mActivity));


                    SharedPreferences.Editor editor = preferences.edit();
                    editor.clear();
                    editor.apply();
                    mActivity.onBackPressed();
                    Intent mIntent = new Intent(mActivity, LoginActivity.class);
                    mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(mIntent);

                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ForgotPasswordModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }
}
