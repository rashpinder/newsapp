package com.dailysamachar.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.cardview.widget.CardView;

import com.dailysamachar.app.Config;
import com.dailysamachar.app.R;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

public class MainActivity3 extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener, View.OnClickListener {
    Intent intent;
    String videoId;
    YouTubePlayer mPlayer;
    ImageView im_pause, im_fullscreen;
    LinearLayout linearLayout;
    FrameLayout frameLayout;
    RelativeLayout rly;
    CardView cardView;
    YouTubePlayer youTubePlayer2;
    boolean check = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View decorView = getWindow().getDecorView();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
// Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
        setContentView(R.layout.item_content3);
        im_pause = findViewById(R.id.pause_video);
        linearLayout = findViewById(R.id.video_control);
        frameLayout = findViewById(R.id.framelayout);
        im_fullscreen = findViewById(R.id.fullscreen);
        cardView = findViewById(R.id.card);
        rly = findViewById(R.id.rly);
        // Initializing YouTube player view
        YouTubePlayerView youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_view);
        try {
            youTubePlayerView.initialize(Config.DEVELOPER_KEY, this);

        } catch (Exception e) {
        }

        intent = getIntent();
        videoId = intent.getStringExtra("videoId");
        youTubePlayerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(MainActivity3.this, "playerview", Toast.LENGTH_SHORT).show();
                linearLayout.setVisibility(View.VISIBLE);
            }
        });
        im_fullscreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                youTubePlayer2.setFullscreen(true);
                youTubePlayer2.setPlayerStyle(YouTubePlayer.PlayerStyle.MINIMAL);
            }
        });


        // final RelativeLayout mainlayout = layout.findViewById(R.id.mainlayout);
        linearLayout.setOnTouchListener(new View.OnTouchListener() {
            private final GestureDetector gestureDetector = new GestureDetector(MainActivity3.this, new GestureDetector.SimpleOnGestureListener() {


                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    super.onSingleTapUp(e);
                    if (check) {
                        linearLayout.setVisibility(View.VISIBLE);
                    } else {
                        linearLayout.setVisibility(View.INVISIBLE);
                    }
                    //  Toast.makeText(MainActivity3.this, "Single", Toast.LENGTH_SHORT).show();

                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    super.onLongPress(e);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //  Show_video_option(item);
                        }
                    }, 1000);

                }

                @Override
                public boolean onDoubleTap(MotionEvent e) {


                    return super.onDoubleTap(e);

                }
            });

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                gestureDetector.onTouchEvent(event);
                //   Toast.makeText(MainActivity3.this, "toas", Toast.LENGTH_SHORT).show();
                return true;
            }
        });


        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(MainActivity3.this, "card", Toast.LENGTH_SHORT).show();
                linearLayout.setVisibility(View.VISIBLE);
            }
        });
        frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Toast.makeText(MainActivity3.this, "frame", Toast.LENGTH_SHORT).show();
                linearLayout.setVisibility(View.VISIBLE);
            }
        });
        rly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Toast.makeText(MainActivity3.this, "rly", Toast.LENGTH_SHORT).show();
                linearLayout.setVisibility(View.VISIBLE);
            }
        });

        im_pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                try {
                    youTubePlayer2.pause();
                    youTubePlayer2.release();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    /*
   transparent status bar
    */
    public void setStatusBar(Activity mActivity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mActivity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            // edited here
            mActivity.getWindow().setStatusBarColor(getResources().getColor(R.color.colorWhite));
        }
    }


    @Override
    public void onClick(View v) {

    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
        if (null == player) return;
        youTubePlayer2 = player;

        //displayCurrentTime();

        // Start buffering
        if (!wasRestored) {
            player.loadVideo(videoId);

        }
        player.setPlayerStyle(YouTubePlayer.PlayerStyle.CHROMELESS);
        player.setFullscreen(true);

        linearLayout.setVisibility(View.GONE);
        im_fullscreen.setVisibility(View.VISIBLE);


    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

    }

    @Override
    protected void onPause() {
        super.onPause();
        //Toast.makeText(mActivity, "pause", Toast.LENGTH_SHORT).show();
        try {
            if (youTubePlayer2 != null) {

                youTubePlayer2.pause();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        // Toast.makeText(mActivity, "Resume", Toast.LENGTH_SHORT).show();


        try {
            // youTubePlayer2.release();
            if (youTubePlayer2 != null) {
                youTubePlayer2.play();
                youTubePlayer2.setFullscreen(true);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}