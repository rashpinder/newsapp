package com.dailysamachar.app.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.dailysamachar.app.Config;
import com.dailysamachar.app.RetrofitApi.ApiClient;
import com.dailysamachar.app.adapters.DemoplayerAdapter;
import com.dailysamachar.app.interfaces.AdsInterface;
import com.dailysamachar.app.interfaces.ApiInterface;
import com.dailysamachar.app.interfaces.MuteInterface;
import com.dailysamachar.app.model.FavUnfavModel;
import com.dailysamachar.app.model.HomeModel;
import com.dailysamachar.app.utils.NewsAppPreferences;
import com.dailysamachar.app.utils.SimpleSideDrawer;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.dailysamachar.app.R;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.motion.utils.Oscillator.TAG;

@SuppressWarnings("ALL")
public class StatesActivity extends YouTubeBaseActivity
        implements DemoplayerAdapter.PaginationInterface, com.dailysamachar.app.interfaces.posInterface, MuteInterface {
    @BindView(R.id.imgFavoriteIV)
    ImageView imgFavoriteIV;
    LinearLayoutManager layoutManager;
    int currentPage = -1;
    String address;
    Activity mActivity = StatesActivity.this;
    DemoplayerAdapter adapter;
    YouTubePlayer youTubePlayer2;
    YouTubePlayerView youTubeView;
    File imagePath;
    private View mPlayButtonLayout;
    private TextView mPlayTimeTextView;
    String item;
    private Handler mHandler = null;
    private SeekBar mSeekBar;
    public static boolean img_fav;
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    Intent intent;
    String videoName;
    String newsId;
    List<HomeModel.Datum> mNewsList;
    com.dailysamachar.app.interfaces.posInterface posInterface;
    MuteInterface muteInterface;
    private int page_no = 1;
    private int item_count = 10;
    private String strLastPage = "FALSE";
    private List<HomeModel.Datum> tempArrayList = new ArrayList<>();
    DemoplayerAdapter.PaginationInterface paginationInterface;
    public Dialog progressDialog;
    boolean terms = true;
    String logo;
    String fav;
    int pos;
    public static int a = 0;
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    @BindView(R.id.recylerview_frag_home)
    RecyclerView recyclerView;
    public static int var;

    @BindView(R.id.txtNoDataFountTV)
    TextView txtNoDataFountTV;
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.txtTitleTV)
    TextView txtTitleTV;
    @BindView(R.id.imgMenuIV)
    ImageView imgMenuIV;
    public SimpleSideDrawer mSimpleSideDrawer;
    LinearLayout homeLL;
    ImageView imgHomeIV;
    TextView txtHomeTV;
    LinearLayout locationLL;
    ImageView imgLocIV;
    TextView txtLocationTV;
    LinearLayout favLL;
    ImageView imgFavIV;
    TextView txtFavTV;
    LinearLayout settingsLL;
    ImageView imgSettingsIV;
    TextView txtSettingsTV;
    LinearLayout logoutLL;
    ImageView imgLogoutIV;
    TextView txtLogoutTV;
    LinearLayout profileLL;
    TextView txtUsernameTV;
    ImageView imgProfileIV;
    TextView txtShareTV;
    ImageView imgShareIV;
    LinearLayout shareLL;
    String loc_sel;
    AdsInterface adsInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_states);
        ButterKnife.bind(this);
        setStatusBar(mActivity);

        paginationInterface = (DemoplayerAdapter.PaginationInterface) this;
        this.posInterface = posInterface;
        this.muteInterface = muteInterface;
        adsInterface = (AdsInterface) this;
        setNavigationDrawer();
        setNavigationViewListener();
        getIntentDAta();
        showNewsList();
    }

    private void getIntentDAta() {
        if (getIntent() != null) {
            if ((getIntent().getExtras()).get("state") != null) {
                loc_sel = (String) getIntent().getExtras().get("state");}
           txtTitleTV.setText(loc_sel);
    }}

    //    private void getIntentDAta() {
//        if (getIntent() != null) {
//            if (Objects.requireNonNull(getIntent().getExtras()).get("loc_sel") != null) {
//                loc_sel = (String) getIntent().getExtras().get("loc_sel");}
//            if (loc_sel.equals("2")){
//                txtFavTV.setTextColor(getResources().getColor(R.color.colorPink));
//                imgFavIV.setColorFilter(getResources().getColor(R.color.colorPink));
//            }
//        }
//    }
    private void setProfileData() {
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.ic_ph_pro)
                .error(R.drawable.ic_ph_pro)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .dontAnimate()
                .dontTransform();
        Glide.with(mActivity)
                .load(NewsAppPreferences.readString(mActivity, NewsAppPreferences.IMAGE, null))
                .apply(options)
                .into(imgProfileIV);
        txtUsernameTV.setText(NewsAppPreferences.readString(mActivity, NewsAppPreferences.USERNAME, null));
    }

    private void setNavigationDrawer() {
        mSimpleSideDrawer = new SimpleSideDrawer(mActivity);
        mSimpleSideDrawer.setLeftBehindContentView(R.layout.drawer_layout);

        homeLL = findViewById(R.id.homeLL);
        imgHomeIV = findViewById(R.id.imgHomeIV);
        txtHomeTV = findViewById(R.id.txtHomeTV);
        locationLL = findViewById(R.id.locationLL);
        txtLocationTV = findViewById(R.id.txtLocationTV);
        imgLocIV = findViewById(R.id.imgLocIV);
        favLL = findViewById(R.id.favLL);
        imgFavIV = findViewById(R.id.imgFavIV);
        txtFavTV = findViewById(R.id.txtFavTV);
        settingsLL = findViewById(R.id.settingsLL);
        imgSettingsIV = findViewById(R.id.imgSettingsIV);
        txtSettingsTV = findViewById(R.id.txtSettingsTV);
        logoutLL = findViewById(R.id.logoutLL);
        imgLogoutIV = findViewById(R.id.imgLogoutIV);
        txtLogoutTV = findViewById(R.id.txtLogoutTV);
        profileLL = findViewById(R.id.profileLL);
        imgProfileIV = findViewById(R.id.imgProfileIV);
        txtUsernameTV = findViewById(R.id.txtUsernameTV);
        imgShareIV = findViewById(R.id.imgShareIV);
        txtShareTV = findViewById(R.id.txtShareTV);
        shareLL = findViewById(R.id.shareLL);

//        imgShareIV.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                if (JaoharConstants.IS_BACK == true) {
////                    Utilities.hideKeyBoad(mActivity, llLeftLL);
////                    JaoharConstants.IS_BACK = false;
////                    FragmentManager fm = getSupportFragmentManager();
////                    fm.popBackStack();
////// Utilities.(HomeActivity.this);
////                    mainRL.setBackground(getResources().getDrawable(R.drawable.home_bg));
////                    bottomMainLL.setVisibility(View.VISIBLE);
////                    homeToolbar();
////                } else {
////                    Utilities.hideKeyBoad(mActivity, llLeftLL);
//                    mSimpleSideDrawer.openLeftSide();
//                }
////            }
//        });

// imgRightLL.setOnClickListener(new View.OnClickListener() {
// @Override
// public void onClick(View view) {
//// if (JaoharConstants.BOOLEAN_USER_LOGOUT) {
//// Intent mIntent = new Intent(mActivity, UserProfileActivity.class);
//// startActivity(mIntent);
//// finish();
////// logOutAPI(JaoharPreference.readString(mActivity, JaoharPreference.USER_ID, ""));
//// } else if (JaoharConstants.BOOLEAN_STAFF_LOGOUT) {
//// Intent mIntent = new Intent(mActivity, UserProfileActivity.class);
//// startActivity(mIntent);
//// finish();
////// logOutAPI(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
//// }
//
// PerformOptionsClick();
// }
// });
    }


    private void setNavigationViewListener() {

        locationLL.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                mSimpleSideDrawer.closeLeftSide();
//                homeLL.setBackgroundColor(Color.TRANSPARENT);
//                favLL.setBackgroundColor(Color.TRANSPARENT);
//                settingsLL.setBackgroundColor(Color.TRANSPARENT);
//                locationLL.setBackgroundColor(Color.TRANSPARENT);
//                logoutLL.setBackgroundColor(Color.TRANSPARENT);
//
//                imgLocIV.setColorFilter(getResources().getColor(R.color.colorPink));
//                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
//                imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
//                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
//                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
//                txtLocationTV.setTextColor(getResources().getColor(R.color.colorPink));
//                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
//                txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
//                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
//                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
//                txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
//                imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                startActivity(new Intent(mActivity, LocationActivity.class));
                finish();
            }
        });
        favLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                homeLL.setBackgroundColor(Color.TRANSPARENT);
//                favLL.setBackgroundColor(Color.TRANSPARENT);
//                locationLL.setBackgroundColor(Color.TRANSPARENT);
//                settingsLL.setBackgroundColor(Color.TRANSPARENT);
//                logoutLL.setBackgroundColor(Color.TRANSPARENT);
//
//                imgFavIV.setColorFilter(getResources().getColor(R.color.colorPink));
//                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
//                imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
//                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
//                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
//
//                txtFavTV.setTextColor(getResources().getColor(R.color.colorPink));
//                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
//                txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
//                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
//                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
//                txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
//                imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                if ((NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null)) != null) {
                    mSimpleSideDrawer.closeLeftSide();
                    startActivity(new Intent(mActivity, StatesActivity.class));
                    finish();
                } else {
                    showLoginDialog(mActivity, "Please Login First");
                }

            }
        });

        profileLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                homeLL.setBackgroundColor(Color.TRANSPARENT);
//                favLL.setBackgroundColor(Color.TRANSPARENT);
//                locationLL.setBackgroundColor(Color.TRANSPARENT);
//                settingsLL.setBackgroundColor(Color.TRANSPARENT);
//                logoutLL.setBackgroundColor(Color.TRANSPARENT);
//
//                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorPink));
//                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
//                imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
//                imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
//                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
//
//                txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
//                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
//                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorPink));
//                txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
//                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
//                txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
//                imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                performProfileClick();
            }
        });


        homeLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSimpleSideDrawer.closeLeftSide();
//                homeLL.setBackgroundColor(Color.TRANSPARENT);
//                favLL.setBackgroundColor(Color.TRANSPARENT);
//                locationLL.setBackgroundColor(Color.TRANSPARENT);
//                settingsLL.setBackgroundColor(Color.TRANSPARENT);
//                logoutLL.setBackgroundColor(Color.TRANSPARENT);
//
//                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
//                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorPink));
//                imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
//                imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
//                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
//                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
//
//                txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
//                txtHomeTV.setTextColor(getResources().getColor(R.color.colorPink));
//                txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
//                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
//                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
//                txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
//                imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                Intent intent=new Intent(mActivity, HomeActivity.class);
                intent.putExtra("loc_sel","1");
                startActivity(intent);
                finish();
            }
        });

        logoutLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                favLL.setBackgroundColor(Color.TRANSPARENT);
//                locationLL.setBackgroundColor(Color.TRANSPARENT);
//                homeLL.setBackgroundColor(Color.TRANSPARENT);
//                settingsLL.setBackgroundColor(Color.TRANSPARENT);
//                logoutLL.setBackgroundColor(Color.TRANSPARENT);
//
//                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
//                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
//                imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
//                imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
//                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorPink));
//
//                txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
//                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
//                txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
//                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorPink));
//                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
//                txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
//                imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                if ((NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null)) != null) {
                    mSimpleSideDrawer.closeLeftSide();
                    performLogoutClick();
                } else {
                    showAlertDialog(mActivity, "You are not Logged In");
                }

            }
        });

        shareLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                favLL.setBackgroundColor(Color.TRANSPARENT);
//                locationLL.setBackgroundColor(Color.TRANSPARENT);
//                homeLL.setBackgroundColor(Color.TRANSPARENT);
//                settingsLL.setBackgroundColor(Color.TRANSPARENT);
//                logoutLL.setBackgroundColor(Color.TRANSPARENT);
//                shareLL.setBackgroundColor(Color.TRANSPARENT);
//
//                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
//                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
//                imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
//                imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
//                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
//
//                txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
//                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
//                txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
//                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
//                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
//                txtShareTV.setTextColor(getResources().getColor(R.color.colorPink));
//                imgShareIV.setColorFilter(getResources().getColor(R.color.colorPink));
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Hey checkout our samachar app :  " + "https://play.google.com/store/apps/details?id=com.dailysamachar.app");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });

    }

    private void performProfileClick() {
        if ((NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null)) != null) {
            startActivity(new Intent(mActivity, ProfileActivity.class));
            mSimpleSideDrawer.closeLeftSide();
        } else {
            showLoginDialog(mActivity, "Please Login First");
        }
    }

    private void performLogoutClick() {
        showSignoutAlert(mActivity,getString(R.string.logout_sure));
    }


    public void showSignoutAlert(Activity mActivity,String mMssg) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.signout_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnNo = alertDialog.findViewById(R.id.btnNo);
        TextView btnYes = alertDialog.findViewById(R.id.btnYes);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
                alertDialog.dismiss();
            }
        });
    }

    public void showAuthorisationAlert(Activity mActivity) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.authorisation_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView loginAgain = alertDialog.findViewById(R.id.loginAgain);

        alertDialog.show();
        loginAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
                alertDialog.dismiss();
            }
        });
    }

    private void logout() {
        SharedPreferences preferences = NewsAppPreferences.getPreferences(mActivity);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
        onBackPressed();
        Intent mIntent = new Intent(mActivity, LoginActivity.class);
        mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(mIntent);
    }


    /*
   transparent status bar
    */
    public void setStatusBar(Activity mActivity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mActivity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            // edited here
            mActivity.getWindow().setStatusBarColor(getResources().getColor(R.color.colorWhite));
        }
    }


    private void showNewsList() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeCategoryListApi();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("type", "3");
        mMap.put("userID", NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null));
        mMap.put("location", loc_sel);
        mMap.put("pageNo", String.valueOf(page_no));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeCategoryListApi() {
        if (page_no == 1) {
            showProgressDialog(mActivity);
        } else if (page_no > 1) {
            dismissProgressDialog();
        }
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.newsListRequest(NewsAppPreferences.readString(mActivity, NewsAppPreferences.AUTH_TOKEN,""),mParam()).enqueue(new Callback<HomeModel>() {
            @Override
            public void onResponse(Call<HomeModel> call, Response<HomeModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                HomeModel mModel = response.body();
                mNewsList = new ArrayList<>();
                assert mModel != null;
                if (mModel.getLastPage() != null) {
                    strLastPage = mModel.getLastPage();
                }
                if (page_no == 1) {
                    dismissProgressDialog();
                }
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {

                    if (page_no == 1) {
                        mNewsList = mModel.getData();

                    } else if (page_no > 1) {
                        tempArrayList = mModel.getData();
                    }
                    if (tempArrayList != null) {
                        if (tempArrayList.size() > 0) {
                            mNewsList.addAll(tempArrayList);
                        }
                    }

                    if (page_no == 1) {
                        setCategoryAdapter();
                    } else {
//                        mShopAdapter.notifyDataSetChanged();
                    }
                    txtNoDataFountTV.setVisibility(View.GONE);
                    imgFavoriteIV.setVisibility(View.VISIBLE);
//                    imgFavoriteIV.setVisibility(View.VISIBLE);
                }
                else if (mModel.getStatus().equals("3")) {
                    showSignoutAlert(mActivity, getString(R.string.logout_sure));
                }

                else {
//                    showAlertDialog(mActivity, mModel.getMessage());
                    txtNoDataFountTV.setVisibility(View.VISIBLE);
                    txtNoDataFountTV.setText(mModel.getMessage());
                    imgFavoriteIV.setVisibility(View.GONE);
                }
            }

            private void setCategoryAdapter() {
                layoutManager = new LinearLayoutManager(mActivity);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setHasFixedSize(false);
                // tag=intent.getStringExtra("tagName");
                SnapHelper snapHelper = new PagerSnapHelper();
                recyclerView.setOnFlingListener(null);
                snapHelper.attachToRecyclerView(recyclerView);


                // this is the scroll listener of recycler view which will tell the current item number
                recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                    }

                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);
                        //here we find the current item number
                        final int scrollOffset = recyclerView.computeVerticalScrollOffset();
                        final int height = recyclerView.getHeight();
                        int page_no = scrollOffset / height;
                        var = page_no;
                        if (mNewsList.get(page_no).getFavourite().equals("1")) {
                            imgFavoriteIV.setImageResource(R.drawable.ic_fav_sel);
                            img_fav = true;
                        } else {
                            imgFavoriteIV.setImageResource(R.drawable.ic_fav);
                            img_fav = false;
                        }
                        if (page_no != currentPage) {
                            currentPage = page_no;
                            try {
                                Release_Privious_Player();
                                Set_Player(currentPage);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        }
                    }
                });

                adapter = new DemoplayerAdapter(mActivity, (ArrayList<HomeModel.Datum>) mNewsList, logo, new DemoplayerAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int positon, String item, View view) {
                        switch (view.getId()) {
                            case R.id.play_video:
//                                        performClick(item);
                                break;
                            case R.id.pause_video:
//                                            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                                performClick2(item, positon);
                                break;

                        }
                    }
                }, paginationInterface,adsInterface,strLastPage,muteInterface);


//                    @Override
//                    public void onItemClick(int positon, String item, View view) {
//                        s
//
//                    }

                recyclerView.setAdapter(adapter);
            }


            @Override
            public void onFailure(Call<HomeModel> call, Throwable t) {
                dismissProgressDialog();
                t.printStackTrace();
            }
        });
    }

    @Override
    public void mPaginationInterface(boolean isLastScrolled) {
        if (isLastScrolled == true) {
            if (strLastPage.equals("FALSE")) {
                showProgressDialog(mActivity);
//            mProgressBar.setVisibility(View.VISIBLE);

                page_no++;
            } else {
                dismissProgressDialog();
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (strLastPage.equals("FALSE")) {
                        executeCategoryListApi();
                    } else {
                        if (progressDialog.isShowing()) {
                            dismissProgressDialog();
                        }
//                        mProgressBar.setVisibility(View.GONE);
                    }
                }
            }, 1000);
        }
    }

    private void performClick2(String item, int positon) {
        Intent intent = new Intent(this, MainActivity20.class);
        intent.putExtra("videoPosition", String.valueOf(positon));
        intent.putExtra("videoId", item);
        intent.putExtra("locc", "states");
        intent.putExtra("logo", mNewsList.get(positon).getLogo());
        Bundle bundle = new Bundle();
        bundle.putSerializable("mylist", (Serializable) mNewsList);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void performClick(String item) {
        Intent intent = new Intent(this, MainActivity3.class);
        intent.putExtra("videoId", item);
        startActivity(intent);
    }

    private void Release_Privious_Player() {
        if (youTubePlayer2 != null) {
            youTubePlayer2.pause();
            youTubePlayer2.release();
        }
    }

    private void Set_Player(int currentPage) {
        item = mNewsList.get(currentPage).getLink();
        newsId = mNewsList.get(currentPage).getId();

        String extractvid = item;
        Log.e("Check", String.valueOf(extractvid));
        View layout = layoutManager.findViewByPosition(currentPage);
        // YouTubePlayerView youTubePlayerView = layout.findViewById(R.id.youtubePlayerView);
        youTubeView = layout.findViewById(R.id.youtube_view);
        mPlayButtonLayout = layout.findViewById(R.id.video_control);

//        mPlayTimeTextView = (TextView) layout.findViewById(R.id.play_time);
//        mSeekBar = (SeekBar) layout.findViewById(R.id.video_seekbar);

        youTubeView.initialize(Config.DEVELOPER_KEY,
                new YouTubePlayer.OnInitializedListener() {
                    @Override
                    public void onInitializationSuccess(YouTubePlayer.Provider provider,
                                                        YouTubePlayer youTubePlayer, boolean wasRestored) {

                        // do any work here to cue video, play video, etc.
                        if (!wasRestored) {
                            String value1 = extractYoutubeVideoId(extractvid);
                            // youTubePlayer.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE);
                            if (value1 != null) {
                                youTubePlayer.loadVideo(value1);
                            }
                            // loadVideo() will auto play video
                            // Use cueVideo() method, if you don't want to play it automatically
                            youTubePlayer2 = youTubePlayer;
                            youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.MINIMAL);
                        }

                    }

                    @Override
                    public void onInitializationFailure(YouTubePlayer.Provider provider,
                                                        YouTubeInitializationResult errorReason) {
                        if (errorReason.isUserRecoverableError()) {
                            errorReason.getErrorDialog(StatesActivity.this, RECOVERY_DIALOG_REQUEST).show();
                        } else {
                            String errorMessage = String.format(
                                    getString(R.string.error_player), errorReason.toString());
                            Toast.makeText(StatesActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                        }

                    }
                });


    }


    public static String extractYoutubeVideoId(String ytUrl) {

        String vId = null;

        String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";

        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(ytUrl);

        if (matcher.find()) {
            vId = matcher.group();
        }
        return vId;
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Toast.makeText(mActivity, "pause", Toast.LENGTH_SHORT).show();
        try {
            if (youTubePlayer2 != null) {

                youTubePlayer2.pause();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        setProfileData();
        // Toast.makeText(mActivity, "Resume", Toast.LENGTH_SHORT).show();
        try {
            youTubePlayer2.release();
            Set_Player(currentPage);
            if (youTubePlayer2 != null) {
                youTubePlayer2.play();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick({R.id.imgFavoriteIV, R.id.imgMenuIV,R.id.imgBackIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgFavoriteIV:
                if ((NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null)) != null) {
                    favUnfav();
                } else {
                    showLoginDialog(mActivity, "Please Login First");
                }
                break;
            case R.id.imgMenuIV:
                mSimpleSideDrawer.openLeftSide();
                break;
                case R.id.imgBackIV:
               performBAckClick();
                break;
        }
    }

    private void performBAckClick() {
        onBackPressed();
    }




    private boolean checkSharePermission() {
        int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
        int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED;
    }

    private void requestSharePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 360);
        } else {
        }
    }

    @Override
    public void onBackPressed() {
        finish();
//        if ((NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null)) == null) {
//            startActivity(new Intent(mActivity, LoginActivity.class));
//            finish();
//        } else {
//            startActivity(new Intent(mActivity, HomeActivity.class));
//            finish();
//        }
    }

    private void favUnfav() {
        performfavUnfav();
    }


    /*
     *
     * Error Alert Dialog
     * */
    public void showLoginDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.setCancelable(true);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                startActivity(new Intent(mActivity, LoginActivity.class));
                finish();
            }
        });
        alertDialog.show();
    }

    /*
     * Check Internet Connections
     * */
    public boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    /*
     * Toast Message
     * */
    public void showToast(Activity mActivity, String strMessage) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show();
    }


    /*
     *
     * Error Alert Dialog
     * */
    public void showAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    /*
     * Show Progress Dialog
     * */

    public void showProgressDialog(Activity mActivity) {
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        Objects.requireNonNull(progressDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        if (progressDialog != null)
            progressDialog.show();
    }


    /*
     * Hide Progress Dialog
     * */
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void fav(int pos) {

    }

    public void performfavUnfav() {
        if (!isNetworkAvailable(Objects.requireNonNull(mActivity))) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeFavUnFavApi();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null));
        mMap.put("newsID", newsId);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeFavUnFavApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.favUnfavRequest(NewsAppPreferences.readString(mActivity, NewsAppPreferences.AUTH_TOKEN,""),mParams()).enqueue(new Callback<FavUnfavModel>() {
            @Override
            public void onResponse(Call<FavUnfavModel> call, Response<FavUnfavModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                FavUnfavModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {

                    if (mModel.getType().equals(1)) {
                        showToast(mActivity, mModel.getMessage());
                        mNewsList.get(var).setFavourite("1");
                        imgFavoriteIV.setImageResource(R.drawable.ic_fav_sel);
                        imgFavoriteIV.setColorFilter(ContextCompat.getColor(mActivity, R.color.colorPink), android.graphics.PorterDuff.Mode.MULTIPLY);
                        img_fav = true;
                    }
                    if (mModel.getStatus().equals("3")) {
                        showAuthorisationAlert(mActivity);


                    }

                    else {
                        mNewsList.get(var).setFavourite("0");
                        imgFavoriteIV.setImageResource(R.drawable.ic_fav);
                        imgFavoriteIV.setColorFilter(ContextCompat.getColor(mActivity, R.color.colorPink), android.graphics.PorterDuff.Mode.MULTIPLY);
                        img_fav = false;
                        showToast(mActivity, mModel.getMessage());
                    }
                }

                else  if (mModel.getStatus().equals("3")) {
                    showAuthorisationAlert(mActivity);
                }

                else {
                    showAlertDialog(mActivity, "No Data Available");
                }
            }

            @Override
            public void onFailure(Call<FavUnfavModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    @Override
    public void getImg(ImageView img) {

    }
}