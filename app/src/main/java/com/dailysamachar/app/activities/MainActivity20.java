package com.dailysamachar.app.activities;


import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.dailysamachar.app.Config;
import com.dailysamachar.app.VideoModel;
import com.dailysamachar.app.adapters.DemoplayerAdapter20;
import com.dailysamachar.app.model.HomeModel;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.dailysamachar.app.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressWarnings("ALL")
public class MainActivity20 extends YouTubeBaseActivity {
    @BindView(R.id.recylerview_frag_home)
    RecyclerView recyclerView;

    LinearLayoutManager layoutManager;
    int currentPage = -1;
    Activity mActivity = MainActivity20.this;
    DemoplayerAdapter20 adapter;
    //    HomeModel videoModel;
    ArrayList<VideoModel> videoArrayList= new ArrayList<>();
//    ArrayList<HomeModel.Datum> videoArrayList = new ArrayList<>();
    List<HomeModel.Datum> videoArrayList2 = new ArrayList<>();
    YouTubePlayer youTubePlayer2;
    YouTubePlayerView youTubeView;

    private View mPlayButtonLayout;
    private TextView mPlayTimeTextView;

    private Handler mHandler = null;
    private SeekBar mSeekBar;
    ImageView logoIV;
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    Intent intent;
    int videoPosition;
    String videoId;
    String logo;
    String value;
    VideoModel videoModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_landscape);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
//        ll.setVisibility(View.GONE);
//        mainLL.setVisibility(View.GONE);
        intent = getIntent();
        if (intent != null) {
            videoPosition = Integer.parseInt(intent.getStringExtra("videoPosition"));
            videoId = intent.getStringExtra("videoId");
            logo = intent.getStringExtra("logo");
            value = intent.getStringExtra("locc");
            Bundle bundle = getIntent().getExtras();
            videoArrayList2 = (List<HomeModel.Datum>) bundle.getSerializable("mylist");
        }

        try {
//            videoArrayList.addAll(videoArrayList2);]

            videoModel=new VideoModel();
            videoModel.setVideoLink(videoArrayList2.get(videoPosition).getLink());
//            videoArrayList.add(videoPosition,videoId);

        } catch (Exception e) {
            e.printStackTrace();
        }
        layoutManager = new LinearLayoutManager(mActivity);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(false);
        // tag=intent.getStringExtra("tagName");
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(recyclerView);


        // this is the scroll listener of recycler view which will tell the current item number
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);


            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                //here we find the current item number
                final int scrollOffset = recyclerView.computeVerticalScrollOffset();
                final int height = recyclerView.getHeight();
                int page_no = scrollOffset / height;

                if (page_no != currentPage) {
                    currentPage = page_no;
                    try {
                        Release_Privious_Player();
                        Set_Player(currentPage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            }
        });
        Collections.swap(videoArrayList2, 0, videoPosition);
        adapter = new DemoplayerAdapter20(mActivity, videoArrayList, (ArrayList<HomeModel.Datum>) videoArrayList2,logo, new DemoplayerAdapter20.OnItemClickListener() {
            @Override
            public void onItemClick(int positon, String item, View view) {
                switch (view.getId()) {
                    case R.id.play_video:
                        performClick(positon, item);
                        break;
                    case R.id.pause_video:
                        performClick2(videoArrayList2.get(positon).getLink(), positon,videoArrayList2.get(positon).getLogo());
                        break;

                }
            }
        });


        recyclerView.setAdapter(adapter);
    }

    /*
      transparent status bar
       */
    public void setStatusBar(Activity mActivity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mActivity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            // edited here
            mActivity.getWindow().setStatusBarColor(getResources().getColor(R.color.colorWhite));
        }
    }


    private void performClick2(String item, int position,String logo) {
//        youTubePlayer2.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE);
//        //youTubePlayer2.setFullscreenControlFlags(YouTubePlayer.FULLSCREEN_FLAG_CONTROL_SYSTEM_UI);
//        youTubePlayer2.setFullscreen(true);
//        youTubePlayer2.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
//        youTubePlayer2.play();
if (value.equals("locc")){
    HomeModel.Datum itema = videoArrayList2.get(currentPage);
    Intent intent = new Intent(this, LocationActivity.class);
//        NewsAppPreferences.writeString(mActivity,"videoName",item);
//        NewsAppPreferences.writeString(mActivity,"logo", logo);
//        NewsAppPreferences.writeInteger(mActivity,"pos", position);
    intent.putExtra("videoName", itema);
//        intent.putExtra("logo", logo);
//        intent.putExtra("pos", position);
//        intent.putExtra("logo", videoArrayList2.get(position).getLogo());
    startActivity(intent);
}
else if (value.equals("explore")){
    HomeModel.Datum itema = videoArrayList2.get(currentPage);
    Intent intent = new Intent(this, HomeActivity.class);
//        NewsAppPreferences.writeString(mActivity,"videoName",item);
//        NewsAppPreferences.writeString(mActivity,"logo", logo);
//        NewsAppPreferences.writeInteger(mActivity,"pos", position);
    intent.putExtra("videoName", itema);
//        intent.putExtra("logo", logo);
//        intent.putExtra("pos", position);
//        intent.putExtra("logo", videoArrayList2.get(position).getLogo());
    startActivity(intent);
}else if (value.equals("states")){
    HomeModel.Datum itema = videoArrayList2.get(currentPage);
    Intent intent = new Intent(this, StatesActivity.class);
//        NewsAppPreferences.writeString(mActivity,"videoName",item);
//        NewsAppPreferences.writeString(mActivity,"logo", logo);
//        NewsAppPreferences.writeInteger(mActivity,"pos", position);
    intent.putExtra("videoName", itema);
//        intent.putExtra("logo", logo);
//        intent.putExtra("pos", position);
//        intent.putExtra("logo", videoArrayList2.get(position).getLogo());
    startActivity(intent);
}
else {
    HomeModel.Datum itema = videoArrayList2.get(currentPage);
    Intent intent = new Intent(this, FavvActivity.class);
//        NewsAppPreferences.writeString(mActivity,"videoName",item);
//        NewsAppPreferences.writeString(mActivity,"logo", logo);
//        NewsAppPreferences.writeInteger(mActivity,"pos", position);
    intent.putExtra("videoName", itema);
//        intent.putExtra("logo", logo);
//        intent.putExtra("pos", position);
//        intent.putExtra("logo", videoArrayList2.get(position).getLogo());
    startActivity(intent);
}
    }

    private void performToast() {
        //  Toast.makeText(mActivity, "djjd", Toast.LENGTH_SHORT).show();
    }

    private void performClick(int positon, String item) {
        // Toast.makeText(mActivity, "jk", Toast.LENGTH_SHORT).show();
//        if (youTubePlayer2!=null)
//        {
//            if (youTubePlayer2.isPlaying())
//            {
//                youTubePlayer2.pause();
//            }
//            else {
//                youTubePlayer2.play();
//            }
//
//        }
        Intent intent = new Intent(this, MainActivity3.class);
        intent.putExtra("videoId", item);
        startActivity(intent);
    }

    private void Release_Privious_Player() {
        if (youTubePlayer2 != null) {
            youTubePlayer2.pause();
//            youTubePlayer2.release();
        }
    }

    private void Set_Player(int currentPage) {
        final String item = videoArrayList2.get(currentPage).getLink();
//        final String item = videoModel.getVideoLink();
        String extractvid = item;
        Log.e("Check", String.valueOf(extractvid));
        View layout = layoutManager.findViewByPosition(currentPage);
        // YouTubePlayerView youTubePlayerView = layout.findViewById(R.id.youtubePlayerView);
        youTubeView = layout.findViewById(R.id.youtube_view);
        mPlayButtonLayout = layout.findViewById(R.id.video_control);

//        mPlayTimeTextView = (TextView) layout.findViewById(R.id.play_time);
//        mSeekBar = (SeekBar) layout.findViewById(R.id.video_seekbar);

        try{
            youTubeView.initialize(Config.DEVELOPER_KEY,
                    new YouTubePlayer.OnInitializedListener() {
                        @Override
                        public void onInitializationSuccess(YouTubePlayer.Provider provider,
                                                            YouTubePlayer youTubePlayer, boolean wasRestored) {

                            // do any work here to cue video, play video, etc.
                            if (!wasRestored) {
                                String value1 = extractYoutubeVideoId(extractvid);
                                // youTubePlayer.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE);
                                youTubePlayer.loadVideo(value1);
                                // loadVideo() will auto play video
                                // Use cueVideo() method, if you don't want to play it automatically
                                youTubePlayer2 = youTubePlayer;


                                youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.MINIMAL);


                            }

                        }

                        @Override
                        public void onInitializationFailure(YouTubePlayer.Provider provider,
                                                            YouTubeInitializationResult errorReason) {
                            if (errorReason.isUserRecoverableError()) {
                                errorReason.getErrorDialog(mActivity, RECOVERY_DIALOG_REQUEST).show();
                            } else {
                                String errorMessage = String.format(
                                        getString(R.string.error_player), errorReason.toString());
                                Toast.makeText(mActivity, errorMessage, Toast.LENGTH_LONG).show();
                            }

                        }
                    });
        }
        catch (Exception e)
        {

        }
    }


    public static String extractYoutubeVideoId(String ytUrl) {

        String vId = null;

        String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";

        Pattern compiledPattern = Pattern.compile(pattern);
//        Matcher matcher = compiledPattern.matcher(ytUrl);
        Matcher matcher = compiledPattern.matcher(ytUrl);

        if (matcher.find()) {
            vId = matcher.group();
        }
        return vId;
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Toast.makeText(mActivity, "pause", Toast.LENGTH_SHORT).show();
        try {
            if (youTubePlayer2 != null) {

                youTubePlayer2.pause();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        // Toast.makeText(mActivity, "Resume", Toast.LENGTH_SHORT).show();


        try {
            youTubePlayer2.release();
            Set_Player(currentPage);
            if (youTubePlayer2 != null) {
                youTubePlayer2.play();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }}