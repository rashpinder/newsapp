package com.dailysamachar.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dailysamachar.app.R;
import com.dailysamachar.app.RetrofitApi.ApiClient;
import com.dailysamachar.app.adapters.CounteryAdapter;
import com.dailysamachar.app.interfaces.ApiInterface;
import com.dailysamachar.app.interfaces.StateInterface;
import com.dailysamachar.app.model.CountryModel;
import com.dailysamachar.app.utils.NewsAppPreferences;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectCountryActivity extends BaseActivity implements StateInterface {
    /**
     * Getting the Current Class Name
     */
    String TAG = SelectCountryActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = SelectCountryActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.selectedCountryRV)
    RecyclerView selectedCountryRV;
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    List<CountryModel.CountryItem> mCountryArrayList;
    CounteryAdapter mAdapter;
    StateInterface stateInterface;
    String countryname;
    // String strIndia = "India";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_country);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        stateInterface = this;
        getCountryList();

    }

    private void getCountryList() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            if (mCountryArrayList != null) {
                mCountryArrayList.clear();
            }
            executeCountryDropDownApi();
        }
    }

    private void executeCountryDropDownApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getCountryRequest().enqueue(new Callback<CountryModel>() {
            @Override
            public void onResponse(Call<CountryModel> call, Response<CountryModel> response) {

                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                CountryModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    mCountryArrayList = new ArrayList<>();
                    mCountryArrayList = mModel.getCountry();

                    setCountryAdapter();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<CountryModel> call, Throwable t) {
                dismissProgressDialog();
                t.printStackTrace();
            }
        });
    }

    private void setCountryAdapter() {
        mAdapter = new CounteryAdapter(mActivity, mCountryArrayList, stateInterface, NewsAppPreferences.readString(mActivity, NewsAppPreferences.COUNTRY, countryname));
        selectedCountryRV.setLayoutManager(new LinearLayoutManager(this));
        selectedCountryRV.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

    }

    @OnClick({R.id.imgBackIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackIV:
                performBackClick();
                break;
        }
    }


    private void performBackClick() {
        startActivity(new Intent(mActivity, HomeActivity.class));
        finish();
    }

    // override interface method
    @Override
    public void state(String title) {
        countryname = title;

        NewsAppPreferences.writeString(mActivity, NewsAppPreferences.COUNTRY, countryname);

        if (NewsAppPreferences.readString(mActivity, NewsAppPreferences.COUNTRY, null).equals("India")) {
            NewsAppPreferences.writeString(mActivity, NewsAppPreferences.STATE, "Punjab");
            NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.IS_FIRST_TIME, true);

        } else if (NewsAppPreferences.readString(mActivity, NewsAppPreferences.COUNTRY, null) != "India") {
            NewsAppPreferences.writeString(mActivity, NewsAppPreferences.STATE, "Delhi");
            NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.IS_FIRST_TIME, true);
        }

        Intent intent = new Intent(mActivity, HomeActivity.class);
        intent.putExtra("country", countryname);
        startActivity(intent);
        finish();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}