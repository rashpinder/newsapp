package com.dailysamachar.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.dailysamachar.app.utils.Constants;
import com.dailysamachar.app.R;
import com.wang.avi.AVLoadingIndicatorView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WebViewActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = WebViewActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = WebViewActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.txtTitleTV)
    TextView txtTitleTV;
    @BindView(R.id.webViewWV)
    WebView webViewWV;
    @BindView(R.id.mProgressBarPB)
    AVLoadingIndicatorView mProgressBarPB;

    /*
     * Initialize object...
     * */

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        ButterKnife.bind(this);
        //Get Intent Data
        setStatusBar(mActivity);
        getIntentData();
    }

    private void getIntentData() {
       if (getIntent().getStringExtra(Constants.WV_TYPE).equals(Constants.PRIVACY_POLICY)) {
            setPrivacyPolicy(Constants.PRIVACY_POLICY_WEB);
        } else if (getIntent().getStringExtra(Constants.WV_TYPE).equals(Constants.ABOUT)) {
            setHelpSupport(Constants.ABOUT_WEB);
        } else if (getIntent().getStringExtra(Constants.WV_TYPE).equals(Constants.TERMS_SERVICES)) {
            setTermsServices(Constants.TERMS_WEB);
        }else if (getIntent().getStringExtra(Constants.WV_TYPE).equals(Constants.CONTACT_US)) {
           Log.e(TAG,"YRU::"+Constants.CONTACT_WEB);
           setContactServices(Constants.CONTACT_WEB);

        }
    }

    @OnClick(R.id.imgBackIV)
    public void onViewClicked() {
        if(getIntent().getStringExtra(Constants.WV_TYPE).equals(Constants.CONTACT_US)){
            startActivity(new Intent(mActivity, HomeActivity.class));
            finish();
        }
        else {
            onBackPressed();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    private void setDataOnWidgets(String strWebUrl) {
        mProgressBarPB.setVisibility(View.VISIBLE);
        // To Show PDF IN WebView
        webViewWV.getSettings().setJavaScriptEnabled(true);
        webViewWV.getSettings().setBuiltInZoomControls(true);
        webViewWV.getSettings().setLoadWithOverviewMode(true);
        webViewWV.getSettings().setUseWideViewPort(true);
        webViewWV.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                mProgressBarPB.setVisibility(View.VISIBLE);
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, final String url) {
                mProgressBarPB.setVisibility(View.GONE);
            }
        });

        webViewWV.loadUrl(strWebUrl);
    }


    private void setPrivacyPolicy(String strUrl) {
        txtTitleTV.setText(getString(R.string.privacy_policy));
        setDataOnWidgets(strUrl);
    }


    private void setHelpSupport(String strUrl) {
        txtTitleTV.setText(getString(R.string.about));
        setDataOnWidgets(strUrl);
    }

    private void setTermsServices(String strUrl) {
        txtTitleTV.setText(getString(R.string.terms_of_services));
        setDataOnWidgets(strUrl);
    }
    private void setContactServices(String strUrl) {
        txtTitleTV.setText(getString(R.string.contact_uss));
        setDataOnWidgets(strUrl);
    }
}
