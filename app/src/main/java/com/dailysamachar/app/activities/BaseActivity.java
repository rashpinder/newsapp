package com.dailysamachar.app.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.dailysamachar.app.utils.NewsAppPreferences;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.dailysamachar.app.R;
import com.google.firebase.messaging.FirebaseMessaging;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class BaseActivity extends AppCompatActivity {

    /*
    * Get Class Name
    * */
    String TAG = BaseActivity.this.getClass().getSimpleName();
    /*
    * Initialize Activity
    * */
    Activity mActivity = BaseActivity.this;

    /*
    * Initialize Other Classes Objects...
    * */
    public Dialog progressDialog;
    public String strDeviceToken;

    /*
    * Get Is User Login
    * */
    public Boolean IsUserLogin(){
        return NewsAppPreferences.readBoolean(mActivity, NewsAppPreferences.ISLOGIN,false);
    }

    public Boolean isFirstTym(){
        return NewsAppPreferences.readBoolean(mActivity, NewsAppPreferences.IS_FIRST_TIME,true);
    }
    /*
    * Get User ID
    * */
    public String getID(){
        return NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID,"");
    }

    /*
    * Get User Email ID
    * */
    public String getUserEmailID(){
        return NewsAppPreferences.readString(mActivity, NewsAppPreferences.EMAIL,"");
    }

    /*
     * Get User AuthToken
     * */
    public String getUserAuthToken(){
        return NewsAppPreferences.readString(mActivity, NewsAppPreferences.AUTH_TOKEN,"");
    }

    /*
    * Get User Name
    * */
    public String getUserName(){
        return NewsAppPreferences.readString(mActivity, NewsAppPreferences.USERNAME,"");
    }

    /*
    * Get User Profile Picture
    * */
    public String getUserProfilePicture(){
        return NewsAppPreferences.readString(mActivity, NewsAppPreferences.IMAGE,"");
    }

//    device token

    public void getDeviceToken() {
        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
            @Override
            public void onComplete(@NonNull Task<String> task) {

                if (!task.isSuccessful()) {
                    Log.e(TAG, "**Get Instance Failed**", task.getException());
                    return;
                }

                // Get new Instance ID token
                strDeviceToken = task.getResult();
                NewsAppPreferences.writeString(mActivity, NewsAppPreferences.DEVICE_TOKEN, strDeviceToken);
                Log.e(TAG, "**Push Token**" + strDeviceToken);
            }
        });
    }
//    public void getDeviceToken() {
//        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
//            @Override
//            public void onComplete(@NonNull Task<String> task) {
//
//                if (!task.isSuccessful()) {
//                    Log.e(TAG, "**Get Instance Failed**", task.getException());
//                    return;
//                }
//
//                // Get new Instance ID token
//                strDeviceToken = task.getResult();
//                NewsAppPreferences.writeString(mActivity, NewsAppPreferences.DEVICE_TOKEN, strDeviceToken);
//                Log.e(TAG, "**Push Token**" + strDeviceToken);
//            }
//        });
//    }
//    public void getDeviceToken() {
//        FirebaseInstanceId.getInstance().getInstanceId()
//                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
//                        if (!task.isSuccessful()) {
//                            Log.e(TAG, "**Get Instance Failed**", task.getException());
//                            return;
//                        }
//
//                        // Get new Instance ID token
//                        strDeviceToken = task.getResult().getToken();
//                        NewsAppPreferences.writeString(mActivity, NewsAppPreferences.DEVICE_TOKEN, strDeviceToken);
//                        Log.e(TAG, "**Push Token**" + strDeviceToken);
//                    }
//                });
//    }

    /*
    transparent status bar
     */
    public void setStatusBar(Activity mActivity){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mActivity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            // edited here
            mActivity.getWindow().setStatusBarColor(getResources().getColor(R.color.colorWhite));
        }}

    /*
     * Finish the activity
     * */
    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionSlideDownExit();
    }

    /*to switch between fragments*/
    public void switchFragment(final Fragment fragment, final String Tag, final boolean addToStack, final Bundle bundle) {
        FragmentManager fragmentManager= getSupportFragmentManager();
        if (fragment != null) {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.rl, fragment, Tag);
            if (addToStack)
                fragmentTransaction.addToBackStack(Tag);
            if (bundle != null)
                fragment.setArguments(bundle);
            fragmentTransaction.commit();
            fragmentManager.executePendingTransactions();
        }
    }


    /*
     * To Start the New Activity
     * */
    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionSlideUPEnter();
    }

    /**
     * Overrides the pending Activity transition by performing the "Enter" animation.
     */
    public void overridePendingTransitionSlideUPEnter() {
        overridePendingTransition(R.anim.bottom_up, 0);
    }

    /**
     * Overrides the pending Activity transition by performing the "Exit" animation.
     */
    public void overridePendingTransitionSlideDownExit() {
        overridePendingTransition(0, R.anim.bottom_down);
    }


    /*
     * Show Progress Dialog
     * */

    public void showProgressDialog(Activity mActivity) {
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        Objects.requireNonNull(progressDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        if (progressDialog != null)
            progressDialog.show();
    }



    /*
     * Hide Progress Dialog
     * */
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    /*
     * Hide Keyboard
     * */
    public boolean hideKeyBoard(Context context, View view) {
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
        return true;
    }

//    /*to switch between fragments*/
//    public void switchFragment(final Fragment fragment, final String Tag, final boolean addToStack, final Bundle bundle) {
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        if (fragment != null) {
//            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//            fragmentTransaction.replace(R.id.mainFL, fragment, Tag);
//            if (addToStack)
//                fragmentTransaction.addToBackStack(Tag);
//            if (bundle != null)
//                fragment.setArguments(bundle);
//            fragmentTransaction.commit();
//            fragmentManager.executePendingTransactions();
//        }}

    /*
     * Validate Email Address
     * */
    public boolean isValidEmaillId(String email) {
        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }


    /*
     * Share Gmail Intent
     * */
    public void shareIntentGmail(Activity mActivity, String profileUrl) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Deal Breaker:");
        Log.e(TAG, "shareIntentGmail: "+profileUrl );
        sharingIntent.putExtra(Intent.EXTRA_TEXT, "Hey check out friend profile and share your view : " + profileUrl);
        mActivity.startActivity(Intent.createChooser(sharingIntent, "Choose One"));
    }

    /*
     * Check Internet Connections
     * */
    public boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    /*
     * Toast Message
     * */
    public void showToast(Activity mActivity, String strMessage) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show();
    }


    /*
     *
     * Error Alert Dialog
     * */
    public void showAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    /*
     *
     * Error Alert Dialog
     * */
    public void showAlertDialogFinish(final Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                mActivity.finish();
            }
        });
        alertDialog.show();
    }

    public void showTimePicker(TextView mTextview)
    {
    Calendar mcurrentTime = Calendar.getInstance();
    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
    int minute = mcurrentTime.get(Calendar.MINUTE);
    int am = mcurrentTime.get(Calendar.AM_PM);

    TimePickerDialog mTimePicker;
    mTimePicker = new TimePickerDialog(mActivity, new TimePickerDialog.OnTimeSetListener() {
    @Override
    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
//    String am_pm;
//    if (selectedHour >= 12) {
//    am_pm = "PM";
//    } else {
//    am_pm = "AM";
//    }
    // mTextview.setText(selectedHour + ":" + selectedMinute);
    mTextview.setText(selectedHour + ":" + convertDate(selectedMinute));
    mTextview.setTextColor(Color.BLACK);

    }
    }, hour, minute,  DateFormat.is24HourFormat(mActivity));//Yes 24 hour time
    mTimePicker.setTitle("Select Time");

    mTimePicker.show();
    }

    public static String convertDate(int input) {
        if (input >= 10) {
            return String.valueOf(input);
        } else {
            return "0" + String.valueOf(input);
        }
    }


        public String formatException(Exception exception) {
            String formattedString = "Internal Error";
            Log.e("BaseActivity", " -- Error: " + exception.toString());
            Log.getStackTraceString(exception);

            String temp = exception.getMessage();

            if (temp != null && temp.length() > 0) {
                formattedString = temp.split("\\(")[0];
                if (temp != null && temp.length() > 0) {
                    return formattedString;
                }
            }

            return formattedString;
        }


        /*
         * Date Picker Dialog
         * */
    public void showDatePickerDialog(Activity mActivity, final TextView mTextView) {
        int mYear, mMonth, mDay, mHour, mMinute;
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(mActivity,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        int intMonth = monthOfYear + 1;
                        mTextView.setText(year + "-" + getFormatedString("" + intMonth) + "-" + getFormatedString("" + dayOfMonth));
                    }
                }, mYear, mMonth, mDay);

//        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
//        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() - 568025136000L);

        datePickerDialog.show();
    }


    public boolean isValidDateOfBirth(TextView mTextView) {
        boolean isDateValid = false;
        String dob = mTextView.getText().toString().trim();

        long mTimeInLong = getTimeInLong(dob);
        long difference = System.currentTimeMillis() - mTimeInLong;
        long dy = TimeUnit.MILLISECONDS.toDays(difference);

        final long years = dy / 365;

        isDateValid = years >= 18;
        Log.e(TAG, "isValidDateOfBirth: " + isDateValid);
        Log.e(TAG, "isValidDateOfBirth: " + years);

        return isDateValid;
    }


    private long getTimeInLong(String mDate) {
        long startDate = 0;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date date = sdf.parse(mDate);

            startDate = date.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return startDate;
    }


    public String getFormatedString(String strTemp) {
        String strActual = "";

        if (strTemp.length() == 1) {
            strActual = "0" + strTemp;
        } else if (strTemp.length() == 2) {
            strActual = strTemp;
        }

        return strActual;
    }


    /**
     * Turn drawable into byte array.
     *
     * @param drawable data
     * @return byte array
     */
    public static byte[] getFileDataFromDrawable(Context context, Drawable drawable) {
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }


    //1 minute = 60 seconds
    //1 hour = 60 x 60 = 3600
    //1 day = 3600 x 24 = 86400
    public long calculateDateDifference(String strDateOfBirth) {
        Date startDate = null;
        Date endDate = null;


        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy");

        try {
            startDate = simpleDateFormat.parse(strDateOfBirth);
            endDate = simpleDateFormat.parse(getCurrentDate());

        } catch (ParseException e) {
            e.printStackTrace();
        }


        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : " + endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

//        System.out.printf("%d days, %d hours, %d minutes, %d seconds%n",elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);

        long years = elapsedDays / 365;


        return years;
    }


    public String getCurrentDate() {
        Date d = new Date();
        CharSequence mCurrentDate = DateFormat.format("dd/mm/yyyy", d.getTime());
        return mCurrentDate.toString();
    }





}
