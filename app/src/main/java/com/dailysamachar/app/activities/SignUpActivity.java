package com.dailysamachar.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dailysamachar.app.RetrofitApi.ApiClient;
import com.dailysamachar.app.interfaces.ApiInterface;
import com.dailysamachar.app.model.SignUpModel;
import com.dailysamachar.app.utils.Constants;
import com.dailysamachar.app.utils.NewsAppPreferences;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.dailysamachar.app.R;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONObject;

import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;

public class SignUpActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    /**
     * Getting the Current Class Name
     */
    String TAG = SignUpActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = SignUpActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.editEmailET)
    EditText editEmailET;
    @BindView(R.id.editPasswordET)
    EditText editPasswordET;
    @BindView(R.id.editNameET)
    EditText editNameET;
    @BindView(R.id.txtSignUpTV)
    TextView txtSignUpTV;
    @BindView(R.id.imgGoogleIV)
    ImageView imgGoogleIV;
    @BindView(R.id.imgFbIV)
    ImageView imgFbIV;
    @BindView(R.id.imgCallIV)
    ImageView imgCallIV;
    @BindView(R.id.txtSignInTV)
    TextView txtSignInTV;
    @BindView(R.id.txtTermsTV)
    TextView txtTermsTV;
    @BindView(R.id.imgTermsIV)
    ImageView imgTermsIV;
    @BindView(R.id.termsRL)
    RelativeLayout termsRL;
    GoogleSignInClient mGoogleSignInClient;
    protected static final int GOOGLE_SIGN_IN = 12324;
    protected GoogleApiClient mGoogleApiClient;
    String strDeviceToken;
    boolean terms = true;
    boolean checked = false;

    CallbackManager mCallbackManager;
    //Firebase
    private FirebaseAuth mFirebaseAuth;
    private String fbEmail, fbLastName, fbFirstName, fbId, userName, fbSocialUserserName;
    String Username = "";

    String facebook_id = "";
    String google_id = "";
    String email = "";
    String google_username = "";
    String GoogleProfilePicture = "";

    private URL fbProfilePicture;
    String FacebookProfilePicture = "";

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        buildGoogleApiClient();
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        // Initialize Facebook Login button
        FirebaseApp.initializeApp(this);


        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mFirebaseAuth = FirebaseAuth.getInstance();
        mCallbackManager = CallbackManager.Factory.create();

        //Logout First
        FirebaseAuth.getInstance().signOut();
        LoginManager.getInstance().logOut();
        getDeviceToken();
        customTextView(txtTermsTV);
    }


    public void getDeviceToken() {
        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
            @Override
            public void onComplete(@NonNull Task<String> task) {

                if (!task.isSuccessful()) {
                    Log.e(TAG, "**Get Instance Failed**", task.getException());
                    return;
                }

                // Get new Instance ID token
                strDeviceToken = task.getResult();
                NewsAppPreferences.writeString(mActivity, NewsAppPreferences.DEVICE_TOKEN, strDeviceToken);
                Log.e(TAG, "**Push Token**" + strDeviceToken);
            }
        });
    }
//    public void getDeviceToken() {
//        FirebaseInstanceId.getInstance().getInstanceId()
//                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
//                        if (!task.isSuccessful()) {
//                            Log.e(TAG, "**Get Instance Failed**", task.getException());
//                            return;
//                        }
//
//                        // Get new Instance ID token
//                        strDeviceToken = task.getResult().getToken();
//                        NewsAppPreferences.writeString(mActivity, NewsAppPreferences.DEVICE_TOKEN, strDeviceToken);
//                        Log.e(TAG, "**Push Token**" + strDeviceToken);
//                    }
//                });
//    }


    private void customTextView(TextView view) {
        SpannableStringBuilder spanTxt = new SpannableStringBuilder(
                "I agree with ");
        spanTxt.append("terms & conditions");
        spanTxt.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(Constants.TERMS_WEB));
                startActivity(intent);
            }
        }, spanTxt.length() - "terms & conditions".length(), spanTxt.length(), 0);
        view.setMovementMethod(LinkMovementMethod.getInstance());
        view.setText(spanTxt, TextView.BufferType.SPANNABLE);
    }
    @OnClick({R.id.txtSignUpTV, R.id.imgGoogleIV, R.id.imgFbIV, R.id.imgCallIV,R.id.txtSignInTV, R.id.termsRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtSignUpTV:
                performSignUpClick();
                break;
            case R.id.imgGoogleIV:
                signIn();
                break;
            case R.id.imgFbIV:
                performFbClick();
                break;
            case R.id.imgCallIV:
                performCallClick();
                break;
            case R.id.txtSignInTV:
                performSignInClick();
                break;
            case R.id.termsRL:
                performTermsClick();
                break;
        }
    }

    private void performTermsClick() {
        if (terms) {
            imgTermsIV.setImageResource(R.drawable.ic_check);
            terms = false;
            checked = true;

        } else {
            imgTermsIV.setImageResource(R.drawable.ic_uncheck);
            terms = true;
            checked = false;
        }
        return;
    }

    private void performSignInClick() {
        Intent intent = new Intent(mActivity, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private void performCallClick() {
        Intent intent = new Intent(mActivity, LoginWithPhNoActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
//        finish();
    }

    private void performSignUpClick() {
        if (isValidate()) {
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.internet_connection_error));
            } else {
                executeSignUpApi();
            }
        }
    }


        /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("email", editEmailET.getText().toString().trim());
        mMap.put("password", editPasswordET.getText().toString().trim());
        mMap.put("userName", editNameET.getText().toString());
        mMap.put("deviceType", "2");
        mMap.put("deviceToken",strDeviceToken);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }


    private void executeSignUpApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.signUpRequest(mParam()).enqueue(new retrofit2.Callback<SignUpModel>() {
            @Override
            public void onResponse(Call<SignUpModel> call, Response<SignUpModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                SignUpModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    showToast(mActivity, mModel.getMessage());
                    NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.ISLOGIN, true);
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.ID, mModel.getData().getUserID());
                    Log.e(TAG, "onResponse: sj"+ mModel.getData().getUserID());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.EMAIL, mModel.getData().getEmail());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.VERIFIED, mModel.getData().getVerified());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.ADMIN, mModel.getData().getAdmin());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.CREATION_AT, mModel.getData().getCreated());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.PASSWORD, response.body().getData().getPassword());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.IMAGE, response.body().getData().getProfileImage());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.FB_TOKEN, response.body().getData().getFacebookToken());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.GOOGLE_TOKEN, response.body().getData().getGoogleToken());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.PH_NO, response.body().getData().getPhoneNo());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.BIO, response.body().getData().getBio());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.USERNAME, response.body().getData().getName());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.VER_CODE, response.body().getData().getVerificationCode());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.AUTH_TOKEN, response.body().getData().getAuthToken());

                    Intent intent = new Intent(mActivity, HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SignUpModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    /*
     * Set up validations for Sign In fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_name));
            flag = false;
        }
        else if (editEmailET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_email_address));
            flag = false;
        } else if (!isValidEmaillId(editEmailET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address));
            flag = false;
        } else if (editPasswordET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password));
            flag = false;
        }
        else if ((editPasswordET.getText().toString().trim().length() < 6)) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_password));
            flag = false;
        }
        else if (!checked) {
            showAlertDialog(mActivity, getString(R.string.please_select_terms));
            flag = false;
        }
        return flag;
    }

    /**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the
     * LocationServices API.
     */

    protected synchronized void buildGoogleApiClient() {
        Log.i("", "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private void signIn() {
//        signOut();
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, GOOGLE_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case GOOGLE_SIGN_IN:
                Task<GoogleSignInAccount> completedTask = GoogleSignIn.getSignedInAccountFromIntent(data);
                handleSignInResult(completedTask);
                break;
            default:
                mCallbackManager.onActivityResult(requestCode, resultCode, data);
                break;

        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            email = account.getEmail();
            google_id = account.getId();
            google_username = account.getDisplayName();

            if (account.getPhotoUrl() != null) {
                GoogleProfilePicture = account.getPhotoUrl().toString();
//                    mBase64GoogleImage=convertUrlToBase64(GoogleProfilePicture);
            } else {
                GoogleProfilePicture = "";
//                    mBase64GoogleImage="";
            }
            executeGoogleApii();

        } catch (ApiException e) {
            Log.e("MyTAG", "signInResult:failed code=" + e.getStatusCode());
            Toast.makeText(mActivity, "Failed", Toast.LENGTH_LONG);

        }
    }

    private void executeGoogleApii() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            loginWithGoogle();
        }
    }

    /*
     * Execute google api
     * */
    private Map<String, String> mGoogleParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("email", email);
        mMap.put("userName", google_username);
        mMap.put("googleToken", google_id);
        mMap.put("deviceType", "2");
        mMap.put("deviceToken",strDeviceToken);
        mMap.put("image", GoogleProfilePicture);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }


    private void loginWithGoogle() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.loginWithGoogleRequest(mGoogleParams()).enqueue(new retrofit2.Callback<SignUpModel>() {
            @Override
            public void onResponse(Call<SignUpModel> call, Response<SignUpModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                SignUpModel mModel = response.body();
                if (mModel.getStatus().equalsIgnoreCase("1")) {
                    showToast(mActivity, mModel.getMessage());
                    NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.ISLOGIN, true);
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.ID, mModel.getData().getUserID());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.EMAIL, mModel.getData().getEmail());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.VERIFIED, mModel.getData().getVerified());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.ADMIN, mModel.getData().getAdmin());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.CREATION_AT, mModel.getData().getCreated());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.PASSWORD, response.body().getData().getPassword());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.IMAGE, response.body().getData().getProfileImage());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.FB_TOKEN, response.body().getData().getFacebookToken());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.GOOGLE_TOKEN, response.body().getData().getGoogleToken());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.PH_NO, response.body().getData().getPhoneNo());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.BIO, response.body().getData().getBio());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.USERNAME, response.body().getData().getName());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.VER_CODE, response.body().getData().getVerificationCode());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.AUTH_TOKEN, response.body().getData().getAuthToken());

                    Intent intent = new Intent(mActivity, HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
                else{
                    showAlertDialog(mActivity,mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SignUpModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void loginWithFacebook() {
        if (isNetworkAvailable(mActivity)) {
            LoginManager.getInstance().logInWithReadPermissions(mActivity, Arrays.asList("public_profile", "email","user_friends"));
            LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    Log.d("onSuccess: ", loginResult.getAccessToken().getToken());
                    getFacebookData(loginResult);
                }

                @Override
                public void onCancel() {
                    Toast.makeText(mActivity, "Cancel", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(FacebookException error) {
                    Toast.makeText(mActivity, error.toString(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error));
        }
    }

    private void getFacebookData(LoginResult loginResult) {
        showProgressDialog(mActivity);
        GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                dismissProgressDialog();
                try {

                    if (object.has("id")) {
                        fbId = object.getString("id");
                        Log.e("LoginActivity", "id" + fbId);

                    }
                    //check permission first userName
                    if (object.has("first_name")) {
                        fbFirstName = object.getString("first_name");
                        Log.e("LoginActivity", "first_name" + fbFirstName);

                    }
                    //check permisson last userName
                    if (object.has("last_name")) {
                        fbLastName = object.getString("last_name");
                        Log.e("LoginActivity", "last_name" + fbLastName);
                    }
                    //check permisson email
                    if (object.has("email")) {
                        fbEmail = object.getString("email");
                        Log.e("LoginActivity", "email" + fbEmail);
                    }

                    JSONObject jsonObject = new JSONObject(object.getString("picture"));
                    if (jsonObject != null) {
                        JSONObject dataObject = jsonObject.getJSONObject("data");
                        Log.e("Loginactivity", "json oject get picture" + dataObject);
                        fbProfilePicture = new URL("https://graph.facebook.com/" + fbId + "/picture?width=500&height=500");
                        Log.e("LoginActivity", "json object=>" + object.toString());
                    }
                    if (fbProfilePicture != null) {
                        FacebookProfilePicture = String.valueOf(fbProfilePicture);

                    } else {
                        FacebookProfilePicture = "";
                    }

                    fbSocialUserserName = fbFirstName + " " + fbLastName;


                    Username = fbSocialUserserName;
                    email = fbEmail;
                    facebook_id = fbId;

                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.FACEBOOK_IDD, facebook_id);

                    executeLoginWithFbApi();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        Bundle bundle = new Bundle();
        Log.e("LoginActivity", "bundle set");
        bundle.putString("fields", "id, first_name, last_name,email,picture");
        graphRequest.setParameters(bundle);
        graphRequest.executeAsync();
    }

    /*
     * Execute fb api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("email", fbEmail);
        mMap.put("userName", Username);
        mMap.put("facebookToken", facebook_id);
        mMap.put("deviceType", "2");
        mMap.put("deviceToken",strDeviceToken);
        mMap.put("image", FacebookProfilePicture);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeLoginWithFbApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.loginWithFbRequest(mParams()).enqueue(new retrofit2.Callback<SignUpModel>() {
            @Override
            public void onResponse(Call<SignUpModel> call, Response<SignUpModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                SignUpModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    showToast(mActivity, mModel.getMessage());
                    NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.ISLOGIN, true);
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.ID, mModel.getData().getUserID());
                    Log.e(TAG, "onResponseID: "+mModel.getData().getUserID() );
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.EMAIL, mModel.getData().getEmail());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.VERIFIED, mModel.getData().getVerified());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.ADMIN, mModel.getData().getAdmin());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.CREATION_AT, mModel.getData().getCreated());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.PASSWORD, response.body().getData().getPassword());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.IMAGE, response.body().getData().getProfileImage());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.FB_TOKEN, response.body().getData().getFacebookToken());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.GOOGLE_TOKEN, response.body().getData().getGoogleToken());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.PH_NO, response.body().getData().getPhoneNo());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.BIO, response.body().getData().getBio());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.USERNAME, response.body().getData().getName());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.VER_CODE, response.body().getData().getVerificationCode());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.AUTH_TOKEN, response.body().getData().getAuthToken());

                    Intent intent = new Intent(mActivity, HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else{
                    showAlertDialog(mActivity,mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SignUpModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    // [START signOut]
    private void signOut() {
        if (mGoogleSignInClient != null) {
            mGoogleSignInClient.signOut()
                    .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            // [START_EXCLUDE]
                            Log.e(TAG, "==Logout Successfully==");
                            // [END_EXCLUDE]
                        }
                    });
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void performFbClick() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            FacebookSdk.sdkInitialize(getApplicationContext());
            LoginManager.getInstance().logOut();
            AppEventsLogger.activateApp(getApplication());
            mCallbackManager = CallbackManager.Factory.create();
            loginWithFacebook();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
