package com.dailysamachar.app.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.dailysamachar.app.RetrofitApi.ApiClient;
import com.dailysamachar.app.interfaces.ApiInterface;
import com.dailysamachar.app.model.SignUpModel;
import com.dailysamachar.app.utils.Constants;
import com.dailysamachar.app.utils.NewsAppPreferences;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.dailysamachar.app.R;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerifyOtpActivity extends BaseActivity {
    Activity mActivity = VerifyOtpActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = VerifyOtpActivity.this.getClass().getSimpleName();

    @BindView(R.id.et1)
    EditText et1;
//    @BindView(R.id.et2)
//    EditText et2;
//    @BindView(R.id.et3)
//    EditText et3;
//    @BindView(R.id.et4)
//    EditText et4;
//    @BindView(R.id.et5)
//    EditText et5;
//    @BindView(R.id.et6)
//    EditText et6;
    @BindView(R.id.txtVerifyTV)
    TextView txtVerifyTV;
    @BindView(R.id.txtPhNoTV)
    TextView txtPhNoTV;
    @BindView(R.id.txtResendOTP)
    TextView txtResendOTP;

    String mPh = "", mVerificationId = "", otp = "", code = "";
    String mDialCode;
    PhoneAuthProvider.ForceResendingToken mResendToken;

    FirebaseAuth mAuth = FirebaseAuth.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otp);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        if (getIntent() != null) {
            Intent intent = getIntent();

            mPh = intent.getStringExtra(Constants.PHONE_NUMBER);
            txtPhNoTV.setText(mPh);
        }
//        mDialCode = intent.getStringExtra(Constants.DIAL_CODE);

//        et1.setText(null);
//        et2.setText(null);
//        et3.setText(null);
//        et4.setText(null);
//        et5.setText(null);
//        et6.setText(null);

//        et1.addTextChangedListener(new GenericTextWatcher(et1));
//        et2.addTextChangedListener(new GenericTextWatcher(et2));
//        et3.addTextChangedListener(new GenericTextWatcher(et3));
//        et4.addTextChangedListener(new GenericTextWatcher(et4));
//        et5.addTextChangedListener(new GenericTextWatcher(et5));
//        et6.addTextChangedListener(new GenericTextWatcher(et6));

        sendVerificationCode();
    }

    private void sendVerificationCode() {
        showProgressDialog(mActivity);
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                mPh,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks

    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onVerificationCompleted(PhoneAuthCredential credential) {
            // This callback will be invoked in two situations:
            // 1 - Instant verification. In some cases the phone number can be instantly
            //     verified without needing to send or enter a verification code.
            // 2 - Auto-retrieval. On some devices Google Play services can automatically
            //     detect the incoming verification SMS and perform verification without
            //     user action.
            dismissProgressDialog();
            Log.d(TAG, "onVerificationCompleted:" + credential);

            code = credential.getSmsCode();

//            if (code != null) {
//
//                String c1, c2, c3, c4, c5, c6;
//
//                c1 = code.substring(0, 1);
//                c2 = code.substring(1, 2);
//                c3 = code.substring(2, 3);
//                c4 = code.substring(3, 4);
//                c5 = code.substring(4, 5);
//                c6 = code.substring(5, 6);

                et1.setText(code);
//                et2.setText(c2);
//                et3.setText(c3);
//                et4.setText(c4);
//                et5.setText(c5);
//                et6.setText(c6);

//               verifying the code
                if (!isNetworkAvailable(mActivity)) {
                    showToast(mActivity, getString(R.string.internet_connection_error));
                } else {
                    verifyVerificationCode(code);
                }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            // This callback is invoked in an invalid request for verification is made,
            // for instance if the the phone number format is not valid.
            dismissProgressDialog();
            Log.w(TAG, "onVerificationFailed", e);

            if (e instanceof FirebaseAuthInvalidCredentialsException) {
                // Invalid request
                // ...
            } else if (e instanceof FirebaseTooManyRequestsException) {
                // The SMS quota for the project has been exceeded
                // ...
                showToast(mActivity, "The SMS quota for this number has been exceeded.");
            }

            // Show a message and update the UI
            // ...
        }

        @Override
        public void onCodeSent(@NonNull String verificationId,
                               @NonNull PhoneAuthProvider.ForceResendingToken token) {
            // The SMS verification code has been sent to the provided phone number, we
            // now need to ask the user to enter the code and then construct a credential
            // by combining the code with a verification ID.
            dismissProgressDialog();
            Log.d(TAG, "onCodeSent:" + verificationId);

            // Save verification ID and resending token so we can use them later
            mVerificationId = verificationId;
            mResendToken = token;

            // ...
        }
    };

    private void verifyVerificationCode(String otp) {

        showProgressDialog(mActivity);

        if (mVerificationId != null && !mVerificationId.equals("")) {

            //creating the credential
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, otp);
            //signing the user
            signInWithPhoneAuthCredential(credential);

        } else {
            dismissProgressDialog();
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_code));
        }
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()) {

                            dismissProgressDialog();

                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");

                            FirebaseUser user = task.getResult().getUser();

                            phoneVerification();


                        } else {

                            dismissProgressDialog();

                            // Sign in failed, display a message and update the UI
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                showAlertDialog(mActivity, getString(R.string.please_enter_valid_code));
                            }
                        }
                    }
                });
    }

//    public class GenericTextWatcher implements TextWatcher {
//
//        private View view;
//
//        private GenericTextWatcher(View view) {
//            this.view = view;
//        }
//
//        @Override
//        public void afterTextChanged(Editable editable) {
//            String text = editable.toString();
//            switch (view.getId()) {
//
//                case R.id.et1:
//                    if (text.length() == 1) {
//                        et2.requestFocus();
//
//                    }
//                    break;
//
//                case R.id.et2:
//                    if (text.length() == 1) {
//                        et3.requestFocus();
//                    } else if (text.length() == 0) {
//                        et1.requestFocus();
//                    }
//                    break;
//
//                case R.id.et3:
//                    if (text.length() == 1) {
//                        et4.requestFocus();
//                    } else if (text.length() == 0) {
//                        et2.requestFocus();
//                    }
//                    break;
//                case R.id.et4:
//                    if (text.length() == 1) {
//                        et5.requestFocus();
//                    } else if (text.length() == 0) {
//                        et3.requestFocus();
//                    }
//                    break;
//                case R.id.et5:
//                    if (text.length() == 1) {
//                        et6.requestFocus();
//                    } else if (text.length() == 0) {
//                        et4.requestFocus();
//                    }
//                    break;
//
//                case R.id.et6:
//                    if (text.length() == 1) {
//                        txtVerifyTV.requestFocus();
//                    } else if (text.length() == 0) {
//                        et5.requestFocus();
//                    }
//                    break;
//            }
//        }

//        @Override
//        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
//
//        }
//
//        @Override
//        public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//        }
//    }

    private void validate() {

        otp = et1.getText().toString();
//                + et2.getText().toString() + et3.getText().toString() + et4.getText().toString() + et5.getText().toString() + et6.getText().toString();

        if (!otp.isEmpty()) {

            if (!isNetworkAvailable(mActivity)) {
                showToast(mActivity, getString(R.string.internet_connection_error));
            } else {
                verifyVerificationCode(otp);
            }

        } else {

            showAlertDialog(mActivity, getString(R.string.please_enter_code));
        }
    }

    public void showReqInfoDialog(Activity mActivity, String strHeading, String strDescriptions) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtCancelTV = alertDialog.findViewById(R.id.btnDismiss);
        TextView txtHeadingTV = alertDialog.findViewById(R.id.txtTitleTV);
        TextView txtDescriptionTV = alertDialog.findViewById(R.id.txtMessageTV);

        txtCancelTV.setText("OK");
        txtHeadingTV.setVisibility(View.GONE);

        txtHeadingTV.setText(strHeading);
        txtDescriptionTV.setText(strDescriptions);

        txtCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewsAppPreferences.writeString(mActivity, NewsAppPreferences.PHONE_NUMBER, mPh);
                alertDialog.dismiss();
                onBackPressed();
            }
        });
        alertDialog.show();
    }


    @OnClick({R.id.txtVerifyTV, R.id.txtResendOTP, R.id.imgBackIV})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtVerifyTV:
                performSubmitClick();
                break;

            case R.id.txtResendOTP:
                performResendClick();
                break;

                case R.id.imgBackIV:
                onBackPressed();
//                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        FirebaseSignOut();
        Intent i=new Intent(mActivity, LoginWithPhNoActivity.class);
        startActivity(i);
        finish();
    }

    private void performResendClick() {
        if (!isNetworkAvailable(mActivity)) {
            showToast(mActivity, getString(R.string.internet_connection_error));
        } else {
//            et1.setText(null);
//            et2.setText(null);
//            et3.setText(null);
//            et4.setText(null);
//            et5.setText(null);
//            et6.setText(null);
            et1.requestFocus();
            showProgressDialog(mActivity);
            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                    mPh,        // Phone number to verify
                    60,                 // Timeout duration
                    TimeUnit.SECONDS,   // Unit of timeout
                    this,               // Activity (for callback binding)
                    mCallbacks, mResendToken);        // OnVerificationStateChangedCallbacks
        }
    }

    private void performSubmitClick() {
        validate();
    }

    private void FirebaseSignOut() {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        mAuth.signOut();
    }


    private void phoneVerification() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executePhoneVerificationApi();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("phoneNo", mPh);
        mMap.put("deviceType", "2");
        mMap.put("deviceToken", NewsAppPreferences.readString(mActivity, NewsAppPreferences.DEVICE_TOKEN,null));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executePhoneVerificationApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.phoneNoRequest(mParams()).enqueue(new Callback<SignUpModel>() {
            @Override
            public void onResponse(Call<SignUpModel> call, Response<SignUpModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                SignUpModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    showToast(mActivity, mModel.getMessage());
                    NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.ISLOGIN, true);
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.ID, mModel.getData().getUserID());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.EMAIL, mModel.getData().getEmail());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.VERIFIED, mModel.getData().getVerified());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.ADMIN, mModel.getData().getAdmin());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.CREATION_AT, mModel.getData().getCreated());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.PASSWORD, response.body().getData().getPassword());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.IMAGE, response.body().getData().getProfileImage());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.FB_TOKEN, response.body().getData().getFacebookToken());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.GOOGLE_TOKEN, response.body().getData().getGoogleToken());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.PH_NO, response.body().getData().getPhoneNo());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.BIO, response.body().getData().getBio());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.USERNAME, response.body().getData().getName());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.VER_CODE, response.body().getData().getVerificationCode());
                    NewsAppPreferences.writeString(mActivity, NewsAppPreferences.AUTH_TOKEN, response.body().getData().getAuthToken());

                    Intent intent = new Intent(mActivity, HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SignUpModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

//    private void executePhoneVerificationApi() {
//        showProgressDialog(mActivity);
//        String mApiUrl = ConstantData.UPDATE_PHONE_VERIFICATION;
//        Log.e(TAG, "**Api Url**" + mApiUrl);
//        JSONObject params = new JSONObject();
//        try {
//            params.put("user_id", getUserID());
//            params.put("phone", mPh);
//        } catch (Exception e) {
//            e.toString();
//        }
//        Log.e(TAG, "**PARAMS**" + params);
//        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, mApiUrl, params, new Response.Listener<JSONObject>() {
//            @RequiresApi(api = Build.VERSION_CODES.M)
//            @Override
//            public void onResponse(JSONObject response) {
//                dismissProgressDialog();
//                Log.e(TAG, "**RESPONSE**" + response);
//
//                try {
//                    if (response.getString("status").equals("1")) {
//
//                        showReqInfoDialog(mActivity, "", "Your phone number has been verified");
//
//                    } else if (response.getString("status").equals("3")) {
//                        LogOut();
//
//                    } else {
//                        Toast.makeText(mActivity, response.getString("message"), Toast.LENGTH_SHORT).show();
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                dismissProgressDialog();
//                Log.e(TAG, "**ERROR**" + error);
//            }
//        });
//
//        Attire4hireApplication.getInstance().addToRequestQueue(jsonRequest);
//    }
}
