package com.dailysamachar.app.activities;

import static androidx.constraintlayout.motion.utils.Oscillator.TAG;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.dailysamachar.app.Config;
import com.dailysamachar.app.R;
import com.dailysamachar.app.RetrofitApi.ApiClient;
import com.dailysamachar.app.adapters.FAvoriteAdapter;
import com.dailysamachar.app.interfaces.AdsInterface;
import com.dailysamachar.app.interfaces.ApiInterface;
import com.dailysamachar.app.model.FavUnfavModel;
import com.dailysamachar.app.model.ForgotPasswordModel;
import com.dailysamachar.app.model.HomeModel;
import com.dailysamachar.app.utils.Constants;
import com.dailysamachar.app.utils.NewsAppPreferences;
import com.dailysamachar.app.utils.SimpleSideDrawer;
import com.facebook.ads.AdSize;
import com.facebook.ads.AudienceNetworkAds;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.material.navigation.NavigationView;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FavvActivity extends YouTubeBaseActivity
        implements FAvoriteAdapter.PaginationInterface, com.dailysamachar.app.interfaces.posInterface, AdsInterface {
    @BindView(R.id.imgFavoriteIV)
    ImageView imgFavoriteIV;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer_layout;
    @BindView(R.id.mProgressBar)
    ProgressBar mProgressBar;
    LinearLayoutManager layoutManager;
    int currentPage = -1;
    String address;
    Activity mActivity = FavvActivity.this;
    FAvoriteAdapter adapter;
    YouTubePlayer youTubePlayer2;
    YouTubePlayerView youTubeView;
    File imagePath;
    private View mPlayButtonLayout;
    private TextView mPlayTimeTextView;
    String item;
    private final Handler mHandler = null;
    private SeekBar mSeekBar;
    public static boolean img_fav;
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    Intent intent;
    String videoName;
    String newsId;
    List<HomeModel.Datum> mNewsList = new ArrayList<>();
    com.dailysamachar.app.interfaces.posInterface posInterface;
    private int page_no = 1;
    private final int item_count = 10;
    private String strLastPage = "FALSE";
    private List<HomeModel.Datum> tempArrayList = new ArrayList<>();
    FAvoriteAdapter.PaginationInterface paginationInterface;
    public Dialog progressDialog;
    boolean terms = true;
    String logo;
    String fav;
    int pos;
    public static int a = 0;
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    @BindView(R.id.recylerview_frag_home)
    RecyclerView recyclerView;
    public static int var;
    com.facebook.ads.AdView adView;
    Boolean isbuttonClicked = false;
    @BindView(R.id.txtNoDataFountTV)
    TextView txtNoDataFountTV;
    @BindView(R.id.imgMenuIV)
    ImageView imgMenuIV;
    public SimpleSideDrawer mSimpleSideDrawer;
    LinearLayout homeLL;
    ImageView imgHomeIV;
    TextView txtHomeTV;
    LinearLayout locationLL;
    ImageView imgLocIV;
    TextView txtLocationTV;
    LinearLayout favLL;
    ImageView imgFavIV;
    TextView txtFavTV;
    LinearLayout settingsLL;
    ImageView imgSettingsIV;
    TextView txtSettingsTV;
    LinearLayout logoutLL;
    ImageView imgLogoutIV;
    TextView txtLogoutTV;
    LinearLayout profileLL;
    TextView txtUsernameTV;
    ImageView imgProfileIV;
    TextView txtShareTV;
    ImageView imgShareIV;
    LinearLayout shareLL;
    String loc_sel;
    TextView txtSignInTV;
    LinearLayout languageLL;
    ImageView imgLanguageIV;
    TextView txtLanguageTV;
    @BindView(R.id.topRL)
    RelativeLayout topRL;
    AdsInterface adsInterface;

//    AdView adView;
    LinearLayout contactLL;
    ImageView imgContactIV;
    TextView txtContactTV;
    @BindView(R.id.navBar)
    NavigationView navBar;
    @BindView(R.id.content)
    RelativeLayout content;
    //    MuteInterface muteInterface;
//    AdView mAdView;
    ImageView img;
    //    private InterstitialAd interstitialAd;
//    InterstitialAdListener interstitialAdListener;
    int b;
    int orientation;
    int adsPos;
    boolean rem = true;
    AudioManager audioManager;
    int ITEMS_PER_AD = 4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favv);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
//        AudienceNetworkAds.initialize(this);
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        paginationInterface = this;
        this.posInterface = posInterface;
//        this.muteInterface = muteInterface;
        adsInterface = this;
        setNavigationDrawer();
        setNavigationViewListener();
//        getIntentDAta();

        showNewsList();
        AudienceNetworkAds.initialize(this);
//        mAdView = findViewById(R.id.adView);
        showAds();
//        adView = new AdView(this, "IMG_16_9_APP_INSTALL#445117679825548_495169711487011", AdSize.BANNER_HEIGHT_50);
//
//        // Find the Ad Container
//        LinearLayout adContainer = (LinearLayout) findViewById(R.id.banner_container);
//
//        // Add the ad view to your activity layout
//        adContainer.addView(adView);
//
//        // Request an ad
//        adView.loadAd();
//        ADS
//        AudienceNetworkAds.initialize(this);

//        adView = new AdView(this, "IMG_16_9_APP_INSTALL#445117679825548_495169711487011", AdSize.BANNER_HEIGHT_50);
//        // Find the Ad Container
//        LinearLayout adContainer = (LinearLayout) findViewById(R.id.banner_container);
//
//        // Add the ad view to your activity layout
//        adContainer.addView(adView);
//        // Request an ad
//        adView.loadAd();

    }


//    public void showAds() {
//        mAdView.setVisibility(View.VISIBLE);
//        AdRequest adRequest = new AdRequest.Builder()
//                .build();
//        mAdView.loadAd(adRequest);
//        mAdView.setAdListener(new AdListener() {
//            @Override
//            public void onAdLoaded() {
//
//                Log.d("mAdView", "onAdFailedToLoad. But why? " + "load");
//                // Code to be executed when an ad finishes loading.
//            }
//
//            @Override
//            public void onAdFailedToLoad(LoadAdError error) {
//                int errorCode = error.getCode();
//                String errorMessage = error.getMessage();
//                Log.e("mAdView", "onAdFailedToLoad. But why? " + errorCode + "and" + errorMessage);
//                // Code to be executed when an ad request fails.
//            }
//
//            @Override
//            public void onAdOpened() {
//
//                Log.d("mAdView", "onAdFailedToLoad. But why? " + "opened");
//                // Code to be executed when an ad opens an overlay that
//                // covers the screen.
//            }
//
//            @Override
//            public void onAdClicked() {
//                Log.d("mAdView", "onAdFailedToLoad. But why? " + "clicked");
//
//                // Code to be executed when the user clicks on an ad.
//            }
//
//
//            @Override
//            public void onAdClosed() {
//                Log.d("mAdView", "onAdFailedToLoad. But why? " + "closed");
//                // Code to be executed when the user is about to return
//                // to the app after tapping on an ad.
//            }
//        });
//    }

public void showAds() {
//        mAdView.setVisibility(View.VISIBLE);
    adView = new com.facebook.ads.AdView(this, "445117679825548_495169711487011", AdSize.BANNER_HEIGHT_50);
//        445117679825548_495169711487011
//        IMG_16_9_APP_INSTALL#YOUR_PLACEMENT_ID

// Find the Ad Container
    LinearLayout adContainer = (LinearLayout) findViewById(R.id.adView);

// Add the ad view to your activity layout
    adContainer.addView(adView);

// Request an ad
    adView.loadAd();

}

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    // This snippet shows the system bars. It does this by removing all the flags
// except for the ones that make the content appear under the system bars.
    private void showSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            topRL.setVisibility(View.VISIBLE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            showSystemUI();
            setStatusBar(mActivity);
        } else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            topRL.setVisibility(View.GONE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            setTheme(R.style.HomeTheme);
            drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            setStatusBar(mActivity);
            hideSystemUI();
        } else {
            topRL.setVisibility(View.VISIBLE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            showSystemUI();
            setStatusBar(mActivity);
        }

    }

    //    private void getIntentDAta() {
//        if (getIntent() != null) {
//            if (Objects.requireNonNull(getIntent().getExtras()).get("loc_sel") != null) {
//                loc_sel = (String) getIntent().getExtras().get("loc_sel");}
//            if (loc_sel.equals("2")){
//                txtFavTV.setTextColor(getResources().getColor(R.color.colorPink));
//                imgFavIV.setColorFilter(getResources().getColor(R.color.colorPink));
//            }
//        }
//    }
    private void setProfileData() {
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.ic_ph_pro)
                .error(R.drawable.ic_ph_pro)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .dontAnimate()
                .dontTransform();
        Glide.with(mActivity)
                .load(NewsAppPreferences.readString(mActivity, NewsAppPreferences.IMAGE, null))
                .apply(options)
                .into(imgProfileIV);
        txtUsernameTV.setText(NewsAppPreferences.readString(mActivity, NewsAppPreferences.USERNAME, null));
    }

    private void setNavigationDrawer() {
        View header = navBar.getHeaderView(0);
//        mSimpleSideDrawer = new SimpleSideDrawer(mActivity);
//        mSimpleSideDrawer.setLeftBehindContentView(R.layout.drawer_layout);
        profileLL = findViewById(R.id.profileLL);
        imgProfileIV = findViewById(R.id.imgProfileIV);
        txtUsernameTV = findViewById(R.id.txtUsernameTV);
        txtSignInTV = findViewById(R.id.txtSignInTV);

        homeLL = header.findViewById(R.id.homeLL);
        imgHomeIV = header.findViewById(R.id.imgHomeIV);
        txtHomeTV = header.findViewById(R.id.txtHomeTV);
        locationLL = header.findViewById(R.id.locationLL);
        txtLocationTV = header.findViewById(R.id.txtLocationTV);
        imgLocIV = header.findViewById(R.id.imgLocIV);
        favLL = header.findViewById(R.id.favLL);
        imgFavIV = header.findViewById(R.id.imgFavIV);
        txtFavTV = header.findViewById(R.id.txtFavTV);
        settingsLL = header.findViewById(R.id.settingsLL);
        imgSettingsIV = header.findViewById(R.id.imgSettingsIV);
        txtSettingsTV = header.findViewById(R.id.txtSettingsTV);
        logoutLL = header.findViewById(R.id.logoutLL);
        imgLogoutIV = header.findViewById(R.id.imgLogoutIV);
        txtLogoutTV = header.findViewById(R.id.txtLogoutTV);
//        profileLL =  header.findViewById(R.id.profileLL);
//        imgProfileIV =  header.findViewById(R.id.imgProfileIV);
//        txtUsernameTV =  header.findViewById(R.id.txtUsernameTV);
        imgShareIV = header.findViewById(R.id.imgShareIV);
        txtShareTV = header.findViewById(R.id.txtShareTV);
        shareLL = header.findViewById(R.id.shareLL);
//        txtSignInTV =  header.findViewById(R.id.txtSignInTV);
        languageLL = header.findViewById(R.id.languageLL);
        txtLanguageTV = header.findViewById(R.id.txtLanguageTV);
        imgLanguageIV = header.findViewById(R.id.imgLanguageIV);
        contactLL = header.findViewById(R.id.contactLL);
        txtContactTV = header.findViewById(R.id.txtContactTV);
        imgContactIV = header.findViewById(R.id.imgContactIV);

    }


    private void setNavigationViewListener() {

        locationLL.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                drawer_layout.closeDrawer(GravityCompat.START);
                imgLocIV.setColorFilter(getResources().getColor(R.color.colorPink));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtLocationTV.setTextColor(getResources().getColor(R.color.colorPink));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LANG_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.SHARE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.PROFILE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOC_SEL, true);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.FAV_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOGOUT_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.HOME_SEL, false);
                txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.CONTACT_SEL, false);


                startActivity(new Intent(mActivity, LocationActivity.class));
                finish();
            }
        });
        favLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgFavIV.setColorFilter(getResources().getColor(R.color.colorPink));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtFavTV.setTextColor(getResources().getColor(R.color.colorPink));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LANG_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.SHARE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.PROFILE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOC_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.FAV_SEL, true);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOGOUT_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.HOME_SEL, false);
                txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.CONTACT_SEL, false);

                if ((NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null)) != null) {
                    drawer_layout.closeDrawer(GravityCompat.START);
                    startActivity(new Intent(mActivity, FavvActivity.class));
                    finish();
                } else {
                    showLoginDialog(mActivity, "Please Login First");
                }

            }
        });

        profileLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorPink));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorPink));
                txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LANG_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.SHARE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.PROFILE_SEL, true);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOC_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.FAV_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOGOUT_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.HOME_SEL, false);
                txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.CONTACT_SEL, false);

                performProfileClick();
            }
        });


        homeLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer_layout.closeDrawer(GravityCompat.START);
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorPink));
                imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorPink));
                txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LANG_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.SHARE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.PROFILE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOC_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.FAV_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOGOUT_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.HOME_SEL, true);
                txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.CONTACT_SEL, false);

                Intent intent = new Intent(mActivity, HomeActivity.class);
                intent.putExtra("loc_sel", "1");
                startActivity(intent);
                finish();
            }
        });

        logoutLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorPink));
                txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorPink));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LANG_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.SHARE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.PROFILE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOC_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.FAV_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOGOUT_SEL, true);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.HOME_SEL, false);
                txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.CONTACT_SEL, false);


                if ((NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null)) != null) {
//                    drawer_layout.closeDrawer(GravityCompat.START);
                    performLogoutClick();
                } else {
                    showAlertDialog(mActivity, "You are not Logged In");
                }

            }
        });

        shareLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtShareTV.setTextColor(getResources().getColor(R.color.colorPink));
                imgShareIV.setColorFilter(getResources().getColor(R.color.colorPink));
                txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LANG_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.SHARE_SEL, true);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.PROFILE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOC_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.FAV_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOGOUT_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.HOME_SEL, false);
                txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.CONTACT_SEL, false);

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Hey checkout our samachar app :  " + "https://play.google.com/store/apps/details?id=com.dailysamachar.app");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });

        txtSignInTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LANG_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.SHARE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.PROFILE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOC_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.FAV_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOGOUT_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.HOME_SEL, false);
                txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.CONTACT_SEL, false);

                Intent intent = new Intent(mActivity, LoginActivity.class);
                startActivity(intent);
                finish();

            }
        });
        languageLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtLanguageTV.setTextColor(getResources().getColor(R.color.colorPink));
                imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorPink));
                txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.SHARE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.PROFILE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOC_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.FAV_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOGOUT_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LANG_SEL, true);
                txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.CONTACT_SEL, false);

                Intent intent = new Intent(mActivity, LanguageActivity.class);
                startActivity(intent);
                finish();

            }
        });
        contactLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
                imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                txtContactTV.setTextColor(getResources().getColor(R.color.colorPink));
                imgContactIV.setColorFilter(getResources().getColor(R.color.colorPink));
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.CONTACT_SEL, true);

                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.SHARE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.PROFILE_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOC_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.FAV_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LOGOUT_SEL, false);
                NewsAppPreferences.writeBoolean(mActivity, NewsAppPreferences.LANG_SEL, false);
                Intent intent = new Intent(mActivity, WebViewActivity.class);
                intent.putExtra(Constants.WV_TYPE, Constants.CONTACT_US);
                startActivity(intent);
                finish();

            }
        });

    }

    private void performProfileClick() {
        if ((NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null)) != null) {
            startActivity(new Intent(mActivity, ProfileActivity.class));
            drawer_layout.closeDrawer(GravityCompat.START);
        } else {
            showLoginDialog(mActivity, "Please Login First");
        }
    }

    private void performLogoutClick() {
        showSignoutAlert(mActivity, getString(R.string.logout_sure));
    }


    public void showSignoutAlert(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.signout_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnNo = alertDialog.findViewById(R.id.btnNo);
        TextView btnYes = alertDialog.findViewById(R.id.btnYes);
        txtMessageTV.setText(strMessage);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
                alertDialog.dismiss();
            }
        });
    }

    public void showAuthorisationAlert(Activity mActivity) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.authorisation_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView loginAgain = alertDialog.findViewById(R.id.loginAgain);

        alertDialog.show();
        loginAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
                alertDialog.dismiss();
            }
        });
    }

//    private void logout() {
//        SharedPreferences preferences = NewsAppPreferences.getPreferences(mActivity);
//        SharedPreferences.Editor editor = preferences.edit();
//        editor.clear();
//        editor.apply();
//        onBackPressed();
//        Intent mIntent = new Intent(mActivity, LoginActivity.class);
//        mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(mIntent);
//    }


    private void logout() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeLogoutApi();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mLogoutParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null));
        Log.e(TAG, "**PARAM**" + mMap);
        return mMap;
    }

    private void executeLogoutApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.logoutRequest(mLogoutParams()).enqueue(new Callback<ForgotPasswordModel>() {
            @Override
            public void onResponse(Call<ForgotPasswordModel> call, Response<ForgotPasswordModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                ForgotPasswordModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    SharedPreferences preferences = NewsAppPreferences.getPreferences(Objects.requireNonNull(mActivity));
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.clear();
                    editor.apply();
                    mActivity.onBackPressed();
                    Intent mIntent = new Intent(mActivity, LoginActivity.class);
                    mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(mIntent);


                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ForgotPasswordModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    /*
   transparent status bar
    */
    public void setStatusBar(Activity mActivity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mActivity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            // edited here
            mActivity.getWindow().setStatusBarColor(getResources().getColor(R.color.colorWhite));
        }
    }


    private void showNewsList() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeCategoryListApi();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("type", "2");
        mMap.put("userID", NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null));
        mMap.put("pageNo", String.valueOf(page_no));
        Log.e(TAG, "**PARAM**" + mMap);
        return mMap;
    }

    private void executeCategoryListApi() {
        if (page_no == 1) {
            showProgressDialog(mActivity);
        } else if (page_no > 1) {
//            mProgressBar.setVisibility(View.GONE);
            dismissProgressDialog();
        }
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.newsListRequest(NewsAppPreferences.readString(mActivity, NewsAppPreferences.AUTH_TOKEN, ""), mParam()).enqueue(new Callback<HomeModel>() {
            @Override
            public void onResponse(Call<HomeModel> call, Response<HomeModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                HomeModel mModel = response.body();

                assert mModel != null;

                if (mModel.getLastPage() != null && !mModel.getLastPage().equals("")) {
                    strLastPage = mModel.getLastPage();
                }
                if (page_no == 1) {
                    dismissProgressDialog();
                }
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {

                    if (page_no == 1) {
                        mNewsList = mModel.getData();

                    } else if (page_no > 1) {
                        tempArrayList = mModel.getData();
                    }
                    if (tempArrayList != null) {
                        if (tempArrayList.size() > 0) {
                            mNewsList.addAll(tempArrayList);
                        }
                    }
                    if (mNewsList.size() > 4) {
                        AddItemToList();
                    }

                    if (page_no == 1) {
                        setCategoryAdapter();
                    } else {
//                        adapter.notifyDataSetChanged();
//                        adapter.notifyItemInserted(pos);
                        synchronized (adapter) {
                            adapter.notify();
                        }
                    }


                    txtNoDataFountTV.setVisibility(View.GONE);
                    imgFavoriteIV.setVisibility(View.VISIBLE);
//                    imgFavoriteIV.setVisibility(View.VISIBLE);
                } else if (mModel.getStatus().equals("3")) {
                    showAuthorisationAlert(mActivity);
                } else {
//                    showAlertDialog(mActivity, mModel.getMessage());
                    txtNoDataFountTV.setVisibility(View.VISIBLE);
                    txtNoDataFountTV.setText(mModel.getMessage());
                    imgFavoriteIV.setVisibility(View.GONE);
                }
            }

            private void setCategoryAdapter() {
                layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setOnFlingListener(null);

                recyclerView.setHasFixedSize(true);
                // tag=intent.getStringExtra("tagName");
                SnapHelper snapHelper = new PagerSnapHelper();
                snapHelper.attachToRecyclerView(recyclerView);


                // this is the scroll listener of recycler view which will tell the current item number
                recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                    }

                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);
                        //here we find the current item number
                        final int scrollOffset = recyclerView.computeVerticalScrollOffset();
                        final int height = recyclerView.getHeight();
                        int page_no = scrollOffset / height;
                        var = page_no;
                        if (var == 0) {
                            if (mNewsList.get(page_no).getFavourite().equals("1")) {
                                imgFavoriteIV.setImageResource(R.drawable.ic_fav_sel);
                                img_fav = true;
                            } else {
                                imgFavoriteIV.setImageResource(R.drawable.ic_fav);
                                img_fav = false;
                            }
                            if (page_no != currentPage) {
                                currentPage = page_no;
                                try {

                                    Set_Player(currentPage);
                                    Release_Privious_Player();


                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        }

//                        else if ((var + 1) % 5 == 0) {
//                            imgFavoriteIV.setVisibility(View.GONE);
////                            showToast(mActivity, "Ad");
//                        }
//
                        else {
                            imgFavoriteIV.setVisibility(View.VISIBLE);
                            if (mNewsList.get(page_no).getFavourite().equals("1")) {
                                imgFavoriteIV.setImageResource(R.drawable.ic_fav_sel);
                                img_fav = true;
                            } else {
                                imgFavoriteIV.setImageResource(R.drawable.ic_fav);
                                img_fav = false;
                            }
                            if (page_no != currentPage) {
                                currentPage = page_no;
                                try {

                                    Set_Player(currentPage);
                                    Release_Privious_Player();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    e.getMessage();
                                    e.getStackTrace();
                                }
                            }
                        }

                    }
                });

                adapter = new FAvoriteAdapter(getApplicationContext(), (ArrayList<HomeModel.Datum>) mNewsList, logo, new FAvoriteAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int positon, String item, View view) {
                        switch (view.getId()) {
                            case R.id.play_video:
//                                        performClick(item);
                                break;
                            case R.id.pause_video:
//                                if (getResources().getConfiguration().orientation== ActivityInfo.SCREEN_ORIENTATION_PORTRAIT){
//                                    topRL.setVisibility(View.GONE);
//                                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//                                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//                                    drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
////                                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
//                                    setTheme(R.style.HomeTheme);
//                                    hideSystemUI();
//                                }
//                                else if (getResources().getConfiguration().orientation==ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE){
//                                    topRL.setVisibility(View.VISIBLE);
//                                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
//                                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//                                    drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
////                                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
//                                    setStatusBar(mActivity);
//                                }else {
//                                    topRL.setVisibility(View.VISIBLE);
//                                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
////                                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
//                                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
//                                    drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
//                                    setStatusBar(mActivity);
//                                }
//                                new Handler().postDelayed(new Runnable() {
//                                    public void run() {
//                                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
//                                        // do something...
//                                    }
//                                }, 6000);
//                                isbuttonClicked = true;
//                                            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//                                performClick2(item, positon);
                                break;
//                            case R.id.imgMuteIV:
//                                performMuteClick();
//                                break;

                        }
                    }
                }, paginationInterface, adsInterface);


//                    @Override
//                    public void onItemClick(int positon, String item, View view) {
//                        s
//                    }
                recyclerView.setItemViewCacheSize(mNewsList.size());
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }


            @Override
            public void onFailure(Call<HomeModel> call, Throwable t) {
                dismissProgressDialog();
                t.printStackTrace();
            }
        });
    }


    private void AddItemToList() {

        List<HomeModel.Datum> mTemNewsList = new ArrayList<>();

        /* to add ads in list in fifth position */
        int i = ITEMS_PER_AD;

        if (page_no > 1) {
            for (int j = 0; j < mNewsList.size(); j++) {
                if (mNewsList.get(j).isIS_AD()) {
                    mNewsList.remove(mNewsList.get(j));
                }
                Log.e(TAG, mNewsList.toString());
            }
            Log.e(TAG, mNewsList.toString());
        }

        while (i <= mNewsList.size()) {
            HomeModel.Datum datum = new HomeModel.Datum();
            datum.setId("");
            datum.setFavourite("");
            datum.setTitle("");
            datum.setDescription("");
            datum.setLogo("");
            datum.setLink("");
            datum.setLocation("");
            datum.setCreated("");
            datum.setLast("");
            datum.setIS_AD(true);
            mNewsList.add(i, datum);
            i += ITEMS_PER_AD;
            i++;
        }

        Log.e(TAG, mNewsList.toString());
    }
//    private void performMuteClick() {
//        if (rem) {
//            img.setImageResource(R.drawable.ic_mute);
//            rem = false;
//            NewsAppPreferences.writeBoolean(mActivity,NewsAppPreferences.REM,false);
//            audioManager.setStreamMute(AudioManager.STREAM_MUSIC,true);
////            audioManager.setStreamVolume(
////                    AudioManager.STREAM_MUSIC, // Stream type
////                    0, // Index
////                    AudioManager.FLAG_SHOW_UI // Flags
////            );
//        } else {
//            img.setImageResource(R.drawable.ic_unmute);
//            rem = true;
//            NewsAppPreferences.writeBoolean(mActivity,NewsAppPreferences.REM,true);
//            audioManager.setStreamMute(AudioManager.STREAM_MUSIC,false);
////            int media_current_volume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
////            AudioManager audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
////            audioManager.setStreamVolume(
////                    AudioManager.STREAM_MUSIC, // Stream type
////                    50, // Index
////                    AudioManager.FLAG_SHOW_UI // Flags
////            );
//        }
//
//    }

    @Override
    public void mPaginationInterface(boolean isLastScrolled) {
        if (isLastScrolled == true) {
            if (strLastPage.equals("FALSE")) {
                showProgressDialog(mActivity);
//            mProgressBar.setVisibility(View.VISIBLE);

                page_no++;
            } else {
//                dismissProgressDialog();
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (strLastPage.equals("FALSE")) {
                        executeCategoryListApi();
                    } else {
//                        if (progressDialog.isShowing()) {
                        dismissProgressDialog();
//                        }
//                        mProgressBar.setVisibility(View.GONE);
                    }
                }
            }, 1000);
        }
    }

    private void performClick2(String item, int positon) {
        Intent intent = new Intent(this, MainActivity20.class);
        intent.putExtra("videoPosition", String.valueOf(positon));
        intent.putExtra("videoId", item);
        intent.putExtra("locc", "favv");
        intent.putExtra("logo", mNewsList.get(positon).getLogo());
        Bundle bundle = new Bundle();
        bundle.putSerializable("mylist", (Serializable) mNewsList);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void performClick(String item) {
        Intent intent = new Intent(this, MainActivity3.class);
        intent.putExtra("videoId", item);
        startActivity(intent);
    }

    private void Release_Privious_Player() {
        if (youTubePlayer2 != null) {
            youTubePlayer2.pause();
            youTubePlayer2.release();
        }
    }

    private void Set_Player(int currentPage) {

        item = mNewsList.get(currentPage).getLink();
        newsId = mNewsList.get(currentPage).getId();

        String extractvid = item;
        Log.e("Check", String.valueOf(extractvid));
        View layout = layoutManager.findViewByPosition(currentPage);
        // YouTubePlayerView youTubePlayerView = layout.findViewById(R.id.youtubePlayerView);
        youTubeView = layout.findViewById(R.id.youtube_view);
        mPlayButtonLayout = layout.findViewById(R.id.video_control);

//        mPlayTimeTextView = (TextView) layout.findViewById(R.id.play_time);
//        mSeekBar = (SeekBar) layout.findViewById(R.id.video_seekbar);
        try {
            youTubeView.initialize(Config.DEVELOPER_KEY,
                    new YouTubePlayer.OnInitializedListener() {
                        @Override
                        public void onInitializationSuccess(YouTubePlayer.Provider provider,
                                                            YouTubePlayer youTubePlayer, boolean wasRestored) {

                            // do any work here to cue video, play video, etc.
                            if (!wasRestored) {
                                String value1 = extractYoutubeVideoId(extractvid);
                                // youTubePlayer.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE);
                                if (value1 != null) {
//                                try {
                                    try {
                                        Log.e(TAG, "onInitializationSuccess: "+value1);
                                        youTubePlayer.loadVideo(value1);
                                    } catch (IllegalStateException e) {
                                        youTubeView.initialize(Config.DEVELOPER_KEY, this);
                                    }
                                }
                                // loadVideo() will auto play video
                                // Use cueVideo() method, if you don't want to play it automatically
                                youTubePlayer2 = youTubePlayer;
                                youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.MINIMAL);
                            }

                        }

                        @Override
                        public void onInitializationFailure(YouTubePlayer.Provider provider,
                                                            YouTubeInitializationResult errorReason) {
                            if (errorReason.isUserRecoverableError()) {
//                            errorReason.getErrorDialog(FavvActivity.this, RECOVERY_DIALOG_REQUEST).show();
                                youTubeView.initialize(Config.DEVELOPER_KEY, this);
                            } else {
//                            String errorMessage = String.format(
//                                    getString(R.string.error_player), errorReason.toString());
//                            Toast.makeText(FavvActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                                youTubeView.initialize(Config.DEVELOPER_KEY, this);
                            }

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static String extractYoutubeVideoId(String ytUrl) {

        String vId = null;

        String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";

        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(ytUrl);

        if (matcher.find()) {
            vId = matcher.group();
        }
        return vId;
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Toast.makeText(mActivity, "pause", Toast.LENGTH_SHORT).show();
        try {
            if (youTubePlayer2 != null) {
                youTubePlayer2.pause();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (NewsAppPreferences.readBoolean(mActivity, NewsAppPreferences.HOME_SEL, false)) {
            imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgHomeIV.setColorFilter(getResources().getColor(R.color.colorPink));
            imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorPink));
            txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
        } else if (NewsAppPreferences.readBoolean(mActivity, NewsAppPreferences.FAV_SEL, false)) {
            imgFavIV.setColorFilter(getResources().getColor(R.color.colorPink));
            imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtFavTV.setTextColor(getResources().getColor(R.color.colorPink));
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));

        } else if (NewsAppPreferences.readBoolean(mActivity, NewsAppPreferences.LOC_SEL, false)) {
            imgLocIV.setColorFilter(getResources().getColor(R.color.colorPink));
            imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            txtLocationTV.setTextColor(getResources().getColor(R.color.colorPink));
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
        } else if (NewsAppPreferences.readBoolean(mActivity, NewsAppPreferences.LOGOUT_SEL, false)) {
            imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorPink));

            txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorPink));
            txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
        } else if (NewsAppPreferences.readBoolean(mActivity, NewsAppPreferences.PROFILE_SEL, false)) {

            imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorPink));
            imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtSettingsTV.setTextColor(getResources().getColor(R.color.colorPink));
            txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
        } else if (NewsAppPreferences.readBoolean(mActivity, NewsAppPreferences.SHARE_SEL, false)) {
            imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtShareTV.setTextColor(getResources().getColor(R.color.colorPink));
            imgShareIV.setColorFilter(getResources().getColor(R.color.colorPink));

            txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
        } else if (NewsAppPreferences.readBoolean(mActivity, NewsAppPreferences.LANG_SEL, false)) {
            imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));

            txtLanguageTV.setTextColor(getResources().getColor(R.color.colorPink));
            imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorPink));

            txtContactTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgContactIV.setColorFilter(getResources().getColor(R.color.colorBlack));
        } else if (NewsAppPreferences.readBoolean(mActivity, NewsAppPreferences.CONTACT_SEL, false)) {
            imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgFavIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLocIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            imgContactIV.setColorFilter(getResources().getColor(R.color.colorPink));

            txtFavTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLocationTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtShareTV.setTextColor(getResources().getColor(R.color.colorBlack));
            imgShareIV.setColorFilter(getResources().getColor(R.color.colorBlack));
            txtLanguageTV.setTextColor(getResources().getColor(R.color.colorBlack));
            txtContactTV.setTextColor(getResources().getColor(R.color.colorPink));
            imgContactIV.setColorFilter(getResources().getColor(R.color.colorPink));
            imgLanguageIV.setColorFilter(getResources().getColor(R.color.colorBlack));
        }

        if ((NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null)) != null) {
            setProfileData();
            imgProfileIV.setVisibility(View.VISIBLE);
            txtUsernameTV.setVisibility(View.VISIBLE);
            txtSignInTV.setVisibility(View.GONE);
        } else {
            imgProfileIV.setVisibility(View.GONE);
            txtUsernameTV.setVisibility(View.GONE);
            txtSignInTV.setVisibility(View.VISIBLE);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    Release_Privious_Player();
                    Set_Player(currentPage);
                    if (youTubePlayer2 != null) {
                        youTubePlayer2.play();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 1000);



//        if (isbuttonClicked) {
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
//            isbuttonClicked = false;
//        }
    }

    @OnClick({R.id.imgFavoriteIV, R.id.imgMenuIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgFavoriteIV:
                if ((NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null)) != null) {
                    favUnfav();
                } else {
                    showLoginDialog(mActivity, "Please Login First");
                }
                break;
            case R.id.imgMenuIV:
//                mSimpleSideDrawer.openLeftSide();
                drawerOpenClick();
                break;
        }
    }

    private void drawerOpenClick() {
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer_layout, 1, 0) {

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
//                super.onDrawerSlide(drawerView, slideOffset);
//                float slideX = drawerView.getWidth() * slideOffset;
//                content.setTranslationX(slideX);
                RelativeLayout fbRl = findViewById(R.id.fbRL);
                RelativeLayout twitterRL = findViewById(R.id.twitterRL);
                RelativeLayout pinterestRL = findViewById(R.id.pinterestRL);
                RelativeLayout instagramRL = findViewById(R.id.instagramRL);

                fbRl.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Uri uri = Uri.parse("https://www.facebook.com/samachartvapp");
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }
                });

                twitterRL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        Uri uri = Uri.parse("https://twitter.com/AppSamachar");
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }
                });

                instagramRL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Uri uri = Uri.parse("https://www.instagram.com/samacharapp/");
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);

                    }
                });

                pinterestRL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Uri uri = Uri.parse("https://in.pinterest.com/appsamachar");
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }
                });
            }
        };

        drawer_layout.addDrawerListener(actionBarDrawerToggle);
        drawer_layout.openDrawer(Gravity.LEFT);
    }


    private boolean checkSharePermission() {
        int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
        int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED;
    }

    private void requestSharePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 360);
        } else {
        }
    }

    @Override
    public void onBackPressed() {
        if (getResources().getConfiguration().orientation == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
            topRL.setVisibility(View.VISIBLE);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            showSystemUI();
            setStatusBar(mActivity);
        } else if (getResources().getConfiguration().orientation == ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE) {
            topRL.setVisibility(View.VISIBLE);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            showSystemUI();
            setStatusBar(mActivity);
        } else if (getResources().getConfiguration().orientation == 2) {
            topRL.setVisibility(View.VISIBLE);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            showSystemUI();
            setStatusBar(mActivity);
        }
//        else if ((NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null)) == null) {
//            startActivity(new Intent(mActivity, LoginActivity.class));
//            finish();
//        }
        else {
            startActivity(new Intent(mActivity, HomeActivity.class));
            finish();
        }


//        if ((NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null)) == null) {
//            startActivity(new Intent(mActivity, LoginActivity.class));
//            finish();
//        } else {
//            if (getResources().getConfiguration().orientation==ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE){
//                topRL.setVisibility(View.VISIBLE);
//                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
//                drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
////                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
//            }else {
//                startActivity(new Intent(mActivity, HomeActivity.class));
//                finish();
//            }
//
//        }
    }

    private void favUnfav() {
        performfavUnfav();
    }


    /*
     *
     * Error Alert Dialog
     * */
    public void showLoginDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.setCancelable(true);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                startActivity(new Intent(mActivity, LoginActivity.class));
                finish();
            }
        });
        alertDialog.show();
    }

    /*
     * Check Internet Connections
     * */
    public boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    /*
     * Toast Message
     * */
    public void showToast(Activity mActivity, String strMessage) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show();
    }


    /*
     *
     * Error Alert Dialog
     * */
    public void showAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    /*
     * Show Progress Dialog
     * */

    public void showProgressDialog(Activity mActivity) {
        dismissProgressDialog();
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        Objects.requireNonNull(progressDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        if (progressDialog != null)
            progressDialog.show();
    }


    /*
     * Hide Progress Dialog
     * */
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void fav(int pos) {

    }

    public void performfavUnfav() {
        if (!isNetworkAvailable(Objects.requireNonNull(mActivity))) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeFavUnFavApi();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", NewsAppPreferences.readString(mActivity, NewsAppPreferences.ID, null));
        mMap.put("newsID", newsId);
        Log.e(TAG, "**PARAM**" + mMap);
        return mMap;
    }

    private void executeFavUnFavApi() {
//        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.favUnfavRequest(NewsAppPreferences.readString(mActivity, NewsAppPreferences.AUTH_TOKEN, ""), mParams()).enqueue(new Callback<FavUnfavModel>() {
            @Override
            public void onResponse(Call<FavUnfavModel> call, Response<FavUnfavModel> response) {
//                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                FavUnfavModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {

                    if (mModel.getType().equals(1)) {
                        showToast(mActivity, mModel.getMessage());
                        mNewsList.get(var).setFavourite("1");
                        imgFavoriteIV.setImageResource(R.drawable.ic_fav_sel);
                        imgFavoriteIV.setColorFilter(ContextCompat.getColor(mActivity, R.color.colorPink), android.graphics.PorterDuff.Mode.MULTIPLY);
                        img_fav = true;
                    } else {
                        mNewsList.get(var).setFavourite("0");
                        imgFavoriteIV.setImageResource(R.drawable.ic_fav);
                        img_fav = false;
                        showToast(mActivity, mModel.getMessage());
                    }

                } else if (mModel.getStatus().equals("3")) {
                    showAuthorisationAlert(mActivity);
                } else {
                    showAlertDialog(mActivity, "No Data Available");
                }
            }

            @Override
            public void onFailure(Call<FavUnfavModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    @Override
    public void adsPos(int pos) {
        adsPos = pos;

//        interstitialAd = new InterstitialAd(mActivity, "IMG_16_9_APP_INSTALL#445117679825548_492968928373756");
//        interstitialAdListener = new InterstitialAdListener() {
//            @Override
//            public void onInterstitialDisplayed(Ad ad) {
//
//                Log.e(TAG, "Interstitial ad displayed.");
//            }
//
//            @Override
//            public void onInterstitialDismissed(Ad ad) {
////                ScheduledExecutorService scheduler =
////                        Executors.newSingleThreadScheduledExecutor();
////                scheduler.scheduleAtFixedRate(new Runnable() {
////
////                    public void run() {
////                        Log.i("hello", "world");
////                        runOnUiThread(new Runnable() {
////                            public void run() {
////                                interstitialAd.loadAd(
////                                        interstitialAd.buildLoadAdConfig()
////                                                .withAdListener(interstitialAdListener)
////                                                .build());
////                                if (interstitialAd.isAdLoaded()) {
////                                    interstitialAd.show();
////                                } else {
////                                    Log.d("TAG"," Interstitial not loaded");
////                                }
////
////                            }
////                        });
////
////                    }
////                }, 1, 1, TimeUnit.MINUTES);
//
//
//                Log.e(TAG, "Interstitial ad dismissed.");
//            }
//
//            @Override
//            public void onError(Ad ad, com.facebook.ads.AdError adError) {
//
//            }
//
//            @Override
//            public void onAdLoaded(Ad ad) {
//
//                Log.d(TAG, "Interstitial ad is loaded and ready to be displayed!");
//                if(interstitialAd != null && interstitialAd.isAdLoaded())
//                interstitialAd.show();
//            }
//
//            @Override
//            public void onAdClicked(Ad ad) {
//
//                Log.d(TAG, "Interstitial ad clicked!");
//            }
//
//            @Override
//            public void onLoggingImpression(Ad ad) {
//
//                Log.d(TAG, "Interstitial ad impression logged!");
//            }
//        };
//
////        Handler handler = new Handler();
////        handler.postDelayed(new Runnable() {
////            @Override
////            public void run() {
//        interstitialAd.loadAd(
//                interstitialAd.buildLoadAdConfig()
//                        .withAdListener(interstitialAdListener)
//                        .build());

//            }
//        }, 5000);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        dismissProgressDialog();
        Log.e(TAG, "onDestroy");
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e(TAG, "onStop");
        dismissProgressDialog();
//        finish();
    }

//    @Override
//    public void getImg(ImageView img) {
//        this.img=img;
//        performMuteClick();
//    }
}