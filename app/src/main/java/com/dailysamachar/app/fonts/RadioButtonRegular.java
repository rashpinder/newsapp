package com.dailysamachar.app.fonts;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.RadioButton;

import androidx.annotation.RequiresApi;

/**
 * Created by Dharmani Apps on 1/18/2018.
 */

/*
 * TextView with Custom font family
 * */
@SuppressLint("AppCompatCustomView")
public class RadioButtonRegular extends RadioButton {

    /*
     * Getting Current Class Name
     * */
    String mTag = RadioButtonRegular.this.getClass().getSimpleName();

    /*
     * Constructor with
     * #Context
     * */
    public RadioButtonRegular(Context context) {
        super(context);
        applyCustomFont(context);
    }


    /*
     * Constructor with
     * #Context
     * #AttributeSet
     * */
    public RadioButtonRegular(Context context,  AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }


    /*
     * Constructor with
     * #Context
     * #AttributeSet
     * #int defStyleAttr
     * */
    public RadioButtonRegular(Context context,  AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }


    /*
     * Constructor with
     * #Context
     * #AttributeSet
     * #int defStyleAttr
     * #int defStyleAttr
     * */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public RadioButtonRegular(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        applyCustomFont(context);
    }

    /*
     * Apply font.
     * */
    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new PoppinsRegular(context).getFont());
        } catch (Exception e) {
            Log.e(mTag, e.toString());
        }
    }
}
