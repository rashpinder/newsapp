package com.dailysamachar.app.fonts;

import android.content.Context;
import android.graphics.Typeface;


public class PoppinsRegular {
    /*
     * Initialize the Typeface
     * */
    public static Typeface fontTypeface;
    /*
     * Custom 3rd parth Font family
     * */
    public String path = "Regular.ttf";
    /*
     * Initialize Context
     * */
    public Context mContext;
    /*
     * Default Constructor
     * */
    public PoppinsRegular() {
    }

    /*
     * Constructor with Context
     * */
    public PoppinsRegular(Context context) {
        mContext = context;
    }

    /*
     * Getting the Font Familty from the Assets Folder
     * */
    public Typeface getFont() {
        if (fontTypeface == null)
            fontTypeface = Typeface.createFromAsset(mContext.getAssets(), path);
        return fontTypeface;
    }

}