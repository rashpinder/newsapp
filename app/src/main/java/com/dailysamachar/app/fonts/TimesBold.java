package com.dailysamachar.app.fonts;

import android.content.Context;
import android.graphics.Typeface;

public class TimesBold {
        /*
         * Initialize Context
         * */
        Context mContext;
        /*
         * Initialize the Typeface
         * */
        private Typeface fontTypeface;
        /*
         * Custom 3rd parth Font family
         * */
        private String path = "TimesBold.ttf";

        /*
         * Default Constructor
         * */
        public TimesBold() {
        }

        /*
         * Constructor with Context
         * */
        public TimesBold(Context context) {
            mContext = context;
        }

        /*
         * Getting the Font Familty from the Assets Folder
         * */
        public Typeface getFont() {
            if (fontTypeface == null) {
                fontTypeface = Typeface.createFromAsset(mContext.getAssets(), path);
            }
            return fontTypeface;
        }

    }

