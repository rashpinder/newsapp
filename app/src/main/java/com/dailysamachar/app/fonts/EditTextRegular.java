package com.dailysamachar.app.fonts;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.EditText;

/**
 * Created by Dharmani Apps on 1/18/2018.
 */


/*
 * EditText with Custom font family
 * */
@SuppressLint("AppCompatCustomView")
public class EditTextRegular extends EditText {
    /*
     * Getting Current Class Name
     * */
    private String mTag = EditTextRegular.this.getClass().getSimpleName();

    /*
     * Constructor with
     * #Context
     * */
    public EditTextRegular(Context context) {
        super(context);
        applyCustomFont(context);
    }

    /*
     * Constructor with
     * #Context
     * #AttributeSet
     * */
    public EditTextRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    /*
     * Constructor with
     * #Context
     * #AttributeSet
     * #int defStyleAttr
     * */
    public EditTextRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }


    /*
     * Apply font.
     * */
    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new PoppinsRegular(context).getFont());
        } catch (Exception e) {
            Log.e(mTag,e.toString());
        }
    }
}
