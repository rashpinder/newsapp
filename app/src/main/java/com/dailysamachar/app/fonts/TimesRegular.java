package com.dailysamachar.app.fonts;

import android.content.Context;
import android.graphics.Typeface;

public class TimesRegular {
    /*
     * Initialize Context
     * */
    Context mContext;
    /*
     * Initialize the Typeface
     * */
    private Typeface fontTypeface;
    /*
     * Custom 3rd parth Font family
     * */
    private String path = "TimesRegular.ttf";

    /*
     * Default Constructor
     * */
    public TimesRegular() {
    }

    /*
     * Constructor with Context
     * */
    public TimesRegular(Context context) {
        mContext = context;
    }

    /*
     * Getting the Font Familty from the Assets Folder
     * */
    public Typeface getFont() {
        if (fontTypeface == null) {
            fontTypeface = Typeface.createFromAsset(mContext.getAssets(), path);
        }
        return fontTypeface;
    }

}
    
