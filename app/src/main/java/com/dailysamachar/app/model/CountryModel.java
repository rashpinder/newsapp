package com.dailysamachar.app.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class CountryModel{

	@SerializedName("country")
	private List<CountryItem> country;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private String status;


	public List<CountryItem> getCountry(){
		return country;
	}

	public void setCountry(List<CountryItem> country) {
		this.country = country;
	}

	public String getMessage(){
		return message;
	}

	public String getStatus(){
		return status;
	}


	public class CountryItem{

		@SerializedName("created")
		private String created;

		@SerializedName("description")
		private String description;

		@SerializedName("id")
		private String id;

		@SerializedName("title")
		private String title;

		public void setCreated(String created) {
			this.created = created;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public void setId(String id) {
			this.id = id;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getCreated(){
			return created;
		}

		public String getDescription(){
			return description;
		}

		public String getId(){
			return id;
		}

		public String getTitle(){
			return title;
		}
	}


}