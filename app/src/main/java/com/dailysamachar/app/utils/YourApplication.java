package com.dailysamachar.app.utils;

import android.app.Application;
import android.content.Context;

import com.facebook.ads.AudienceNetworkAds;
import com.logrocket.core.SDK;


public class YourApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        AudienceNetworkAds.initialize(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

        SDK.init(
                this,
                base,
                options -> {
                    options.setAppID("yoemt4/smachar");
                }
        );
    }
}