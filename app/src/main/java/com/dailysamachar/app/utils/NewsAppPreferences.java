package com.dailysamachar.app.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class NewsAppPreferences {

    /***************
     * Define SharedPrefrances Keys & MODE
     ****************/
    public static final String PREF_NAME = "App_PREF";
    public static final int MODE = Context.MODE_PRIVATE;
    /*
     * Keys
     * */
    public static final String ID = "id";
    public static final String ISLOGIN = "is_login";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String USERNAME = "username";
    public static final String CREATION_AT = "creation_at";
    public static final String IMAGE = "image";
    public static final String VER_CODE = "ver_code";
    public static final String AUTH_TOKEN = "AUTH_TOKEN";
    public static final String VERIFIED = "verified";
    public static final String FB_TOKEN = "fb_token";
    public static final String ADMIN = "admin";
    public static final String GOOGLE_TOKEN = "google_token";
    public static final String PH_NO = "ph_no";
    public static final String BIO = "bio";
    public static final String Phone_TYPE = "phone_type";
    public static final String FACEBOOK_IDD = "fb_id";
    public static final String CURR_LAT = "current_lat";
    public static final String CURR_LONG = "current_long";
    public static final String TYPE = "type";
    public static final String STATE = "state";
    public static final String SELECT_STATE_FROM_STATE_DROPDOWN = "SELECT_STATE_FROM_STATE_DROPDOWN";
    public static final String COUNTRY = "country";
    public static final String PHONE_NUMBER = "phone_number";
    public static final String IS_FIRST_TIME = "isFirstTime";

    public static final String HOME_SEL = "home_sel";
    public static final String FAV_SEL = "fav_sel";
    public static final String LOC_SEL = "loc_sel";
    public static final String PROFILE_SEL = "profile_sel";
    public static final String LOGOUT_SEL = "logout_sel";
    public static final String SHARE_SEL = "share_sel";
    public static final String DEVICE_TOKEN = "device_token";
    public static final String LANG_SEL = "lang_sel";
    public static final String CONTACT_SEL = "contact_sel";
    public static final String SELECTED_LOCATION = "selected_location";
    public static final String DELETE_ACCOUNT = "delete_account";
    public static final String LAST_PAGE = "last_page";
    public static final String REM = "rem";


    /*
     * Write the Boolean Value
     * */
    public static void writeBoolean(Context context, String key, boolean value) {
        getEditor(context).putBoolean(key, value).apply();
    }

    /*
     * Read the Boolean Valueget
     * */
    public static boolean readBoolean(Context context, String key, boolean defValue) {
        return getPreferences(context).getBoolean(key, defValue);
    }



    /*
     * Write the Integer Value
     * */
    public static void writeInteger(Context context, String key, int value) {
        getEditor(context).putInt(key, value).apply();

    }

    /*
     * Read the Interger Value
     * */
    public static int readInteger(Context context, String key, int defValue) {
        return getPreferences(context).getInt(key, defValue);
    }

    /*
     * Write the String Value
     * */
    public static void writeString(Context context, String key, String value) {
        getEditor(context).putString(key, value).apply();

    }

    /*
     * Read the String Value
     * */
    public static String readString(Context context, String key, String defValue) {
        return getPreferences(context).getString(key, defValue);
    }

    /*
     * Write the Float Value
     * */
    public static void writeFloat(Context context, String key, float value) {
        getEditor(context).putFloat(key, value).apply();
    }

    /*
     * Read the Float Value
     * */
    public static float readFloat(Context context, String key, float defValue) {
        return getPreferences(context).getFloat(key, defValue);
    }

    /*
     * Write the Long Value
     * */
    public static void writeLong(Context context, String key, long value) {
        getEditor(context).putLong(key, value).apply();
    }

    /*
     * Read the Long Value
     * */
    public static long readLong(Context context, String key, long defValue) {
        return getPreferences(context).getLong(key, defValue);
    }

    /*
     * Return the SharedPreferences
     * with
     * @Prefreces Name & Prefreces = MODE
     * */
    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREF_NAME, MODE);
    }

    /*
     * Return the SharedPreferences Editor
     * */
    public static SharedPreferences.Editor getEditor(Context context) {
        return getPreferences(context).edit();
    }

}

