package com.dailysamachar.app.utils;

public class Constants {
    /*
     * Constants @params
     * */

    public  static String WV_TYPE = "wv_type";
    public  static String PRIVACY_POLICY = "privacy_policy";
    public  static String ABOUT = "about";
    public  static String TERMS_SERVICES = "term_services";
    public  static String CONTACT_US = "contact";

    public  static int REQUEST_CODE = 123;
    public  static final int REQUEST_PERMISSION_CODE = 919;

    public  static String PHONE_NUMBER = "phone_number";
    public  static String DIAL_CODE = "dial_code";


    //web view urls
    public static String PRIVACY_POLICY_WEB = "https://dharmani.com/NewsApp/PrivacyPolicy.html";
    public static String TERMS_WEB ="https://dharmani.com/NewsApp/TermsAndConditions.html";
    public static String ABOUT_WEB = "https://www.dharmani.com/NewsApp/About.php";
//    public static String CONTACT_WEB = "https://www.dharmani.com/contact.html";
    public static String CONTACT_WEB = "https://dharmani.com/NewsApp/contact.php";
}
