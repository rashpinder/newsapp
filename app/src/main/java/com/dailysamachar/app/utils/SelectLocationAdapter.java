package com.dailysamachar.app.utils;

import static android.content.ContentValues.TAG;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.dailysamachar.app.R;
import com.dailysamachar.app.interfaces.StateInterface;
import com.dailysamachar.app.model.StatesModel;

import java.util.List;

public class SelectLocationAdapter extends RecyclerView.Adapter<SelectLocationAdapter.MyViewHolder> {
    private final Context context;
    List<StatesModel.State> mStateArrayList;
    private final StateInterface mStateInterface;

    private int row_index;
    private int count = 0;
    //    String sel_stateID;
    String state_name;
    boolean selected = false;

    public SelectLocationAdapter(Context context, List<StatesModel.State> mStateArrayList, StateInterface mStateInterface, String state_name) {
        this.context = context;
        this.mStateArrayList = mStateArrayList;
        this.mStateInterface = mStateInterface;
        this.state_name = state_name;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_states_list, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder,  int listPosition) {
        StatesModel.State mModel = mStateArrayList.get(listPosition);
        holder.itemStateTV.setText(mModel.getTitle());
        holder.rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                row_index = listPosition;
                count = 1;
                selected = true;
                notifyDataSetChanged();

            }
        });


        if (row_index == listPosition && count == 1) {
            mStateInterface.state(mModel.getTitle());
        }

//
//        if (row_index == 0 && count == 1) {
//            mStateInterface.state(NewsAppPreferences.readString(context, NewsAppPreferences.SELECTED_LOCATION, null));
//            holder.imgCheckIV.setImageResource(R.drawable.ic_check);
//        } else {
//            holder.imgCheckIV.setImageResource(R.drawable.ic_uncheck);
//        }


        if (row_index == listPosition && count == 1) {
            mStateInterface.state(mModel.getTitle());
            holder.imgCheckIV.setImageResource(R.drawable.ic_check);
        } else {
            holder.imgCheckIV.setImageResource(R.drawable.ic_uncheck);
        }


        if (!selected) {
            if (mModel.getTitle().equals(state_name)) {
                holder.imgCheckIV.setImageResource(R.drawable.ic_check);
//                mStateInterface.state(mModel.getTitle());
            } else {
                holder.imgCheckIV.setImageResource(R.drawable.ic_uncheck);
            }
        }

        if (state_name != null && !state_name.equals("") ) {
            if(NewsAppPreferences.readString(context, NewsAppPreferences.SELECTED_LOCATION, null)!=null)
            {
                if (NewsAppPreferences.readString(context, NewsAppPreferences.SELECTED_LOCATION, null).equals(state_name)) {
                    if (listPosition == 0) {
                        holder.imgCheckIV.setImageResource(R.drawable.ic_check);
                    }
                } else {
                    if (listPosition == 0) {
                        holder.imgCheckIV.setImageResource(R.drawable.ic_uncheck);
                    }
                }
            }

        }



}


    @Override
    public int getItemCount() {
        return (mStateArrayList == null) ? 0 : mStateArrayList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView itemStateTV;
        ImageView imgCheckIV;
        RelativeLayout rl;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.itemStateTV = itemView.findViewById(R.id.itemStateTV);
            this.rl = itemView.findViewById(R.id.rl);
            this.imgCheckIV = itemView.findViewById(R.id.imgCheckIV);
        }
    }

}
